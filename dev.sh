# Useful if you use cygwin, but something messes up your PATH or PKG_CONFIG_PATH..
# Usage: . dev.sh
# ( ./dev.sh won't work! )

export PATH=/usr/local/bin:/usr/bin:/bin:/usr/X11R6/bin
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/lib/pkgconfig:/usr/X11R6/lib/pkgconfig
