#!/bin/sh

git log --pretty=oneline -n 1 `echo -n $1 | sed "s;^$PWD/;;"` | cat
