#! /usr/bin/env python
# encoding: utf-8

import os

HAVE_GIT=os.system("git rev-parse --git-dir >/dev/null 2>/dev/null") == 0
if (HAVE_GIT):
	TAG=os.popen('git describe --tag --candidates=0 2>/dev/null').readline().strip('\n')
	if (TAG != ''):
		INGAMEVER="git-"+TAG
	else:
		INGAMEVER='git-'+os.popen('git-rev-parse --default HEAD').readline().strip('\n')
	BRANCH=os.popen("git branch | grep -e '^\*' | sed 's/^\* //'").readline().strip('\n')
else:
	INGAMEVER="0.0-r1"
	BRANCH="release"


# the following two variables are used by the target "waf dist"
VERSION=str(INGAMEVER)
APPNAME='dirtyblobwars'

# these variables are mandatory ('/' are converted automatically)
srcdir = 'src'
blddir = 'build'

import Params
Params.g_autoconfig = 1

def init():
	pass

def set_options(opt):
	# options provided in a script in a subdirectory named "src"
	opt.sub_options('src')

	# options provided by the modules
	opt.tool_options('g++')
	opt.tool_options('boost')

	# custom options
	opt.add_option('--with-mapeditor', action='store_true', help='Build a simple map editor', dest='mapedit')
	opt.add_option('--bindir', type='string', default='games/bin/', help='Put binaries into this dir (relative to prefix)', dest='bindir')
	opt.add_option('--docsdir', type='string', default='share/doc/dirtystudios/blobwars/', help='Put docs into this dir (relative to prefix)', dest='docsdir')
	opt.add_option('--datadir', type='string', default='share/games/dirtystudios/blobwars/', help='Put data files into this dir (relative to prefix)', dest='datadir')


iconv_test = """
#include <iconv.h>

int main()
{

	ICONV_CONST char* in_orig = "boo";
	size_t in_len = 6;
	char* out;
	size_t out_len;
	iconv_t cd;

	iconv(cd, &in_orig, &in_len, &out, &out_len);

	return 0;
}
"""


def check_iconv(conf):
	conf.check_header2('iconv.h')
	conf.check_library2('iconv', mandatory=0)

	test = conf.create_test_configurator()
	test.code = "#define ICONV_CONST const\n" + iconv_test
	test.uselib = 'ICONV'
	status = test.run()

	if status:
		conf.check_message('we can compile iconv program', '', 1, 'with const')
		conf.env['defines']['ICONV_CONST'] = 'const'
	else:
		test.code = "#define ICONV_CONST  \n" + iconv_test
		status = test.run()
		if status:
			conf.check_message('we can compile iconv program', '', 1, 'without const')
			conf.env['defines']['ICONV_CONST'] = ' '
		else:
			conf.check_message('we can compile iconv program', '', 0)
			raise "Compilation failed"
	
	pass

def check_jpeg(conf):
	from Params import fatal

	if not conf.check_library2('jpeg.dll', uselib='JPEG', mandatory = 0):
		conf.check_library2('jpeg', uselib='JPEG')
	# whaa! Why there aren't any people who can program? Making a header without including required headers?!?!
	#  Looks, not only Parallel Realities can't code..
	test = conf.create_header_configurator()
	test.name = 'jpeglib.h'
	test.header_code = "#include <cstdio>\n"
	test.mandatory = 1
	test.run()

	pass

def boost_header(conf, name):
	test = conf.create_header_configurator()
        test.name = name
        test.mandatory = 1
	test.uselib = "BOOST"
        test.run()

	pass

def configure(conf):
	import Params
	from Params import fatal
	pre = conf.env['PREFIX']
	conf.env['BINDIR']  = pre + Params.g_options.bindir
	conf.env['DOCSDIR'] = pre + Params.g_options.docsdir
	conf.env['DATADIR'] = pre + Params.g_options.datadir
	conf.env['MAPEDIT'] = Params.g_options.mapedit

	conf.check_tool('g++')

	#conf.sub_config('src')

	#conf.env['CXXFLAGS']='-g -Wall -O0' # -I/home/dirty_ice/crossdev/winroot/include'
	#conf.env['CCFLAGS']='-g -Wall -O0'
	#conf.env['LINKFLAGS']='-L/home/dirty_ice/crossdev/winroot/lib'
	if (Params.g_options.debug_level == "ultradebug"):
		conf.env['CXXFLAGS'] += ["-Wabi", "-Wctor-dtor-privacy", "-Wnon-virtual-dtor", "-Wreorder", "-Weffc++", "-Wstrict-null-sentinel", "-Wold-style-cast", "-Woverloaded-virtual", "-Wsign-promo", "-Wformat=2", "-Winit-self", "-Wswitch-default", "-Wswitch-enum", "-Wunused", "-Wextra", "-Wfloat-equal", "-Wundef", "-Wpointer-arith", "-Wcast-qual", "-Wcast-align", "-Wwrite-strings", "-Wconversion", "-Waddress", "-Wmissing-field-initializers", "-Wmissing-noreturn", "-Wmissing-format-attribute", "-Wpacked", "-Wno-invalid-offsetof", "-Wvolatile-register-var", "-Wcomment", "-Wtrigraphs", "-Wimport"]


	# check SDL stuff
	conf.check_pkg2('sdl', '1.2')
	conf.check_tool('checks')
	conf.check_header2('SDL/SDL_image.h')
	conf.check_library2('SDL_image')
	conf.check_header2('SDL/SDL_mixer.h')
	conf.check_library2('SDL_mixer')
	conf.check_header2('SDL/SDL_ttf.h')
	conf.check_library2('SDL_ttf')
	conf.check_header2('physfs.h')

	# check physfs
	conf.check_library2('physfs')
	conf.check_header2('physfs.h')
	
	# check iconv
	check_iconv(conf)

	# check libpng
	conf.check_pkg2('libpng12', '1.2', uselib='PNG')

	# check libjpeg
	check_jpeg(conf)

	# check boost stuff
	#conf.env["WANT_BOOST"] = ""
	conf.check_tool('boost')
	boost_header(conf, 'boost/format.hpp')
	boost_header(conf, 'boost/scoped_ptr.hpp')
	boost_header(conf, 'boost/shared_ptr.hpp')
	boost_header(conf, 'boost/tokenizer.hpp')

	platform=conf.detect_platform()
	conf.check_message_custom("platform", '', platform)
	if (platform != 'cygwin'):
		conf.define('UNIX', 1)
	else:
		conf.define('WIN32', 1)

	conf.env['VERSION'] = INGAMEVER
	conf.env['BRANCH'] = BRANCH
	conf.define('VERSION', INGAMEVER)
	conf.define('BRANCH', BRANCH)
	conf.define('DATADIR', conf.env['DATADIR'])

	conf.write_config_header('config.h')

def build(bld):
	import preproc
	preproc.strict_quotes = 1

	bld.add_subdirs('.')

def shutdown():
	import os, Params
	if not Params.g_commands['build']: return
	os.popen('ln -f -s build/default/dirtyblobwars ../dirtyblobwars')
	os.popen('ln -f -s build/default/dirtyblobwars-mapeditor ../mapedit')
#EOF
