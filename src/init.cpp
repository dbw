/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <SDL_mixer.h>

#include "CAudio.h"
#include "CEngine.h"
#include "CGame.h"
#include "CGraphics.h"
#include "CMap.h"
#include "CMath.h"
#include "config.h"
#include "font/manager.hpp"
#include "gettext.hpp"
#include "init.h"
#include "console/console.hpp"
#include "physfs/manager.hpp"
#include "physfs/stream.hpp"

FontEngine::Manager font_manager;

void checkForLicense()
{
	try
	{
		if (!engine.loadData("data/LICENSE"))
		{
			throw 0;
		}
	}
	catch (const Exception::Base& e)
	{
		error << e.GetDesc() << std::endl;
		graphics.showLicenseErrorAndExit();
	}
	catch (...)
	{
		graphics.showLicenseErrorAndExit();
	}
}

/*
Show the GNU Public License the first time the game is played. Waits 4 seconds
and then proceeds. THIS MUST NOT BE REMOVED!!!!!
  - Why MUST NOT? The game is GPL, and it's allow me to remove things, you can't prohibit it.
	I will remove it, because i get sick of waiting the stupid license screen 120 times a day
	because the version change. And stop SHOUTING and stupid !!!!!! putting!
	~ DirtY iCE
*/
void showLicense()
{
	SDL_FillRect(graphics.screen, NULL, graphics.black);
	graphics.delay(1000);

	SDL_FillRect(graphics.screen, NULL, graphics.black);
	SDL_Surface *pic = graphics.loadImage("gfx/main/licensePic.png");
	graphics.blit(pic, 0, 0, graphics.screen, false);
	SDL_FreeSurface(pic);

	checkForLicense();

	char line[255];
	int y = 0;

	char *token = strtok((char*)engine.dataBuffer, "\n");

	while (true)
	{
		sscanf(token, "%d %[^\n\r]", &y, line);
		
		if (y == -1)
		{
			break;
		}

		graphics.drawString(_(line), 320, y, true, graphics.screen);

		token = strtok(NULL, "\n");

		if (token == NULL)
		{
			break;
		}
	}

	graphics.delay(4000);

	graphics.drawString(_("Press Space to Continue..."), 320, 440, true, graphics.screen);

	engine.flushInput();
	engine.clearInput();

	while (true)
	{
		graphics.updateScreen();
		engine.getInput();
		if (engine.keyState[SDLK_SPACE])
			break;
		SDL_Delay(16);
	}
	
	SDL_FillRect(graphics.screen, NULL, graphics.black);
	graphics.delay(1000);
}

bool loadConfig()
{
	std::string version;
	bool rtn = false;

	debug("init", 3) << "Loading config file.." << std::endl;

	if (!PhysFS::Manager::FileExists("config")) return true;

	PhysFS::IStream is("config");

	getline(is, version);

	debug("init", 2) << "Version = '" << version << "'" << std::endl << "  - Expected '" << VERSION << "'" << std::endl;

	// Don't show the license on version change if we're debug
	// Show the license after version change is a stupd thing anyway..
#ifndef DEBUG
	if ((version != VERSION))
	{
		rtn = true;
	}
#endif

	is >> engine.fullScreen;
	is >> game.musicVol;
	is >> game.soundVol;
	is >> game.output;
	is >> game.brightness;
	is >> engine.extremeAvailable;
	is >> game.gore;


	debug("init", 3) << "Extreme Mode = " << engine.extremeAvailable << std::endl;
	debug("init", 3) << "Output Type  = " << game.output << std::endl;

	// Override audio if there is no sound available
	if ((engine.useAudio) && (!game.output))
	{
		engine.useAudio = game.output;
	}
		
	engine.loadKeyConfig();
	engine.loadJoystickConfig();

	return rtn;
}

void saveConfig()
{
	PhysFS::OStream os("config");
	
	os << VERSION << std::endl;
	os << engine.fullScreen << " " << game.musicVol << " ";
	os << game.soundVol << " " << game.output << " ";
	os << game.brightness << " " << engine.extremeAvailable << " " << game.gore;
	
	debug("init", 3) << "Output Type = " << game.output << std::endl;
}

/*
Chugg chugg chugg.... brrr... chugg chugg chugg...brrrrrr... chugg ch..
BRRRRRRRRRRRRRRRRRMMMMMMMMMMMMMMMMMMM!! Well, hopefully anyway! ;)
*/
TinyGetText::DictionaryManager dictionary_manager;

void initSystem(const char * argv0)
{
	debug("init", 1) << "Initializing begin" << std::endl;

	debug("init", 3) << "Initializing PhysFS" << std::endl;
	PhysFS::Manager::Init(argv0);

	debug("init", 3) << "Initializing TinyGetText" << std::endl;
	dictionary_manager.add_directory("locale");
	dictionary_manager.set_charset("UTF-8");

	bool displayLicense = loadConfig();
	
	long flags = SDL_INIT_VIDEO|SDL_INIT_JOYSTICK;
			
	if (engine.useAudio)
	{
		flags |= SDL_INIT_AUDIO;
	}

	debug("init", 3) << "Initializing SDL" << std::endl;
	/* Initialize the SDL library */
	if (SDL_Init(flags) < 0)
	{
		error << "Couldn't initialize SDL: " << SDL_GetError() << std::endl << "Game cannot continue, exiting.." << std::endl;
		exit(1);
	}

	if (!engine.fullScreen)
	{
		graphics.screen = SDL_SetVideoMode(640, 480, 32, SDL_HWPALETTE | SDL_RESIZABLE);
	}
	else
	{
		graphics.screen = SDL_SetVideoMode(640, 480, 32, SDL_HWPALETTE | SDL_FULLSCREEN | SDL_RESIZABLE);
	}

	if (graphics.screen == NULL)
	{
		error << "Couldn't set 640x480 video mode: " << SDL_GetError() << std::endl << "Game cannot continue, exiting.." << std::endl;
		exit(1);
	}

	// This (attempts to) set the gamma correction. We attempt to catch an error here
	// in case someone has done something really stupid in the config file(!!)
    if (game.brightness != -1) {
        Math::limitInt(&game.brightness, 1, 20);
        float brightness = game.brightness;
        brightness /= 10;
        SDL_SetGamma(brightness, brightness, brightness);
    }

	debug("init", 3) << "Initializing SDL_TTF" << std::endl;
	if (TTF_Init() < 0)
	{
		error << "Couldn't initialize SDL TTF: " << SDL_GetError() << std::endl << "Game cannot continue, exiting.." << std::endl;
		exit(1);
	}

	if (engine.useAudio)
	{
		debug("init", 3) << "Initializing audio" << std::endl;
		if (Mix_OpenAudio(48000 /*22050*/, AUDIO_S16, engine.useAudio, 1024) < 0)
		{
			warning << "Couldn't set 48 kHz 16-bit audio - Reason: " << Mix_GetError() << std::endl
					<< "Sound and Music will be disabled" << std::endl;
			engine.useAudio = 0;
		}
	}

	debug("init", 2) << "Found " << SDL_NumJoysticks() << " Joysticks.." << std::endl;

	if (SDL_NumJoysticks() > 0)
	{
		SDL_JoystickEventState(SDL_ENABLE);

		for (int i = 0 ; i < SDL_NumJoysticks() ; i++)
		{
			debug("init", 3) << "Opening Joystick " << i << " - '" << SDL_JoystickName(i) << "'" << std::endl;
			engine.joystick[i] = SDL_JoystickOpen(i);
			
			if (i == 1)
			{
				break;
			}
		}
	}

	debug("init", 3) << "Initializing other ingame classes, RTSL to know what!" << std::endl;
	SDL_ShowCursor(SDL_DISABLE);
	SDL_EventState(SDL_MOUSEMOTION, SDL_DISABLE);

	graphics.registerEngine(&engine);
	graphics.mapColors();

 	audio.registerEngine(&engine);
	audio.setSoundVolume(game.soundVol);
	audio.setMusicVolume(game.musicVol);
	audio.output = game.output;

	debug("init", 3) << "Sound Volume = " << game.soundVol << std::endl;
	debug("init", 3) << "Music Volume = " << game.musicVol << std::endl;
	debug("init", 3) << "Output Type  = " << game.output << std::endl;
	debug("init", 3) << "Brightness   = " << game.brightness << std::endl;

	debug("init", 3) << "Trying to load fonts.." << std::endl;
			
	graphics.loadFont(0, "data/freesans.ttf", 12);
	graphics.loadFont(1, "data/freesans.ttf", 16);
	graphics.loadFont(2, "data/freesans.ttf", 20);
	graphics.loadFont(3, "data/freesans.ttf", 24);
	graphics.loadFont(4, "data/freesans.ttf", 28);

	FontEngine::FontSizeMap font_sizes;
	font_sizes[0] = 12;
	font_sizes[1] = 16;
	font_sizes[2] = 20;
	font_sizes[3] = 24;
	font_sizes[4] = 28;
	font_manager.MapOldSizes(font_sizes);
	font_manager.LoadFont("data/freesans.ttf", "default");
	
	debug("init", 3) << "Font sizes all loaded!!" << std::endl;

	audio.loadSound(SND_CHEAT, "sound/Lock And Load!!!");
	audio.loadSound(SND_HIGHLIGHT, "sound/menu.wav");
	audio.loadSound(SND_SELECT, "sound/select.wav");

	SDL_Surface *logo = graphics.loadImage("gfx/main/dbw_head.png", false);

	SDL_WM_SetIcon(logo, NULL);
	SDL_WM_SetCaption("DirtY BLoB WaRs", "DirtY BLoB WaRs");
	SDL_EnableKeyRepeat(350, 80);

	SDL_FreeSurface(logo);
	
	if (displayLicense)
	{
		showLicense();
	}
	else
	{
		checkForLicense();
	}
		
	srand(time(NULL));
	
	engine.saveConfig = true;

	debug("init", 2) << "Init Complete..." << std::endl;
}

/*
Removes [hopefully] all the resources that has been
loaded and created during the game. This is called by
atexit();
*/
void cleanup()
{
	debug("init", 2) << "Cleaning Up..." << std::endl;

	debug("init", 3) << "Freeing Audio..." << std::endl;
	audio.destroy();

	debug("init", 3) << "Freeing Game Info..." << std::endl;
	game.destroy();

	debug("init", 3) << "Freeing Map Data..." << std::endl;
	map.destroy();

	debug("init", 3) << "Freeing Engine Data..." << std::endl;
	engine.destroy();

	debug("init", 3) << "Freeing Graphics..." << std::endl;
	graphics.destroy();

	if (engine.saveConfig)
	{
		debug("init", 3) << "Saving Config..." << std::endl;
		saveConfig();
	}

	if (SDL_NumJoysticks() > 0)
	{
		SDL_JoystickEventState(SDL_DISABLE);
		for (int i = 0 ; i < SDL_NumJoysticks() ; i++)
		{
			debug("init", 3) << "Closing Joystick " << i << " - '" << SDL_JoystickName(i) << "'" << std::endl;
			SDL_JoystickClose(engine.joystick[i]);
		}
	}

	if (engine.useAudio)
	{
		debug("init", 3) << "Closing audio.." << std::endl;
		Mix_CloseAudio();
	}

	debug("init", 3) << "Closing TTF..." << std::endl;
	font_manager.~Manager();
	TTF_Quit();

	debug("init", 3) << "Closing SDL..." << std::endl;
	SDL_Quit();
	
	debug("init", 3) << "Freeing PhysFS data.." << std::endl;
	PhysFS::Manager::DeInit();

	debug("init", 2) << "All Done. (probably)" << std::endl;
}

