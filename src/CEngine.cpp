/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "CData.h"
#include "CEngine.h"
#include "CGraphics.h"
#include "CMath.h"
#include "config.h"
#include "gettext.hpp"
#include "console/console.hpp"
#include "physfs/manager.hpp"

Engine::Engine()
{
	for (int i = 0 ; i < 350 ; i++)
	{
		keyState[i] = 0;
	}

	joyX = joyY = 0;
	joyNavAvail = 0;

	mouseLeft = mouseRight = 0;
	waitForButton = false;
	waitForKey = false;
	
	joystick[0] = joystick[1] = NULL;
	allowJoypad = true;

	strcpy(lastKeyPressed, "");

	fullScreen = 0;

	useAudio = 2;
	
	practice = false;
	
	allowQuit = false;

	saveConfig = false;

	highlightedWidget = NULL;

	strcpy(message, "");
	messageTime = -1;

	// Development Stuff
	devNoMonsters = false;

	dataBuffer = NULL;

	// Timer
	time1 = time2 = 0;
	timeDifference = 0;

	// Cheats
	memset(lastKeyEvents, ' ', 25);

	cheats = false;

	clearCheatVars();
	
	extremeAvailable = 0;
}

void Engine::destroy()
{
	debug("engine", 1) << "Destroying engine.." << std::endl;
	debug("engine", 2) << "engine: free widgets" << std::endl;
	deleteWidgets();

	debug("engine", 2) << "engine: free databuffer" << std::endl;
	if (dataBuffer != NULL)
		delete[] dataBuffer;

	debug("engine", 2) << "engine: free binarybuffer" << std::endl;

	debug("engine", 2) << "Clearing Define List..." << std::endl;
	defineList.clear();
}

void Engine::clearCheatVars()
{
	memset(lastKeyEvents, ' ', 25);
	cheatHealth = cheatExtras = cheatFuel = cheatLevels = false;
	cheatBlood = cheatInvulnerable = cheatReload = cheatSpeed = cheatSkipLevel = false;
	cheat_no_out = false;
}

bool Engine::compareLastKeyInputs()
{
	#if DEMO
		return false;
	#endif

	if (strstr(lastKeyEvents, "lockandload"))
	{
		cheats = true;
		return true;
	}

	return false;
}

bool Engine::loadJoystickConfig()
{
	debug("engine", 2) << "Loading joystick config from joystick.cfg" << std::endl;
	
	if (!PhysFS::Manager::FileExists("joystick.cfg")) return false;

	PhysFS::Manager::ReadFileIntoPointer("joystick.cfg", (char *) &joypad, sizeof(Joystick));
	
	return true;
}

bool Engine::saveJoystickConfig()
{
	debug("engine", 2) << "Saving joystick config to joystick.cfg" << std::endl;
	
	PhysFS::Manager::WriteFile("joystick.cfg", (char *) &joypad, sizeof(Joystick));
	
	return true;
}

bool Engine::loadKeyConfig()
{
	debug("engine", 2) << "Loading keyboard config from keyboard.cfg" << std::endl;
	
	if (!PhysFS::Manager::FileExists("keyboard.cfg")) return false;

	PhysFS::Manager::ReadFileIntoPointer("keyboard.cfg", (char *) &keyboard, sizeof(Keyboard));
	
	return true;
}

bool Engine::saveKeyConfig()
{
	debug("engine", 2) << "Saving keyboard config to keyboard.cfg" << std::endl;
	
	PhysFS::Manager::WriteFile("keyboard.cfg", (char *) &keyboard, sizeof(keyboard));
	
	return true;
}

void Engine::restoreKeyDefaults()
{
	keyboard.setDefaultKeys();
}

bool Engine::isKey(int key)
{
	switch (key)
	{
		case KEY_UP:
			return keyState[keyboard.jump];
			break;
		case KEY_DOWN:
			return keyState[keyboard.down];
			break;
		case KEY_LEFT:
			return keyState[keyboard.left];
			break;
		case KEY_RIGHT:
			return keyState[keyboard.right];
			break;
		case KEY_FIRE:
			return keyState[keyboard.fire];
			break;
		case KEY_MAP:
			return keyState[keyboard.map];
			break;
		case KEY_PAUSE:
			return keyState[keyboard.pause];
			break;
		case KEY_JETPACK:
			return keyState[keyboard.jetpack];
			break;
	}
	
	return false;
}

void Engine::clearKey(int key)
{
	switch (key)
	{
		case KEY_UP:
			keyState[keyboard.jump] = 0;
			break;
		case KEY_DOWN:
			keyState[keyboard.down] = 0;
			break;
		case KEY_LEFT:
			keyState[keyboard.left] = 0;
			break;
		case KEY_RIGHT:
			keyState[keyboard.right] = 0;
			break;
		case KEY_FIRE:
			keyState[keyboard.fire] = 0;
			break;
		case KEY_MAP:
			keyState[keyboard.map] = 0;
			break;
		case KEY_PAUSE:
			keyState[keyboard.pause] = 0;
			break;
		case KEY_JETPACK:
			keyState[keyboard.jetpack] = 0;
			break;
	}
}

#define JOY_FLAKINESS 8

bool Engine::getJoyLeft()
{
	if (SDL_GetTicks() < joyNavAvail)
	{
		return false;
	}
	
	return (joyX / JOY_FLAKINESS) < 0;
}

bool Engine::getJoyRight()
{
	if (SDL_GetTicks() < joyNavAvail)
	{
		return false;
	}
	
	return (joyX / JOY_FLAKINESS) > 0;
}

bool Engine::getJoyUp()
{
	if (SDL_GetTicks() < joyNavAvail)
	{
		return false;
	}
	
	return (joyY / JOY_FLAKINESS) < 0;
}

bool Engine::getJoyDown()
{
	if (SDL_GetTicks() < joyNavAvail)
	{
		return false;
	}
	
	return (joyY / JOY_FLAKINESS) > 0;
}

int Engine::getJoyX(int max)
{
  return int((double(joyX) / 32768.0) * double(max));
}

int Engine::getJoyY(int max)
{
  return int((double(joyY) / 32768.0) * double(max));
}

void Engine::lockJoy(Uint32 ticks)
{
	Uint32 newAvail = SDL_GetTicks() + ticks;

	if (newAvail > joyNavAvail)
	{
		joyNavAvail = newAvail;
	}
}

void Engine::addKeyEvent()
{
	if (strlen(lastKeyPressed) > 1)
	{
		return;
	}

	int index = -1;

	for (int i = 0 ; i < 25 ; i++)
	{
		if (lastKeyEvents[i] == ' ')
		{
			index = i;
			break;
		}
	}

	if (index == -1)
	{
		for (int i = 0 ; i < 25 ; i++)
		{
			lastKeyEvents[i] = lastKeyEvents[i + 1];
		}

		index = 24;
	}

	lastKeyEvents[index] = lastKeyPressed[0];

	compareLastKeyInputs();
}

void Engine::getInput()
{
	SDL_GetMouseState(&mouseX, &mouseY);

	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_QUIT:
				if (allowQuit)
				{
					exit(0);
				}
				break;

			case SDL_VIDEORESIZE:
				graphics.width  = std::max(640, event.resize.w);
				graphics.height = std::max(480, event.resize.h);
				graphics.screen = SDL_SetVideoMode(graphics.width, graphics.height, 32, SDL_HWPALETTE | SDL_RESIZABLE);
				graphics.resize();
				break;

			case SDL_MOUSEBUTTONDOWN:
				if (event.button.button == SDL_BUTTON_LEFT) mouseLeft = 1;
				if (event.button.button == SDL_BUTTON_RIGHT) mouseRight = 1;
				break;

			case SDL_MOUSEBUTTONUP:
				if (event.button.button == SDL_BUTTON_LEFT) mouseLeft = 0;
				if (event.button.button == SDL_BUTTON_RIGHT) mouseRight = 0;
				break;

			case SDL_KEYDOWN:
				
				if (waitForButton)
				{
					if (event.key.keysym.sym == SDLK_ESCAPE)
					{
						lastButtonPressed = -1;
						*highlightedWidget->value = abs(*highlightedWidget->value) - 1000;
						highlightedWidget->redraw();
						waitForButton = false;
						allowJoypad = false;
					}
					
					return;
				}
				
				if (waitForKey)
				{
					if (event.key.keysym.sym == SDLK_ESCAPE)
					{
						*highlightedWidget->value = -*highlightedWidget->value;
					}
					else
					{
						*highlightedWidget->value = event.key.keysym.sym;
					}
					
					lastButtonPressed = -1;
					highlightedWidget->redraw();
					waitForButton = false;
					waitForKey = false;
					allowJoypad = false;
					
					return;
				}

				keyState[event.key.keysym.sym] = 1;
				strcpy(lastKeyPressed, SDL_GetKeyName(event.key.keysym.sym));
				addKeyEvent();
				break;

			case SDL_KEYUP:
				keyState[event.key.keysym.sym] = 0;
				break;

			case SDL_JOYAXISMOTION:
				if (event.jaxis.axis == 0)
				{
				  joyX=event.jaxis.value;
				}
				else if (event.jaxis.axis == 1)
				{
				  joyY=event.jaxis.value;
				}
				break;

			case SDL_JOYBUTTONDOWN:
				if (waitForButton)
				{
					lastButtonPressed = event.jbutton.button;
					*highlightedWidget->value = lastButtonPressed;
					highlightedWidget->redraw();
					waitForButton = false;
					allowJoypad = false;
					return;
				}
				
				if (event.jbutton.button == joypad.fire)
					keyState[keyboard.fire] = 1;
				else if (event.jbutton.button == joypad.jump)
					keyState[keyboard.jump] = 1;
				else if (event.jbutton.button == joypad.jetpack)
					keyState[keyboard.jetpack] = 1;
				else if (event.jbutton.button == joypad.map)
					keyState[keyboard.map] = 1;
				else if (event.jbutton.button == joypad.pause)
					keyState[keyboard.pause] = 1;
				break;

			case SDL_JOYBUTTONUP:
				if (event.jbutton.button == joypad.fire)
					keyState[keyboard.fire] = 0;
				else if (event.jbutton.button == joypad.jump)
					keyState[keyboard.jump] = 0;
				else if (event.jbutton.button == joypad.jetpack)
					keyState[keyboard.jetpack] = 0;
				else if (event.jbutton.button == joypad.map)
					keyState[keyboard.map] = 0;
				else if (event.jbutton.button == joypad.pause)
					keyState[keyboard.pause] = 0;
				break;

			default:
				break;
		}
	}
}

int Engine::getMouseX()
{
	return mouseX;
}

int Engine::getMouseY()
{
	return mouseY;
}

void Engine::setMouse(int x, int y)
{
	SDL_WarpMouse(x, y);
}

bool Engine::userAccepts()
{
	if ((keyState[SDLK_SPACE]) || (keyState[SDLK_ESCAPE]) || (keyState[SDLK_LCTRL]) || (keyState[SDLK_RCTRL]) || (keyState[SDLK_RETURN]) || (keyState[SDLK_LCTRL]))
	{
		return true;
	}

	return false;
}

void Engine::doPause()
{
	if (!paused)
	{
		if (keyState[keyboard.pause])
		{
			paused = true;
			keyState[keyboard.pause] = 0;
		}
	}
	else
	{
		if ((keyState[keyboard.pause]) || (keyState[SDLK_ESCAPE]))
		{
			paused = false;
			keyState[keyboard.pause] = 0;
			keyState[SDLK_ESCAPE] = 0;
		}
	}
}

void Engine::flushInput()
{
	while (SDL_PollEvent(&event)){}
}

void Engine::clearInput()
{
	for (int i = 0 ; i < 350 ; i++)
		keyState[i] = 0;

	mouseLeft = mouseRight = 0;
}

bool Engine::loadData(const char *filename)
{
	if (dataBuffer != NULL)
	{
		delete[] dataBuffer;
		dataBuffer = NULL;
	}
	

	int fSize = PhysFS::Manager::ReadFile(filename, &dataBuffer);

	debug("engine", 3) << "loadData() : Loaded '" << filename << "' (" << fSize << ")" << std::endl;

	return true;
}

void Engine::reportFontFailure()
{
	error <<_("Unable to load font. The game cannot continue without it.") << std::endl;
	error << _("Please confirm that the game and all required depencies are installed") << std::endl;

	exit(1);
}

void Engine::setPlayerPosition(int x, int y, int limitLeft, int limitRight, int limitUp, int limitDown)
{
	playerPosX = x - OFFSETX;
	playerPosY = y - OFFSETY;

	Math::limitInt(&playerPosX, limitLeft, limitRight);
	Math::limitInt(&playerPosY, limitUp, limitDown);
}

int Engine::getFrameLoop()
{
	return frameLoop;
}

void Engine::doFrameLoop()
{
	Math::wrapChar(&(++frameLoop), 0, 59);
}

void Engine::doTimeDifference()
{
	timeDifference = (time2 - time1) / 10.0;
	time1 = time2;
	time2 = SDL_GetTicks();
}

float Engine::getTimeDifference()
{
	return timeDifference;
}

void Engine::resetTimeDifference()
{
	time1 = time2 = SDL_GetTicks();
}

void Engine::setInfoMessage(const char *message, int priority, int type)
{
	if (priority >= messagePriority)
	{
		strcpy(this->message, message);
		messageTime = 180;
		messagePriority = priority;
		messageType = type;
	}
}

void Engine::deleteWidgets()
{
	Widget *widget;

	for (widget = (Widget*)widgetList.getHead()->next ; widget != NULL ; widget = (Widget*)widget->next)
		widget->redraw();

	widgetList.clear();

	highlightedWidget = NULL;
}

void Engine::addWidget(Widget *widget)
{
	widget->previous = (Widget*)widgetList.getTail();
	widgetList.add(widget);
}

bool Engine::loadWidgets(const char *filename)
{
	deleteWidgets();

	if (!loadData(filename))
		return false;

	char token[50], name[50], groupName[50], label[80], options[100], *line;
	int x, y, min, max;

	int i;

	Widget *widget;

	line = strtok((char*)dataBuffer, "\n");

	while (true)
	{
		sscanf(line, "%s", token);

		if (strcmp(token, "END") == 0)
			break;

		sscanf(line, "%*s %s %s %*c %[^\"] %*c %*c %[^\"] %*c %d %d %d %d", name, groupName, label, options, &x, &y, &min, &max);

		widget = new Widget;

		i = 0;

		while (true)
		{
			if (strcmp(token, widgetName[i]) == 0)
				widget->type = i;

			if (strcmp("-1", widgetName[i]) == 0)
				break;

			i++;
		}

		widget->setProperties(name, groupName, label, options, x, y, min, max);

		addWidget(widget);


		if ((line = strtok(NULL, "\n")) == NULL)
			break;
	}

	highlightedWidget = (Widget*)widgetList.getHead()->next;

	return true;
}

Widget *Engine::getWidgetByName(const char *name)
{
	Widget *widget = (Widget*)widgetList.getHead();

	while (widget->next != NULL)
	{
		widget = (Widget*)widget->next;

		if (strcmp(widget->name, name) == 0)
			return widget;
	}

	warning << "No such widget '" << name << "'" << std::endl;

	return NULL;
}

void Engine::showWidgetGroup(const char *groupName, bool show)
{
	bool found = false;

	Widget *widget = (Widget*)widgetList.getHead();

	while (widget->next != NULL)
	{
		widget = (Widget*)widget->next;

		if (strcmp(widget->groupName, groupName) == 0)
		{
			widget->visible = show;
			widget->redraw();
			found = true;
		}
	}

	if (!found)
		warning << "Group '" << groupName << "' does not exist" << std::endl;
}

void Engine::enableWidgetGroup(const char *groupName, bool show)
{
	bool found = false;

	Widget *widget = (Widget*)widgetList.getHead();

	while (widget->next != NULL)
	{
		widget = (Widget*)widget->next;

		if (strcmp(widget->groupName, groupName) == 0)
		{
			widget->enabled = show;
			widget->redraw();
			found = true;
		}
	}

	if (!found)
		warning << "Group '" << groupName << "' does not exist" << std::endl;
}

void Engine::showWidget(const char *name, bool show)
{
	Widget *widget = getWidgetByName(name);
	if (widget != NULL)
	{
		widget->visible = show;
		widget->redraw();
	}
}

void Engine::enableWidget(const char *name, bool enable)
{
	Widget *widget = getWidgetByName(name);
	if (widget != NULL)
	{
		widget->enabled = enable;
		widget->redraw();
	}
}

void Engine::setWidgetVariable(const char *name, int *variable)
{
	Widget *widget = getWidgetByName(name);
	if (widget != NULL)
		widget->value = variable;
}

bool Engine::widgetChanged(const char *name)
{
	Widget *widget = getWidgetByName(name);
	if (widget != NULL)
		return widget->changed;

	return false;
}

void Engine::highlightWidget(int dir)
{
	highlightedWidget->redraw();

	if (dir == 1)
	{
		while (true)
		{
			if (highlightedWidget->next != NULL)
			{
				highlightedWidget = (Widget*)highlightedWidget->next;
			}
			else
			{
				highlightedWidget = (Widget*)widgetList.getHead()->next;
			}

			if (highlightedWidget->type == 4)
				continue;

			if ((highlightedWidget->enabled) && (highlightedWidget->visible))
				break;
		}
	}

	if (dir == -1)
	{
		while (true)
		{
			if ((highlightedWidget->previous != NULL) && (highlightedWidget->previous != (Widget*)widgetList.getHead()))
			{
				highlightedWidget = highlightedWidget->previous;
			}
			else
			{
				highlightedWidget = (Widget*)widgetList.getTail();
			}

			if (highlightedWidget->type == WG_LABEL)
				continue;

			if ((highlightedWidget->enabled) && (highlightedWidget->visible))
				break;
		}
	}

	highlightedWidget->redraw();
}

void Engine::highlightWidget(const char *name)
{
	highlightedWidget = getWidgetByName(name);
}

int Engine::processWidgets()
{
	int update = 0;

	if (keyState[SDLK_UP] || getJoyUp())
	{
		highlightWidget(-1);
		update = 1;
		clearInput();

		// Hack because there's no central event loop.  Without this,
		// joystick navigation is impossible.
		lockJoy(300);
	}

	if (keyState[SDLK_DOWN] || getJoyDown())
	{
		highlightWidget(1);
		update = 1;
		clearInput();

		// Hack because there's no central event loop.  Without this,
		// joystick navigation is impossible.
		lockJoy(300);
	}

	if (keyState[SDLK_LEFT] ||
	    // Allow manipulation of everything but pushbuttons using
	    // joypad-left.
	    (highlightedWidget->type != WG_BUTTON &&
	     highlightedWidget->type != WG_JOYPAD &&
	     getJoyLeft()))
	{
		if (*highlightedWidget->value > highlightedWidget->min)
		{
			*highlightedWidget->value = *highlightedWidget->value - 1;
			update = 3;
			if ((highlightedWidget->type == WG_RADIO) || (highlightedWidget->type == WG_SLIDER))
				update = 1;
			highlightedWidget->changed = true;
		}

		if ((highlightedWidget->type == WG_RADIO) || (highlightedWidget->type == WG_SLIDER))
			clearInput();

		// Hack because there's no central event loop.  Without this,
		// joystick navigation is impossible.
		lockJoy(300);
	}

	if (keyState[SDLK_RIGHT] ||
	    // Allow manipulation of everything but pushbuttons using
	    // joypad-right.
	    (highlightedWidget->type != WG_BUTTON &&
	     highlightedWidget->type != WG_JOYPAD &&
	     getJoyRight()))
	{
		if (*highlightedWidget->value < highlightedWidget->max)
		{
			*highlightedWidget->value = *highlightedWidget->value + 1;
			update = 3;
			if ((highlightedWidget->type == WG_RADIO) || (highlightedWidget->type == WG_SLIDER))
				update = 1;
			highlightedWidget->changed = true;
		}

		if ((highlightedWidget->type == WG_RADIO) || (highlightedWidget->type == WG_SLIDER))
			clearInput();

		// Hack because there's no central event loop.  Without this,
		// joystick navigation is impossible.
		lockJoy(300);
	}

	if ((keyState[SDLK_RETURN]) || (keyState[SDLK_SPACE]) || (keyState[SDLK_LCTRL]))
	{
		if (highlightedWidget->value == NULL)
		{
			warning << "'" << highlightedWidget->name << "' has not been implemented!" << std::endl;
		}
		else
		{
			if (highlightedWidget->type == WG_BUTTON)
			{
				*highlightedWidget->value = 1;
				highlightedWidget->changed = true;
			}
			else if (highlightedWidget->type == WG_JOYPAD)
			{
				waitForButton = true;
				waitForKey = false;
				allowJoypad = true;
				
				if (*highlightedWidget->value > -1000)
				{
					*highlightedWidget->value = (-1000 - *highlightedWidget->value);
				}
			}
			else if (highlightedWidget->type == WG_KEYBOARD)
			{
				waitForKey = true;
				waitForButton = false;
				allowJoypad = false;
				*highlightedWidget->value = -*highlightedWidget->value;
			}
			
			update = 2;
		}
		

		flushInput();
		clearInput();

		// Hack because there's no central event loop.  Without this,
		// joystick navigation is impossible.
		lockJoy(300);
	}

	return update;
}

#ifndef UNIX
char *strtok_r(char *s1, const char *s2, char **lasts)
{
	char *ret;
	
	if (s1 == NULL)
	{
		s1 = *lasts;
	}
	
	while (*s1 && strchr(s2, *s1))
	{
		++s1;
	}
	
	if (*s1 == '\0')
	{
		return NULL;
	}
	
	ret = s1;
	
	while(*s1 && !strchr(s2, *s1))
	{
		++s1;
	}
	
	if(*s1)
	{
		*s1++ = '\0';
	}
	
	*lasts = s1;
	
	return ret;
}
#endif

/*
Loads key-value defines into a linked list, comments are ignored. The defines.h file is used by the
game at compile time and run time, so everything is syncronised. This technique has the advantage of
allowing the game's data to be human readable and easy to maintain.
*/
bool Engine::loadDefines()
{
	char string[2][1024];

	if (!loadData("data/defines.h"))
		return false;

	char *token = strtok((char*)dataBuffer, "\n");

	Data *data;

	while (true)
	{
		token = strtok(NULL, "\n");
		if (!token)
			break;

		if (!strstr(token, "/*"))
		{
			sscanf(token, "%*s %s %[^\n\r]", string[0], string[1]);
			data = new Data();
			data->set(string[0], string[1], 1, 1);
			defineList.add(data);
		}
	}

	return true;
}

/*
Returns the value of a #defined value... ACTIVE is declared as 1 so it will
traverse the list and return 1 when it encounters ACTIVE. This has two advantages.
1) It makes the game data human readable and 2) It means if I change a #define in
the code, I don't have to change all the data entries too. You probably already
thought of that though... :)
*/
int Engine::getValueOfDefine(const char *word)
{
	int rtn = 0;

	Data *data = (Data*)defineList.getHead();

	while (data->next != NULL)
	{
		data = (Data*)data->next;

		if (strcmp(data->key, word) == 0)
		{
			rtn = atoi(data->value);
			return rtn;
		}
	}

	error << "getValueOfDefine() : '" << word << "' is not defined!" << std::endl;
	return 0;
}

/*
Does the opposite of the above(!)
*/
const char *Engine::getDefineOfValue(const char *prefix, int value)
{
	int rtn = 0;

	Data *data = (Data*)defineList.getHead();

	while (data->next != NULL)
	{
		data = (Data*)data->next;

		if (strstr(data->key, prefix))
		{
			rtn = atoi(data->value);
			
			if (rtn == value)
			{
				return data->key;
			}
		}
	}

	error << "getDefineOfValue() : '" << prefix << "', " << value << "is not defined!" << std::endl;
	return prefix;
}

/*
I like this function. It receives a list of flags declared by their #define name... like
the function above, delimited with plus signs. So ENT_FLIES+ENT_AIMS. It then works out the
flags (in a bit of a half arsed manner because of my lazy (2 << 25) declarations, adds all
the values together and then returns them... phew! Makes data files human readable though :)
*/
int Engine::getValueOfFlagTokens(const char *realLine)
{
	if (strcmp(realLine, "0") == 0)
		return 0;

	char *store;
	char line[1024];
	bool found;
	int value;
	strcpy(line, realLine);

	int flags = 0;

	char *word = strtok_r(line, "+", &store);

	if (!word)
	{
		error << "getValueOfFlagTokens() : NULL Pointer!" << std::endl;
		return 0;
	}

	Data *data;

	while (true)
	{
		data = (Data*)defineList.getHead();
		found = false;

		while (data->next != NULL)
		{
			data = (Data*)data->next;

			if (strcmp(data->key, word) == 0)
			{
				value = -1;
				sscanf(data->value, "%d", &value);

				if (value == -1)
				{
					sscanf(data->value, "%*s %*d %*s %d", &value);
					value = 2 << value;
				}

				flags += value;
				found = true;
				break;
			}
		}

		if (!found)
		{
			error << "getValueOfFlagTokens() : Illegal Token '" << word << "'" << std::endl;
			break;
		}

		word = strtok_r(NULL, "+", &store);
		if (!word)
			break;
	}

	return flags;
}

void Engine::delay(unsigned int frameLimit)
{
	unsigned int ticks = SDL_GetTicks();

	if(frameLimit < ticks)
		return;
	
	if(frameLimit > ticks + 16)
		SDL_Delay(16);
	else
		SDL_Delay(frameLimit - ticks);
}
