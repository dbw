/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <list>
#include <stdexcept>

#include "CAudio.h"
#include "CEngine.h"
#include "CGame.h"
#include "CGraphics.h"
#include "CMap.h"
#include "CMath.h"
#include "config.h"
#include "console/console.hpp"
#include "gettext.hpp"
#include "lisp/lisp.hpp"
#include "lisp/parser.hpp"
#include "lisp/list_iterator.hpp"
#include "title.h"


/**
* Displays the skill level widgets and hide the others
*/
void showSkillLevels()
{
	engine.showWidgetGroup("skill", true);
	engine.showWidgetGroup("mainMenu", false);
	engine.showWidgetGroup("gameSlots", false);

	engine.showWidget("back", true);

	engine.highlightWidget("normal");
		  
	engine.enableWidget("extreme", engine.extremeAvailable);
}

/**
* Displays the save game widgets and hide the others
*/
void showGameSlots()
{
	engine.showWidgetGroup("gameSlots", true);
	engine.showWidgetGroup("mainMenu", false);

	engine.showWidget("back", true);

	engine.highlightWidget("slot1");
}

void showSaves()
{
	engine.showWidgetGroup("saveGames", true);
	engine.showWidgetGroup("mainMenu", false);

	engine.showWidget("back", true);

	engine.highlightWidget("save1");
}

/**
* Displays the manual widgets and hide the others
*/
void showManualHelp()
{
	#ifdef FRAMEWORK_SDL
	openHelpURL();
	#else
	engine.showWidgetGroup("help", true);
	engine.showWidgetGroup("mainMenu", false);
	
	engine.showWidget("back", true);

	engine.highlightWidget("back");
	#endif
}

/**
* Displays the title widgets and hide the others.
* Also disables Continue and Load Game if games are not
* available to be loaded.
*/
void showTitleWidgets()
{
	engine.showWidgetGroup("skill", false);
	engine.showWidgetGroup("saveGames", false);
	engine.showWidgetGroup("gameSlots", false);
	engine.showWidgetGroup("help", false);
	engine.showWidget("back", false);
	
	engine.showWidgetGroup("mainMenu", true);
	
	engine.highlightWidget("newGame");

	if (!engine.continueSaveSlot)
	{
		engine.enableWidget("loadGame", false);
		engine.enableWidget("continue", false);
	}
	else
	{
		engine.highlightWidget("continue");
	}
}

/**
* Creates the data labels for the save game slots
* according to the save game data available
*/
void setupSaveWidgets()
{
	char widgetName[10];
	strcpy(widgetName, "");
	
	for (int i = 0 ; i < 5 ; i++)
	{
		sprintf(widgetName, "save%d", i + 1);
		strcpy(engine.getWidgetByName(widgetName)->label, engine.saveSlot[i]);
		
		if ((strstr(engine.saveSlot[i], _("Empty").c_str())) || (strstr(engine.saveSlot[i], _("Corrupt").c_str())))
		{
			engine.enableWidget(widgetName, false);
		}
		
		sprintf(widgetName, "slot%d", i + 1);
		strcpy(engine.getWidgetByName(widgetName)->label, engine.saveSlot[i]);
	}
}

/**
* Loads the title widgets
*/
void loadTitleWidgets()
{
	if (!engine.loadWidgets("data/titleWidgets"))
	{
		graphics.showErrorAndExit(ERR_FILE, "data/titleWidgets");
	}

	initSaveSlots();

	setupSaveWidgets();
	
	Widget *widget = engine.getWidgetByName("labelManual");
	strcpy(widget->label, GAMEPLAYMANUAL);

	showTitleWidgets();
}

/**
* Does the main game title sequence
* @return The section to proceed to (see main.cpp)
*/
int title()
{
	audio.free();
	graphics.free();

	SDL_FillRect(graphics.screen, NULL, graphics.black);
	//graphics.delay(1000);

	newGame();

	loadTitleWidgets();

	float backAlpha = 0;
	float titleAlpha = 0;
	bool allFadedOn = false;

	graphics.quickSprite("cheatHeader", graphics.loadImage("gfx/main/cheats.png"));
	graphics.quickSprite("optionsHeader", graphics.loadImage("gfx/main/options.png"));
	graphics.quickSprite("keyHeader", graphics.loadImage("gfx/main/keyConfig.png"));
	graphics.quickSprite("joystickHeader", graphics.loadImage("gfx/main/joystickConfig.png"));
	graphics.quickSprite("optionsBackground", graphics.loadImage("gfx/main/optionsBackground.png"));

	SDL_Surface * par_text = graphics.quickSprite("par_text", graphics.loadImage("gfx/main/par_text.png", false));
	SDL_Surface * ds_logo  = graphics.quickSprite("ds_logo",  graphics.loadImage("gfx/main/ds_logo.png", false));
	SDL_Surface * ds       = graphics.quickSprite("ds",       graphics.loadImage("gfx/main/ds.png", false));
	SDL_Surface * dbw_logo = graphics.quickSprite("dbw_logo", graphics.loadImage("gfx/main/dbw_logo.png", false));
	
	graphics.setFontColor(0xff, 0xff, 0xff, 0x00, 0x00, 0x00);

	graphics.setFontSize(3);
	SDL_Surface * andsign  = graphics.quickSprite("and",      graphics.getString("&", true));
	SDL_Surface * presents = graphics.quickSprite("Presents", graphics.getString(_("Presents"), true));
	SDL_Surface * sdl      = graphics.quickSprite("SDL",      graphics.getString(_("An SDL Game"), true));

	graphics.setFontSize(0);
	SDL_Surface *copyright = graphics.quickSprite("Copyright", graphics.getString("Copyright © 2004, 2005 Parallel Realities", true));
	SDL_Surface *copyright2 = graphics.quickSprite("Copyright2", graphics.getString(_("Copyright © 2007-2008 Kővágó Zoltán, & contributors"), true));

	SDL_Surface *branch  = graphics.quickSprite("branch",  graphics.getString(_("Branch:")  + " " BRANCH,  true));
	SDL_Surface *version = graphics.quickSprite("Version", graphics.getString(_("Version:") + " " VERSION, true));

	//SDL_SetAlpha(title, SDL_SRCALPHA|SDL_RLEACCEL, 0);

	audio.loadMusic("music/helmet-shake.mod");

	graphics.loadBackground("gfx/main/CircuitBoard.jpg");
	SDL_SetAlpha(graphics.background, SDL_SRCALPHA|SDL_RLEACCEL, 0);

	int startNewGame, options, quit, help, easy, normal, hard, extreme, back, practice;
	int load, cont, save[5], slot[6];
	int autoSaveSlot;

	startNewGame = options = quit = easy = normal = hard = extreme = back = help = practice = 0;
	load = cont = save[0] = save[1] = save[2] = save[3] = save[4] = 0;
	slot[0] = slot[1] = slot[2] = slot[3] = slot[4] = slot[5] = 0;
	autoSaveSlot = 0;

	engine.setWidgetVariable("newGame", &startNewGame);
	engine.setWidgetVariable("loadGame", &load);
	engine.setWidgetVariable("continue", &cont);
	engine.setWidgetVariable("options", &options);
	engine.setWidgetVariable("help", &help);
	engine.setWidgetVariable("quit", &quit);

	float offX = 0;

	engine.flushInput();
	engine.clearInput();

	audio.playMusic();

	Uint32 now = SDL_GetTicks();

	unsigned int frameLimit = SDL_GetTicks() + 16;

	while (true)
	{
		graphics.updateScreen();

		engine.getInput();

		SDL_FillRect(graphics.screen, NULL, graphics.black);

		for (int x = (int)offX ; x < graphics.width ; x += graphics.background->w)
		{
			for (int y = 0 ; y < graphics.height ; y += graphics.background->h)
			{
				graphics.blit(graphics.background, x, y, graphics.screen, false);
			}
		}

		Uint32 tick = SDL_GetTicks();

		if (!allFadedOn)
		{
			if (/*(tick >= now + 2000) &&*/ (tick <= now + 10000))
			{
				if (tick >= now + 4000)
				{
					graphics.blit(par_text, graphics.width / 2 - 80, graphics.height / 2 - 220, graphics.screen, true);
					graphics.blit(andsign, graphics.width / 2 + 300, graphics.height / 2 - 220, graphics.screen, true);
					graphics.blit(ds, graphics.width / 2, graphics.height / 2 + 210, graphics.screen, true);
				}
				graphics.blit(ds_logo, graphics.width / 2, graphics.height / 2, graphics.screen, true);
			}
			else if ((tick >= now + 13000) && (tick <= now + 19000))
			{
				graphics.blit(presents, graphics.width/2, graphics.height/2, graphics.screen, true);
			}
			else if ((tick >= now + 22000) && (tick <= now + 27000))
			{
				graphics.blit(sdl, graphics.width/2, graphics.height/2, graphics.screen, true);
			}

			if (engine.userAccepts())
			{
				allFadedOn = true;
				engine.flushInput();
				engine.clearInput();
				backAlpha = 255;
				titleAlpha = 255;
				SDL_SetAlpha(graphics.background, SDL_SRCALPHA|SDL_RLEACCEL, (int)backAlpha);
				//SDL_SetAlpha(title, SDL_SRCALPHA|SDL_RLEACCEL, (int)titleAlpha);
			}
		}

		if ((tick >= now + 4000) || (allFadedOn))
		{
			if (backAlpha < 255)
			{
				backAlpha++;
				SDL_SetAlpha(graphics.background, SDL_SRCALPHA|SDL_RLEACCEL, (int)backAlpha);
			}
		}

		if ((tick >= now + 29000) || (allFadedOn))
		{
			if (titleAlpha < 255)
			{
				titleAlpha++;
				//SDL_SetAlpha(title, SDL_SRCALPHA|SDL_RLEACCEL, (int)titleAlpha);
			}
			else
			{
				graphics.blit(copyright, 10, graphics.height - 40, graphics.screen, false);
				graphics.blit(copyright2,10, graphics.height - 20, graphics.screen, false);
				graphics.blit(branch,  (graphics.width - 10 - branch->w),  graphics.height - 40, graphics.screen, false);
				graphics.blit(version, (graphics.width - 10 - version->w), graphics.height - 20, graphics.screen, false);
				allFadedOn = true;
			}
		}

		Math::wrapFloat(&(offX -= 0.25), -graphics.background->w, 0);

		if (titleAlpha > 0)
		{
			graphics.blit(dbw_logo, graphics.width/2, 120, graphics.screen, true);
		}

		if (allFadedOn)
		{
			drawWidgets();
			audio.playMenuSound(engine.processWidgets());
		}

		if (options)
		{
			showOptions();
			loadTitleWidgets();
			engine.setWidgetVariable("newGame", &startNewGame);
			engine.setWidgetVariable("loadGame", &load);
			engine.setWidgetVariable("continue", &cont);
			engine.setWidgetVariable("options", &options);
			engine.setWidgetVariable("help", &help);
			engine.setWidgetVariable("quit", &quit);
			options = 0;
			engine.flushInput();
		}

		if (help)
		{
			showManualHelp();
			engine.setWidgetVariable("back", &back);
			help = 0;
		}

		if (startNewGame)
		{
			showGameSlots();
			engine.setWidgetVariable("slot1", &slot[0]);
			engine.setWidgetVariable("slot2", &slot[1]);
			engine.setWidgetVariable("slot3", &slot[2]);
			engine.setWidgetVariable("slot4", &slot[3]);
			engine.setWidgetVariable("slot5", &slot[4]);
			engine.setWidgetVariable("slot6", &slot[5]);
			engine.setWidgetVariable("back", &back);
			startNewGame = 0;
		}

		for (int i = 0 ; i < 6 ; i++)
		{
			if (slot[i])
			{
				showSkillLevels();
				engine.setWidgetVariable("practice", &practice);
				engine.setWidgetVariable("easy", &easy);
				engine.setWidgetVariable("normal", &normal);
				engine.setWidgetVariable("hard", &hard);
				engine.setWidgetVariable("extreme", &extreme);
				engine.setWidgetVariable("back", &back);
				autoSaveSlot = i;
				slot[i] = 0;
			}
		}

		if (load)
		{
			showSaves();
			engine.setWidgetVariable("save1", &save[0]);
			engine.setWidgetVariable("save2", &save[1]);
			engine.setWidgetVariable("save3", &save[2]);
			engine.setWidgetVariable("save4", &save[3]);
			engine.setWidgetVariable("save5", &save[4]);
			engine.setWidgetVariable("back", &back);
			load = 0;
		}

		if (easy) {engine.skill = 0; break;}
		if (normal) {engine.skill = 1; break;}
		if (hard) {engine.skill = 2; break;}
		if (extreme) {engine.skill = 3; break;}
		if (practice) {engine.skill = -1; break;}

		if (cont) {load = engine.continueSaveSlot; break;}

		if (save[0]) {load = 1; break;}
		if (save[1]) {load = 2; break;}
		if (save[2]) {load = 3; break;}
		if (save[3]) {load = 4; break;}
		if (save[4]) {load = 5; break;}

		if (engine.keyState[SDLK_ESCAPE])
		{
			audio.playMenuSound(2);
			back = 1;
			engine.clearInput();
			engine.flushInput();
		}

		if (back)
		{
			showTitleWidgets();
			back = 0;
		}

		if (quit)
		{
			break;
		}

		engine.delay(frameLimit);
		frameLimit = SDL_GetTicks()  + 16;
	}

	engine.deleteWidgets();

	SDL_FillRect(graphics.screen, NULL, graphics.black);
	graphics.updateScreen();
	audio.stopMusic();
	
	if (quit)
	{
		exit(0);
	}
	
	newGame();
	
	(autoSaveSlot < 5) ? game.autoSaveSlot = autoSaveSlot : game.autoSave = false;

	if (load)
	{
		loadGame(load - 1);
		
		debug("misc", 2) << "Map Name = " << game.mapName << std::endl;
	}
	else if (engine.skill == -1)
	{
		game.setMapName("data/practice");
		game.setStageName("Training Mission");
		engine.practice = true;
		game.skill = 1;
		return SECTION_GAME;
	}
	else if (engine.skill == 3)
	{
		game.skill = 3;
		game.hasAquaLung = game.hasJetPack = true;
	}
	
	if (strcmp(game.mapName, "data/spaceStation") == 0)
	{
		game.setMapName("data/finalBattle");
		game.setStageName("Final Battle");
		return SECTION_GAME;
	}

	return SECTION_HUB;
}


/**
 * A class; that stores one line in the credits screen.
 */
class CreditsLine
{
	public:
		/**
		 * Constructor for CreditsLine
		 * @param iter A lisp::ListIterator, to the actual Lisp item
		 */
		CreditsLine(const lisp::ListIterator& iter) : it(iter) { surface = NULL; };
		/// @return The SDL_Surface of the image
		SDL_Surface * GetImage() { if (!surface) MakeSurface(); return surface; };
		/// @return The height of the line (currently the skip)
		int GetHeight() { if (!surface) MakeSurface(); return height; };
		/// Destructor
		~CreditsLine() { SDL_FreeSurface(surface); }
	private:
		lisp::ListIterator it;
		SDL_Surface * surface;
		int height;

		void MakeSurface();
};

/**
 * Parses the Lisp item; then generates an SDL_Surface
 * for blitting.
 */
void CreditsLine::MakeSurface()
{
	if (it.item() == "line")
	{
		height = it.value()->get_int();
		const lisp::Lisp * ptr = it.lisp()->get_cdr();
		graphics.setFontSize(ptr->get_car()->get_int());
		ptr = ptr->get_cdr();
		surface = graphics.getString(ptr->get_car()->get_string(), true);
	}
	else
		throw Exception::RuntimeError("Unknown lisp item: '" + it.item() +"'", AT);
};

/**
 * Loads and shows the credits at the end of the game.
 */
void doCredits()
{
	map.clear();

	audio.loadMusic("music/autumnsDawning.s3m");
	audio.playMusic();

	engine.resetTimeDifference();

	SDL_Surface *backdrop = graphics.quickSprite("credits_background", graphics.loadImage("gfx/main/creditsBack.png", false));
	
	lisp::Parser parser(true);
	const lisp::Lisp *credits_root = parser.parse("credits.dbwc");
	const lisp::Lisp *credits = credits_root->get_lisp("dirtyblobwars-credits");

	float y = 480;

	std::list<CreditsLine> lines;
	std::list<CreditsLine>::iterator it;

	lisp::ListIterator iter(credits);

	while (iter.next())
	{
		lines.push_back(CreditsLine(iter));
	}

	while (!lines.empty())
	{
		graphics.updateScreen();
		
		engine.getInput();
		engine.doTimeDifference();

		SDL_FillRect(graphics.screen, NULL, graphics.black);

		graphics.blit(backdrop, bx, by+365, graphics.screen, false);

		y -= (0.3 * engine.getTimeDifference());
		//y -= 1;

		it = lines.begin();
		if ((y + it->GetHeight()) < -10)
		{
			y += it->GetHeight();
			lines.pop_front();
		}

		//if we don't get a new iterator; we'll have an iterator to a deleted memory region!
		it = lines.begin();

		for (float y2 = y; ((y2 += it->GetHeight()) < 490) && (it != lines.end()); it++)
		{
			graphics.blit(it->GetImage(), graphics.width/2, by+(int)y2, graphics.screen, true);
		}
	}

	audio.fadeMusic();
	graphics.fadeToBlack();
}
