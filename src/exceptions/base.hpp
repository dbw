/* -*- fundamental -*-  -- otherwise C++ mode will eat the indentication..
Copyright © 2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/**
 * @file exceptions/base.hpp
 * Base header of DBW exceptions
 */

#ifndef EXCEPTIONS__BASE_HPP
#define EXCEPTIONS__BASE_HPP

#include <iostream>
#include <map>
#include <sstream>

/**
 * All exception and related stuff should go into this namespace.
 */
namespace Exception
{

/**
 * Base exception class. All exceptions inside DBW should derived from this class.
 */
class Base : public std::exception
{
	public:
		/**
		 * Default constructor constructing an exception with an empty info map.
		 * @param[in] file In what file did the exception occur
		 * @param[in] line Inside the file, what line.
		 * @param[in] function The name of the function (<dfn>__PRETTY_FUNCTION__</dfn>)
		 * @param[in] what A human-readable description of the exception.
		 */
		Base(const std::string& file, const unsigned short line, const std::string& function, const std::string& what) throw() : line(line), file(file), function(function), what_(what), info() {};

		/// Odd, but std::exception needs it
		virtual ~Base() throw() {}

		/// @return The value of \ref what field
		std::string GetWhat() const throw() { return what_; };
		/// @return A pretty text about where and what occured, also listing the info
		std::string GetDesc() const throw()
		{
			std::stringstream str;
			str << "Exception @ " << file << ":" << line << std::endl
				<< "In function " << function << " :" << std::endl
				<< what_ << std::endl;

			for (std::map<std::string, std::string>::const_iterator iter = info.begin(); iter != info.end(); iter++)
			{
				str << std::endl << iter->first << ": " << iter->second;
			}

			return str.str();
		}
		
		/// Compatibility with std::exception
		inline const char* what() const throw() { return GetDesc().c_str(); }

		/**
		 * Adds some info to the map.
		 * @param[in] name The name of the key
		 * @param[in] value The value of the key (the info)
		 */
		void AddInfo(std::string name, std::string value) throw() { info[name] = value; }

	protected:

		unsigned short line;   ///< The number of line on the exception occured
		
		std::string file,      ///< The name of the file in the exception occured
		            function,  ///< The name of the function in the exception occured (<dfn>__PRETTY_FUNCTION__</dfn>)
		            what_;     ///< The description of the exception

		/// A map storing adittional informations.
		std::map<std::string, std::string> info;
};

/**
 * A utility macro to define an exception quicly with ::Exception::Base as base class.
 * @param[in] name The name of the exception.
 * @param[in] desc The description of the exception (<b>what</b> parameter of Exception::Base::Base() )
 */
#define DEF_EXCEPTION(name, desc) DEF_EXCEPTION_CUST_BASE_BEG(name, desc, ::Exception::Base) }

/**
 * A utility macro to define an exception quickly with a custom base class.
 * @param[in] name The name of the exception.
 * @param[in] desc The description of the exception (<b>what</b> parameter of Exception::Base::Base() )
 * @param[in] base The name of the base class (plus namespace operators, if needed)
 */
#define DEF_EXCEPTION_CUST_BASE(_name, desc, base) DEF_EXCEPTION_CUST_BASE_BEG(_name, desc, base) }

/**
 * A utility macro to define an exception quickly with a custom base class.
 * @note This macro doesn't close the class, so you can add additional
 *       functions or variables. Use DEF_EXCEPTION_CUST_BASE() or
 *       DEF_EXCEPTION() to close class automatically.
 * @param[in] name The name of the exception.
 * @param[in] desc The description of the exception (<b>what</b> parameter of Exception::Base::Base() )
 * @param[in] base The name of the base class (plus namespace operators, if needed)
 */
#define DEF_EXCEPTION_CUST_BASE_BEG(_name, desc, base)                                                                               \
	class _name : public base                                                                                                        \
	{                                                                                                                                \
		public:                                                                                                                      \
			/**
			 * Default constructor constructing an exception with an empty info map.
			 * @param[in] file In what file did the exception occur
			 * @param[in] line Inside the file, what line.
			 * @param[in] func The name of the function (<dfn>__PRETTY_FUNCTION__</dfn>)
			 */                                                                                                                      \
		_name(const std::string& file, const unsigned short line, const std::string& func) throw() : base(file, line, func, desc) {}


/// A shorthand for the three parameters of exception (file, line, function)
#define AT __FILE__ + 7, __LINE__, __PRETTY_FUNCTION__
//#define VAR(var) #var, boost::lexical_cast<std::string>(var)

/**
 * An exception throwed when control flows to a section where it shouldn't.
 *
 * Many apps uses <code>assert(NULL);</code> for this; some, like <a
 * href="http://www.openttd.org/">OpenTTD</a> hides it into a
 * <dfn>NOT_REACHED()</dfn> macro. But we have a complete class for it ;)
 *
 * Some example of use: into the <dfn>default:</dfn> part of a switch (when it's
 * not the way of the normal running, of course), stub functions, and maybe some
 * quick checks, but mostly it's better to use assert() for it..
 */
DEF_EXCEPTION(NotReachedException, "Run flow to a section where it shouldn't go.");

/// Throws a NotReachedException without a description
#define NOT_REACHED() throw ::Exception::NotReachedException(AT)
/**
 * Throws a NotReachedException with adding a descreption.
 * @param[in] desc A description of the exception
 */
#define NOT_REACHED_DESC(desc) { ::Exception::NotReachedException e(AT); e.AddInfo("Details", desc); throw e; }

/**
 * An exception throwed when an assert() command fails.
 *
 * Assert command fails when it's expression is evaluates to NULL, making it ideal
 * for checking pointers, but it's usuable for almost everithing.
 */
DEF_EXCEPTION(AssertionFailure, "Assertion failure");

// undefine standard c library's assert..
#undef assert

/**
 * Throws an AssertionFailure when the expression evaluates to NULL.
 * @param x The expression to check.
 */
#define assert(x) ::Exception::DoAssert((x), #x, AT)

/**
 * The function that does the real checking. <b>NEVER CALL THIS FUNCTION
 * DIRECTLY!</b> Use the assert() macro instead.
 * @param assertion The value of the expression
 * @param expr A string containing the expression
 * @param file Which file
 * @param line Which line
 * @param function Which function
 */
static void inline DoAssert(bool assertion, const std::string& expr, const std::string& file, const unsigned short line, const std::string& function) throw(AssertionFailure)
{
	if (!assertion)
	{
		AssertionFailure fail(file, line, function);
		fail.AddInfo("Expression", expr);
		throw fail;
	}
}

/// An exception similar to std::runtime_error
class RuntimeError : public Base
{
	public:
		/**
		 * Default constructor constructing an exception with an empty info map.
		 * @param[in] desc Description of the exception
		 * @param[in] file In what file did the exception occur
		 * @param[in] line Inside the file, what line.
		 * @param[in] function The name of the function (<dfn>__PRETTY_FUNCTION__</dfn>)
		 */
		RuntimeError(const std::string& desc, const std::string& file, const unsigned short line, const std::string& func) throw() : Base(file, line, func, desc) {};
};

} //namespace Exception

#endif //EXCEPTIONS__BASE_HPP
