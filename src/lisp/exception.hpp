/*
Copyright © 2006 Matthias Braun <matze@braunis.de>
Copyright © 2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/**
 * @file lisp/exception.hpp
 * Exception definition used by lisp parser.
 */

#ifndef LISP__EXCEPTION_HPP
#define LISP__EXCEPTION_HPP

#include "../exceptions/base.hpp"

namespace lisp
{
DEF_EXCEPTION(LispParserError, "An error occured during parsing the lisp file");
}

#endif
