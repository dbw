/* -*- C++ -*-
Copyright © 2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/**
 * @file physfs/exception.hpp
 * PhysFS exception definiotions
 */

#ifndef PHYSFS__EXCEPTION_HPP
#define PHYSFS__EXCEPTION_HPP

#include "../exceptions/base.hpp"
#include <physfs.h>
namespace PhysFS
{

/**
 * A base class for PhysFS exceptions. All PhysFS exception should
 * derived from this class.
 * @see DEF_EXCEPTION_CUST_BASE() for quick definition.
 */
class BaseException : public ::Exception::Base
{
	protected:
		BaseException(const std::string& name, const unsigned int line, const std::string& func, const std::string& what) throw() : Base(name, line, func, what) {}
};

/// An exception thrown when PhysFS initialization fails.
DEF_EXCEPTION_CUST_BASE(InitException, "PhysFS initialization failed", BaseException);
/// An exception thrown when PhysFS de-initialization fails.
DEF_EXCEPTION_CUST_BASE(DeInitException, "PhysFS de-initialization failed", BaseException);
/**
 * An exception thrown when PhysFS fails to open a file (permission
 * denied, for example).
 * @note If a file is not found, FileNotFoundException is thrown
 *       instead of this.
 */
DEF_EXCEPTION_CUST_BASE(FileOpenException, "PhysFS failed to open file", BaseException);
/**
 * Exception thrown when the application tries to open a file that is
 * not exists.
 */
DEF_EXCEPTION_CUST_BASE(FileNotFoundException, "PhysFS failed to find the specified file", BaseException);
/**
 * Exception thrown when file read fails for some reason (disk-read
 * error, for example)
 */
DEF_EXCEPTION_CUST_BASE(FileReadException, "PhysFS encouraged an error while reading from file", BaseException);
/// Exception thrown when file write fails for some reason.
DEF_EXCEPTION_CUST_BASE(FileWriteException, "PhysFS encouraged an error while writing to file", BaseException);
/**
 * Exception thrown when PhysFS fails to close a file. It should only
 * happen rarely, and only on systems, that buffers disk I/O, but as
 * the PhysFS manual says at PHYSFS_close(), a well-written program
 * should ALWAYS check the return value from the close call..
 * @note The exception handler can acces the file handle without
 *       trouble, see handle.
 */
DEF_EXCEPTION_CUST_BASE_BEG(FileCloseException, "PhysFS failed to close file", BaseException)
	public:
		/**
		 * Contains the handle, that was failed to close. If you don't
		 * do anything with it in the exception handler, it's likely
		 * to lost forever, causing a memory leak, and killig your
		 * cat. (Okay, it's only a joke ;) )
		 */
		PHYSFS_file * handle;

		FileCloseException(const FileCloseException& e) : BaseException(e), handle(handle) {}
		FileCloseException& operator= (const FileCloseException& e) { BaseException::operator=(e); handle = e.handle; return *this; }
};
DEF_EXCEPTION_CUST_BASE(FileUndeterminableSizeException, "PhysFS failed to determine the size of the file", BaseException);

}

#endif //PHYSFS__EXCEPTION_HPP
