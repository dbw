/*
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/**
 * @file physfs/rwops.hpp
 * SDL_RWops interface headers for PhysFS
 */

#ifndef PHYSFS__RWOPS_HPP
#define PHYSFS__RWOPS_HPP

#include <SDL/SDL_rwops.h>
#include "exception.hpp"

namespace PhysFS
{

/**
 * A function to get an SDL_RWops structure for a PhysFS file. It a separate
 * function, but for functions, that needs a <dfn>SDL_RWops *</dfn> it can be
 * used if it would be a constructor ;)
 * @author Matthias Braun <matze@braunis.de> (Original author; made for <a
 *         href="http://supertux.lethargik.org/">SuperTux</a>)
 * @author Kővágó Zoltán <DirtY.iCE.hu@gmail.com>
 * @see <a href="http://kekkai.org/roger/sdl/rwops/rwops.html">SDL_RWops
 *      Tutorial</a>: a short introduction to SDL_RWops.
 * @see <a href="http://libsdl.org/cgi/docwiki.cgi/SDL_RWops">SDL_RWops - SDL
 *      Documentation Wiki</a>: reference like thing on SDL_RWops
 */
SDL_RWops * RWops(const std::string file) throw(FileNotFoundException, FileOpenException, std::bad_alloc);

} //namespace PhysFS

#endif //PHYSFS__RWOPS_HPP
