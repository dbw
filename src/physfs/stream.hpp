/*
Copyright © 2006 Matthias Braun <matze@braunis.de>
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/**
 * @file physfs/stream.hpp
 * std::stream interface header for PhysFS
 */

#ifndef PHYSFS__STREAM_HPP
#define PHYSFS__STREAM_HPP

#include "manager.hpp"
#include <iostream>

namespace PhysFS
{

/**
 * An std::streambuf class suitable to buffer a PhysFS file. Usually you
 * shouldn't use this class directly, use PhysFS::IStream to get a full-fledged
 * input stream for the file (like std::cin).
 * @note This class is for input only.
 */
class IStreambuf : public std::streambuf
{
	public:
		/**
		 * Initializes a new object and opens the file.
		 * @param[in] filename The name of the file to open
		 * @throw FileNotFoundException if the file not found.
		 * @throw FileOpenException if the file exists, but cannot open for some reasons.
		 */
		IStreambuf(const std::string& filename) throw(FileNotFoundException, FileOpenException) : file(Manager::OpenFile(filename)), filename(filename) {}
		/**
		 * Destroys the IStreambuf object and closes the PHYSFS_file handle.
		 * @throw FileCloseException if PhysFS cannot close the file for some reason.
		 */
		~IStreambuf() throw(FileCloseException) { Manager::CloseFile(file, filename); }

	protected:
		virtual int underflow() throw();
		virtual pos_type seekoff(off_type pos, std::ios_base::seekdir, std::ios_base::openmode) throw();
		virtual pos_type seekpos(pos_type pos, std::ios_base::openmode) throw();

	private:
		PHYSFS_file* file;
		std::string filename;
		char buf[1024];

		IStreambuf(const IStreambuf& i) : std::streambuf(), file(i.file), filename(i.filename) {}
		IStreambuf& operator= (const IStreambuf&) { return *this; }
};

/**
 * An std::streambuf class suitable to buffer a PhysFS file. Usually you
 * shouldn't use this class directly, use PhysFS::OStream to get a full-fledged
 * output stream for the file (like std::cout).
 * @note This class is for output only.
 */
class OStreambuf : public std::streambuf
{
	public:
		OStreambuf(const std::string& filename) throw(FileOpenException);
		~OStreambuf() throw(FileCloseException);

	protected:
		virtual int overflow(int c) throw();
		virtual int sync() throw();

	private:
		PHYSFS_file* file;
		std::string filename;
		char buf[1024];
		OStreambuf(const OStreambuf& i) : std::streambuf(), file(i.file), filename(i.filename) {}
		OStreambuf& operator= (const OStreambuf&) { return *this; }
};

/**
 * A class for reading in files through PhysFS using standard C++ std::istream
 * interface. All operators, that you can use on other istreams, you can use
 * here aswell.
 */
class IStream : public std::istream
{
	public:
		/**
		 * Initializes a new istream by opening the file immediately.
		 * @param[in] filename The name of the file to open.
		 * @throw FileNotFoundException if the file not exists
		 * @throw FileOpenException if the file exists, but cannot open for some reason
		 * @throw std::bad_alloc if there's not enough memory for IStreambuf instance.
		 */
		IStream(const std::string& filename) throw(FileNotFoundException, FileOpenException, std::bad_alloc) : std::istream(new PhysFS::IStreambuf(filename)) {}
		/**
		 * Destroys the object and closes the file handle.
		 * @throw FileCloseException if PhysFS cannot close the file for some reason.
		 */
		~IStream() throw(FileCloseException) { delete rdbuf(); }
};

/**
 * A class for writing files through PhysFS using standard C++ std::istream
 * interface. All operators, that you can use on other ostreams, you can use
 * here aswell.
 */
class OStream : public std::ostream
{
	public:
		/**
		 * Initializes a new ostream by opening the file immediately.
		 * @param[in] filename The name of the file to open. If exists, it'll be overwritten.
		 * @throw FileOpenException if the file exists, but cannot open for some reason
		 * @throw std::bad_alloc if there's not enough memory for IStreambuf instance.
		 */
		OStream(const std::string& filename) throw(FileOpenException, std::bad_alloc) : std::ostream(new PhysFS::OStreambuf(filename)) {}
		/**
		 * Destroys the object and closes the file handle.
		 * @throw FileCloseException if PhysFS cannot close the file for some reason.
		 */
		~OStream() { delete rdbuf(); }
};

} //namespace PhysFS

#endif //PHYSFS__STREAM_HPP
