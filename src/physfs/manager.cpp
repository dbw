/* -*- C++ -*-
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/**
 * @file physfs/manager.cpp
 * Physfs functions
 * This file containing callbacks for Physfs (replacing the old pak system)
 */

#include "config.h"
#include "manager.hpp"
#include "../protectstr/protectstr.hpp"
#include <stdexcept>
#include <boost/lexical_cast.hpp>

namespace PhysFS
{

/**
 * Initializes PhysFS.
 * @param[in] in_argv the <dfn>argv[0]</dfn> parameter of main().
 * @throw InitException if PhysFS initialization fails.
 */
void Manager::Init(const char* in_argv) throw(InitException)
{
	if (!PHYSFS_init(in_argv))
	{
		InitException e(AT);
		e.AddInfo("Tip", "Make sure you installed PhysFS correctly; try to compile DirtY BLoB WaRs from sources.");
		e.AddInfo("PHYSFS_getLastError()", ProtectStr(PHYSFS_getLastError()));
		throw e;
	}

	// TEST, I'll change it back if doesn't work as expected..
	//#ifdef UNIX
		std::string userdir = std::string(PHYSFS_getUserDir()) + ".dirtystudios/dirtyblobwars";
		if (!SetWriteDir(userdir)) SetWriteWarning(userdir);
		if (!PHYSFS_addToSearchPath(userdir.c_str(), 0)) AddPathWarning(userdir);
	//#else
	//	if (!SetWriteDir("./savedata")) SetWriteWarning("savedata");
	//	if (!PHYSFS_addToSearchPath("savedata", 0)) AddPathWarning("savedata");
	//#endif

	if (!PHYSFS_addToSearchPath("data", 1)) AddPathWarning("data");

	#ifdef UNIX
		if (!PHYSFS_addToSearchPath(DATADIR, 1)) AddPathWarning(DATADIR);
	#endif

}

/**
 * Deinitializes PhysFS.
 * @throw DeInitException if PhysFS deinitializatin fails for some reason.
 */
void Manager::DeInit() throw(DeInitException)
{
	//BOOOM-BOOM-BOOM!!!
	if (!PHYSFS_deinit())
	{
		DeInitException e(AT);
		e.AddInfo("PHYSFS_getLastError()", ProtectStr(PHYSFS_getLastError()));
		throw e;
	}
}

/**
 * Opens a file.
 * Internal function, you <b>shouldn't use this directly</b>.
 * @param[in] file The file to open
 * @throw FileNotFoundException if the specified file doesn't exist.
 * @throw FileOpenException if an error occures during file open.
 */
PHYSFS_file * Manager::OpenFile(const std::string& file) throw(FileNotFoundException, FileOpenException)
{
	if (!PHYSFS_exists(file.c_str()))
	{
		FileNotFoundException e(AT);
		e.AddInfo("File name", file);
		throw e;
	}

	PHYSFS_file* filehandler = PHYSFS_openRead(file.c_str());
	if (!filehandler)
	{
		FileOpenException e(AT);
		e.AddInfo("File name", file);
		e.AddInfo("PHYSFS_getLastError()", ProtectStr(PHYSFS_getLastError()));
		throw e;
	}

	return filehandler;
}

/**
 * Opens a file and reads it's constents to memory, allocating the required size
 * of memory.
 * @param[in] file The name of the file to open
 * @param[out] pointer A pointer to a char *; ReadFile will set this pointer to
 * the memory region containing the content of the file.
 * @warning If reading fails, the variable stay unchanged!
 * @warning If reading not fails, you have to take care to free the memory,
 *  ie.: @code
 *   char * buf;
 *   PHYSFS_sint64 size = PhysFS::Manager::ReadFile("some/file.ext", &buf);
 *   // do the work
 *   delete[] buf;
 *  @endcode
 * @return Number of bytes read
 * @throw FileNotFoundException if the specified file doesn't exist.
 * @throw FileOpenException if an error occures during file open.
 * @throw FileUndeterminableSizeException if PhysFS fails to determine the size
 *        of the file
 * @throw FileReadException if an error occures during reading the file.
 * @throw FileCloseException if the file couldn't closed for some reason.
 * @throw std::bad_alloc if there's not enough memory to read the file
 */
PHYSFS_sint64 Manager::ReadFile(const std::string& file, char ** pointer)
	throw(FileNotFoundException, FileOpenException, FileUndeterminableSizeException, FileReadException, FileCloseException, std::bad_alloc)
{
	PHYSFS_file* handle = OpenFile(file);

	PHYSFS_sint64 filesize = FileSize(handle, file);
	// make a buffer here. I could simply say *pointer = new char[filesize]; but
	// that would violate ``do not touch pointer if fails''..
	char * buf = new char[filesize];

	PHYSFS_sint64 read = PHYSFS_read(handle, buf, 1, filesize);
	if (read < filesize)
	{
		delete[] buf;
		FileReadException e(AT);
		e.AddInfo("File name", file);
		e.AddInfo("Size requested", boost::lexical_cast<std::string>(filesize));
		e.AddInfo("Actually read", boost::lexical_cast<std::string>(read));
		e.AddInfo("PHYSFS_getLastError()", ProtectStr(PHYSFS_getLastError()));
		throw e;
	}
	try
	{
		CloseFile(handle, file);
	}
	catch(...)
	{
		delete[] buf;
		throw;
	}

	*pointer = buf;
	return filesize;
}

/**
 * Opens a file and reads it's contents to memory, allocating the required size
 * of memory.
 * @param[in] file The name of the file to open
 * @return A pointer to a std::strinbuf containing the contents of the file.
 * @warning Since this function returns a pointer, you have to take care to free
 *  the memory, ie.: @code
 *   std::stringbuf * buf = PhysFS::Manager::ReadFile("some/file.ext");
 *   // do the work
 *   delete buf;
 *  @endcode
 * @throw FileNotFoundException if the specified file doesn't exist.
 * @throw FileOpenException if an error occures during file open.
 * @throw FileUndeterminableSizeException if PhysFS fails to determine the size
 *        of the file
 * @throw FileReadException if an error occures during reading the file.
 * @throw FileCloseException if the file couldn't closed for some reason.
 * @throw std::bad_alloc if there's not enough memory to read the file
 */
std::stringbuf * Manager::ReadFile(const std::string& file)
	throw(FileNotFoundException, FileOpenException, FileUndeterminableSizeException, FileReadException, FileCloseException, std::bad_alloc)
{
	char * buf;
	PHYSFS_sint64 filesize = ReadFile(file, &buf);

	std::stringbuf * sb = NULL;
	try
	{
		sb = new std::stringbuf(std::string(buf, filesize));
	}
	catch (...)
	{
		delete sb;
		delete[] buf;
		throw;
	}

	delete[] buf;

	return sb;
}

/**
 * Opens a file and reads it's contents to a prevoiusly allocated memory region.
 * @param[in] file The name of the file to read
 * @param[out] pointer An allocated area of memory to put data into. If the
 *             function fails, the content of this buffer will be <b>unspecified</b>.
 * @param[in] max Max number of bytes to read
 * @return The number of bytes read.
 * @note If the buffer is smaller than file, this function will silently ignore
 *       all further data. Use FileSize() to determine required memory size.
 * @throw FileNotFoundException if the specified file doesn't exist.
 * @throw FileOpenException if an error occures during file open.
 * @throw FileReadException if an error occures during reading the file.
 * @throw FileCloseException if the file couldn't closed for some reason.
 */
PHYSFS_sint64 Manager::ReadFileIntoPointer(const std::string& file, char * pointer, const int max)
	throw(FileNotFoundException, FileOpenException, FileReadException, FileCloseException)
{
	PHYSFS_file* handle = OpenFile(file);

	PHYSFS_sint64 read = PHYSFS_read(handle, pointer, 1, max);
	if (read < 0 || (read < max && !PHYSFS_eof(handle)))
	{
		FileReadException e(AT);
		e.AddInfo("File name", file);
		e.AddInfo("Size requested", boost::lexical_cast<std::string>(max));
		e.AddInfo("Actually read", boost::lexical_cast<std::string>(read));
		e.AddInfo("PHYSFS_getLastError()", ProtectStr(PHYSFS_getLastError()));
		throw e;
	}
	CloseFile(handle, file);

	return read;
}

/**
 * Write contents of a std::stringbuf to a file
 * @param[in] file The name of the file to write.
 * @param[in] sb The stringbuf object to write.
 * @throw FileOpenException if an error occures during file open.
 * @throw FileWriteException if an error occures during writing the file.
 * @throw FileCloseExrception if the file couldn't closed for some reason.
 * @note If the write fails, then the file content will be unspecified, except
 *       if the exception is FileOpenException, which means no change.
 */
void Manager::WriteFile(const std::string& file, const std::stringbuf& sb) throw(FileOpenException, FileWriteException, FileCloseException)
{
	if (std::string::npos != file.find('/', 0)) MakeDirForFile(file);
	PHYSFS_file* handle = PHYSFS_openWrite(file.c_str());
	if (!handle)
	{
		FileOpenException e(AT);
		e.AddInfo("File name", file);
		e.AddInfo("PHYSFS_getLastError()", ProtectStr(PHYSFS_getLastError()));
		throw e;
	}


	const std::string& str = sb.str();
	PHYSFS_sint64 written = PHYSFS_write(handle, str.c_str(), 1, str.size());
	if (written < PHYSFS_sint64(str.size()))
	{
		FileWriteException e(AT);
		e.AddInfo("File name", file);
		e.AddInfo("Size requested", boost::lexical_cast<std::string>(str.size()));
		e.AddInfo("Size written", boost::lexical_cast<std::string>(written));
		e.AddInfo("PHYSFS_getLastError()", ProtectStr(PHYSFS_getLastError()));
		throw e;
	}

	CloseFile(handle, file);
}

/**
 * Write a region of memory to a file
 * @param[in] file The name of the file to write.
 * @param[in] pointer A pointer to the beginning of the memory region.
 * @param[in] length The length of the buffer, in bytes.
 * @throw FileOpenException if an error occures during file open.
 * @throw FileWriteException if an error occures during writing the file.
 * @throw FileCloseExrception if the file couldn't closed for some reason.
 * @note If the write fails, then the file content will be unspecified, except
 *       if the exception is FileOpenException, which means no change.
 */
void Manager::WriteFile(const std::string& file, const char * pointer, const int length)
	throw(FileOpenException, FileWriteException, FileCloseException)
{
	PHYSFS_file* handle = PHYSFS_openWrite(file.c_str());
	if (!handle)
	{
		FileOpenException e(AT);
		e.AddInfo("File name", file);
		e.AddInfo("PHYSFS_getLastError()", ProtectStr(PHYSFS_getLastError()));
		throw e;
	}

	PHYSFS_sint64 written = PHYSFS_write(handle, pointer, 1, length);
	if (written < length)
	{
		FileWriteException e(AT);
		e.AddInfo("File name", file);
		e.AddInfo("Size requested", boost::lexical_cast<std::string>(length));
		e.AddInfo("Size written", boost::lexical_cast<std::string>(written));
		e.AddInfo("PHYSFS_getLastError()", ProtectStr(PHYSFS_getLastError()));
		throw e;
	}

	CloseFile(handle, file);
}

/**
 * Gets the size of a file.
 * @param[in] file The name of the file what's size are you corius about.
 * @return The size of the file in bytes.
 * @throw FileNotFoundException if the specified file doesn't exist.
 * @throw FileOpenException if an error occures during file open.
 * @throw FileUndeterminableSizeException if PhysFS fails to determine size
 * @throw FileCloseException if PhysFS fails to close file
 */
PHYSFS_sint64 Manager::FileSize(const std::string& file) throw(FileNotFoundException, FileOpenException, FileCloseException, FileUndeterminableSizeException)
{
	PHYSFS_file * handle = OpenFile(file);
	bool ret = FileSize(handle, file);
	CloseFile(handle, file);
	return ret;
}

/**
 * Gets the size of a file.
 * @note This is an internal command, <b>you should use</b>
 *       Manager::FileSize(const std::string&)
 * @param[in] handle A handle to the file
 * @param[in] file The name of the file (can be blank as only used for exceptions.)
 * @return The size of the file in bytes.
 * @throw FileUndeterminableSizeException if PhysFS fails to determine size
 */

PHYSFS_sint64 Manager::FileSize(PHYSFS_file * handle, const std::string& file) throw(FileUndeterminableSizeException)
{
	PHYSFS_sint64 size = PHYSFS_fileLength(handle);
	if (size < 0)
	{
		FileUndeterminableSizeException e(AT);
		e.AddInfo("File name", file);
		e.AddInfo("Return value", boost::lexical_cast<std::string>(size));
		e.AddInfo("PHYSFS_getLastError()", ProtectStr(PHYSFS_getLastError()));
		throw e;
	}
	return size;
}

/**
 * Close a PhysFS handle.
 * @param[in] handle The handle to destroy
 * @param[in] file Name of the file. (Can be blank as only used for exceptions.)
 * @throw FileCloseException if closing fails. A pointer to the handle will
 *        included in the exception, do whatever you want with it in the
 *        exception handler (eat it, if you have no better idea).
 */
void Manager::CloseFile(PHYSFS_file * handle, const std::string& file) throw(FileCloseException)
{
	if (!PHYSFS_close(handle))
	{
		FileCloseException e(AT);
		e.AddInfo("File name", file);
		e.AddInfo("PHYSFS_getLastError()", ProtectStr(PHYSFS_getLastError()));
		e.handle = handle;
		throw e;
	}
}

// protected functions:
// | | | | | | | | | | |
// ⌄ ⌄ ⌄ ⌄ ⌄ ⌄ ⌄ ⌄ ⌄ ⌄ ⌄

/**
 * Outputs a warning about can't add a search path.
 * @param[in] path The path that was failed to add.
 */
void Manager::AddPathWarning(const std::string& path) throw()
{
	warning << "Failed to add '" << path << "' to the search path." << std::endl
			<< "The game might not run correctly." << std::endl
			<< "PhysFS reports: " << ProtectStr(PHYSFS_getLastError()) << std::endl;
}

/**
 * Outputs a warning about can't set save path.
 * @param[in] path The path that was failed to set.
 */
void Manager::SetWriteWarning(const std::string& path) throw()
{
	warning << "Failed to set '" << path << "' as write dir!" << std::endl
			<< "The game can't save your options and your state." << std::endl
			<< "PhysFS reports: " << ProtectStr(PHYSFS_getLastError()) << std::endl;
}

/**
 * Set the writing directory. If the directory doesn't exist, this function will
 * create it (even if needs to recursively create directories)
 * @param[in] dir Directory to set writing dir to.
 * @retval true if everithing went smoothly
 * @retval false if an error occured (permission denied, read-only filesytem,
 *         etc)
 */
bool Manager::SetWriteDir(const std::string& dir) throw()
{
	// don't go into the costly loops if the dir is ok..
	if (PHYSFS_setWriteDir(dir.c_str())) return true;

	/// @todo Rewrite using boost::tokenizer
	// calculate number of tokens
	size_t pos, pos2 , num, num2;
	num = num2 = 0;
	pos = -1;
	while (std::string::npos != (pos = dir.find('/', ++pos)))
	{
		num++;
	}
	num++;
	std::string * substrs = new std::string[num];

	// tokenize
	pos2 = -1;
	for (unsigned int i = 0; i < num; i++)
	{
		pos = pos2;
		pos2 = dir.find('/', ++pos);
		substrs[i] = dir.substr(pos, i != num-1 ? pos2 - pos : 999);
	}

	// try to remove the last field of path and set directory
	std::string basepath;
	num2 = num;
	do
	{
		if (num <= 0)
			// we can't even write the root directory. it may be a read-only
			// filesystem, or the user has no rights.. we can't do anything, so
			// return here..
			return false;

		basepath = "";
		for (unsigned int i = 0; i < num; i++) basepath += substrs[i] + ((i != (num-1)) ? "/" : "");

		num--;
	} while (!PHYSFS_setWriteDir(basepath.c_str()));

	// now recursively make directories
	std::string path2 = "";
	for (unsigned int i = 1; i < (num2-num); i++)
	{
		path2 += (i != 1 ? "/" : "") + substrs[num+i];
		debug("physfs", 2) << "Making directory:: '" << basepath << "' :: '" << path2 << "'" << std::endl;
		
		if (!PHYSFS_mkdir(path2.c_str()))
			// hmm.. parent directory exist, and writeable; but still can't
			// make directory..
			return false;
	}

	// everithing or nothing :P
	return PHYSFS_setWriteDir(dir.c_str());
}

/**
 * Make directories for the specified file if they're not exists.
 * @param[in] file The name of the file with a PhysFS path to make a directory
 *            for.
 * @todo Make use of exceptions. Currently, if the directory is not makeable,
 *       the code will continue, and fail in OpenFile() (hopefully).
 */
void Manager::MakeDirForFile(const std::string& file) throw()
{
	size_t num, pos, pos2;
	num = 0;
	pos = pos2 = -1;
	while (file.npos != (pos = file.find('/', ++pos)))
	{
		num++;
	}

	pos2 = -1;
	for (unsigned int i = 0; i < num; i++)
	{
		pos = pos2;
		pos2 = file.find('/', ++pos);
		PHYSFS_mkdir(file.substr(pos, pos2 - pos).c_str());
	}
}

} //namespace PhysFS
