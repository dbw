/*
Copyright © 2006 Matthias Braun <matze@braunis.de>
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/**
 * @file physfs/stream.cpp
 * std::stream interface for PhysFS
 */

#include "stream.hpp"
#include "../protectstr/protectstr.hpp"

namespace PhysFS
{

// PhysFS streaming
// Copyright 2006 Matthias Braun

/// Called by the std::istream automatically when the buffer underflows. <b>DO NOT CALL THIS DIRECTLY!</b>
int PhysFS::IStreambuf::underflow() throw()
{
	if(PHYSFS_eof(file))
	{
		return traits_type::eof();
	}

	PHYSFS_sint64 bytesread = PHYSFS_read(file, buf, 1, sizeof(buf));
	if(bytesread <= 0)
	{
		return traits_type::eof();
	}
	setg(buf, buf, buf + bytesread);

	return static_cast<unsigned char>(buf[0]);
}

/**
 * Seek in the buffer. Called by std::istream, <b>DO NOT CALL THIS DIRECTLY!</b>
 * Use std::istream::seekg() instead
 */
PhysFS::IStreambuf::pos_type PhysFS::IStreambuf::seekpos(pos_type pos, std::ios_base::openmode) throw()
{
	if(PHYSFS_seek(file, static_cast<PHYSFS_uint64> (pos)) == 0)
	{
		return pos_type(off_type(-1));
	}

	// the seek invalidated the buffer
	setg(buf, buf, buf);
	return pos;
}

/**
 * Seek in the buffer relative to an offset. Called by std::istream, 
 * <b>DO NOT CALL THIS DIRECTLY!</b> Use std::istream::seekg() instead.
 */
PhysFS::IStreambuf::pos_type PhysFS::IStreambuf::seekoff(off_type off, std::ios_base::seekdir dir, std::ios_base::openmode mode) throw()
{
	off_type pos = off;
	PHYSFS_sint64 ptell = PHYSFS_tell(file);

	switch(dir)
	{
		case std::ios_base::beg:
			break;
		case std::ios_base::cur:
			if(off == 0)
				return static_cast<pos_type> (ptell) - static_cast<pos_type> (egptr() - gptr());
			pos += static_cast<off_type> (ptell) - static_cast<off_type> (egptr() - gptr());
			break;
		case std::ios_base::end:
			pos += static_cast<off_type> (PHYSFS_fileLength(file));
			break;
		default:
			return pos_type(off_type(-1));
	}

	return seekpos(static_cast<pos_type> (pos), mode);
}

//---------------------------------------------------------------------------

/**
 * Initializes a new object, and opens for write.
 * @param[in] filename The file to write to. If exists, it'll overwritten.
 * @throw FileOpenException if the file cannot opened for writing, for some reason
 */
PhysFS::OStreambuf::OStreambuf(const std::string& filename) throw(FileOpenException)
	: file(PHYSFS_openWrite(filename.c_str())), filename(filename)
{
	if(!file)
	{
		FileOpenException e(AT);
		e.AddInfo("File name", filename);
		e.AddInfo("PHYSFS_getLastError()", ProtectStr(PHYSFS_getLastError()));
		throw e;
	}

	setp(buf, buf+sizeof(buf));
}

/**
 * Destroys the OStreambuf object and closes the PHYSFS_file handle.
 * @throw FileCloseException if PhysFS cannot close the file for some reason.
 */
PhysFS::OStreambuf::~OStreambuf() throw(FileCloseException)
{
	sync();
	Manager::CloseFile(file, filename);
}

/// Called by the std::ostream automatically when the buffer overflows. <b>DO NOT CALL THIS DIRECTLY!</b>
int PhysFS::OStreambuf::overflow(int c) throw()
{
	char c2 = char(c);

	if(pbase() == pptr())
		return 0;

	size_t size = pptr() - pbase();
	PHYSFS_sint64 res = PHYSFS_write(file, pbase(), 1, size);
	if(res <= 0)
		return traits_type::eof();

	if(c != traits_type::eof())
	{
		PHYSFS_sint64 res = PHYSFS_write(file, &c2, 1, 1);
		if(res <= 0)
			return traits_type::eof();
	}

	setp(buf, buf + res);
	return 0;
}

/// Writes out context remaining in the buffer.
int PhysFS::OStreambuf::sync() throw()
{
	return overflow(traits_type::eof());
}

} //namespace PhysFS
