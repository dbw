/*
Copyright © 2006 Matthias Braun <matze@braunis.de>
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/**
 * @file physfs/rwops.cpp
 * SDL_RWops interface for PhysFS
 */

#include "rwops.hpp"
#include "manager.hpp"
#include "../console/console.hpp"
#include "../protectstr/protectstr.hpp"

namespace PhysFS
{

int RWopsSeek(SDL_RWops* context, int offset, int whence) throw();
int RWopsRead(SDL_RWops* context, void* ptr, int size, int maxnum) throw();
int RWopsClose(SDL_RWops* context) throw();
int RWopsWrite(SDL_RWops* context, const void *ptr, int size, int num) throw();

/**
 * Makes an SDL_RWops structure for a file to open through PhysFS.
 * @param file The name of the file to open
 * @return A pointer to an SDL_RWops structure.
 * @throw FileNotFoundException if file doesn't exist.
 * @throw FileOpenException if PhysFS fails to open file.
 * @throw std::bad_alloc if memory is not enough.
 */
SDL_RWops * RWops(const std::string file) throw(FileNotFoundException, FileOpenException, std::bad_alloc)
{
	PHYSFS_file * filehandler = Manager::OpenFile(file);

	SDL_RWops * rw = new SDL_RWops;
	rw->type  = 0;
	rw->hidden.unknown.data1 = filehandler;
	rw->seek  = RWopsSeek;
	rw->read  = RWopsRead;
	rw->write = RWopsWrite; 
	rw->close = RWopsClose;

	return rw;
}

/* PhysFS reading funcs for SDL_RWops
   Copyright 2006 Matthias Braun <mattze@braunis.de> */
/**
 * Function for SDL_RWops structure.
 * @see RWops::operator()
 * @see <a href="http://libsdl.org/cgi/docwiki.cgi/SDL_RWops">SDL_RWops - SDL
 *      Documentation Wiki</a>: reference like thing on SDL_RWops
 * @{
 * Seek in the file
 */
int RWopsSeek(SDL_RWops* context, int offset, int whence) throw()
{
	PHYSFS_file* file = (PHYSFS_file*) context->hidden.unknown.data1;
	int res;
	switch(whence) {
		case SEEK_SET:
			res = PHYSFS_seek(file, offset);
			break;
		case SEEK_CUR:
			res = PHYSFS_seek(file, PHYSFS_tell(file) + offset);
			break;
		case SEEK_END:
			res = PHYSFS_seek(file, PHYSFS_fileLength(file) + offset);
			break;
		default:
			res = 0;
			exit(1);
			break;
	}

	if(res == 0) {
		error << "PhysFS::RWops: Error seeking in file! PhysFS reported error: " << ProtectStr(PHYSFS_getLastError()) << std::endl;
		return -1;
	}

	return (int) PHYSFS_tell(file);
}

/// Read some data
int RWopsRead(SDL_RWops* context, void* ptr, int size, int maxnum) throw()
{
	PHYSFS_file* file = (PHYSFS_file*) context->hidden.unknown.data1;

	int res = PHYSFS_read(file, ptr, size, maxnum);
	return res;
}

/// Close the file - note, it can fail!
int RWopsClose(SDL_RWops* context) throw()
{
	PHYSFS_file* file = (PHYSFS_file*) context->hidden.unknown.data1;

	int res = PHYSFS_close(file);
	if (!res)
	{
		error << "PhysFS::RWops: File close failed! PhysFS reported error: " << ProtectStr(PHYSFS_getLastError()) << std::endl;
		return -1;
	}

	delete context;

	return 0;
}

/// This is read-only..
int RWopsWrite(SDL_RWops* context, const void *ptr, int size, int num) throw()
{
	return -1;
}
/// @}

}
