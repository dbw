/*
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/** 
 * @file physfs/manager.hpp
 * Physfs functions' headers
 * Headers for callbacks for Physfs (replacing the old pak system)
 */

#ifndef PHYSFS__MANAGER_HPP
#define PHYSFS__MANAGER_HPP

#include <physfs.h>

#include "console/console.hpp"
#include "exception.hpp"

/// All PhyFS related stuff should go into this namespace.
namespace PhysFS
{

/**
 * Functions to initialize PhysFS and read, write files through it, wrapped into
 * a class.
 */
class Manager
{
	public:
		/// @{
		static void Init(const char* in_argv) throw(InitException);
		static void DeInit() throw(DeInitException);
		/// @}

		/**
		 * @{
		 * Checks if the specified file exists.
		 * @retval true if the file exists
		 * @retval false if the file doesn't exists
		 */
		static bool FileExists(const std::string& file) throw() { return PHYSFS_exists(file.c_str()); };
		static PHYSFS_sint64 FileSize(const std::string& file) 
			throw(FileNotFoundException, FileOpenException, FileCloseException, FileUndeterminableSizeException);
		/// @}

		/// @{
		static PHYSFS_sint64 ReadFile(const std::string& file, char ** pointer)
			throw(FileNotFoundException, FileOpenException, FileReadException, FileCloseException,
				  FileUndeterminableSizeException, std::bad_alloc);
		static std::stringbuf * ReadFile(const std::string& file)
			throw(FileNotFoundException, FileOpenException, FileReadException, FileCloseException,
				  FileUndeterminableSizeException, std::bad_alloc);
		static PHYSFS_sint64 ReadFileIntoPointer(const std::string& file, char * pointer, const int max = 0)
			throw(FileNotFoundException, FileOpenException, FileReadException, FileCloseException);
		/// @}
		
		/// @{
		static void WriteFile(const std::string& file, const std::stringbuf& sb)
			throw(FileOpenException, FileWriteException, FileCloseException);
		static void WriteFile(const std::string& file, const char * pointer, const int length)
			throw(FileOpenException, FileWriteException, FileCloseException);
		/// @}

		/**
		 * Internal function! <b>Only use, if you know what are you doing!</b>
		 * @{
		 */
		static PHYSFS_file * OpenFile(const std::string& file) throw(FileNotFoundException, FileOpenException);
		static PHYSFS_sint64 FileSize(PHYSFS_file * handle, const std::string& file) throw(FileUndeterminableSizeException);
		static void CloseFile(PHYSFS_file * handle, const std::string& file) throw(FileCloseException);
		// @}

	protected:
		static void AddPathWarning(const std::string& path) throw();
		static void SetWriteWarning(const std::string& path) throw();
		static bool SetWriteDir(const std::string& dir) throw();
		static void MakeDirForFile(const std::string& file) throw();

	private:
		/// Not instanceable
		Manager() throw() {}

};

} //namespace PhysFS

#endif //PHYSFS__MANAGER_HPP

