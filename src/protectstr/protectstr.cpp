/******************************************************************************
 * ProtectStr - a library to safely access a C-style string from C++ code     *
 * Copyright © 2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>                    *
 *                                                                            *
 *  This program is free software. It comes without any warranty, to the      *
 * extent permitted by applicable law. You can redistribute it and/or modify  *
 * it under the terms of the Do What The Fuck You Want To Public License,     *
 * Version 2, as published by Sam Hocevar. See the shipped COPYING file or    *
 * http://sam.zoy.org/wtfpl/ for more details.                                *
 *                                                                            *
 *  Although is not required, giving a credit to me for using this library    *
 * would be nice.                                                             *
 ******************************************************************************/
/*
DirtY BLoB WaRS
Copyright © 2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/*  This is a dual licensed file. You're free to use any license, or both, under
 * the following restrictions:
 *  You're redistributing DirtY BLoB WaRs, when you keep, or use content from
 * any other file from DirtY BLoB WaRs distribution, than protectstr.cpp and
 * protectstr.hpp .
 *  When you reditribute DirtY BLoB WaRs, you must keep the GPL license, and
 * redistribute it under the GPL terms, but you can choose to drop WTFPL, or
 * not.
 */

/**
 * @file protectstr.cpp
 * Safe convert from char * to std::string
 * @author Kővágó Zoltán <DirtY.iCE.hu@gmail.com>
 */

#include "config.h"
#include "protectstr.hpp"

#ifdef UNIX
#include <signal.h>
#include <setjmp.h>

static sigjmp_buf resume;

void __attribute__((noreturn)) Handler(int num)
{
	siglongjmp(resume, 1);
}

#endif //UNIX

/**
 * Safely convert a C style string to std::string.
 * Null pointers, unterminated strings, invalid pointers are all filtered
 * out. At least, on Linux.
 * @param[in] c_str The C style string to convert.
 * @return c_str converted to std::string, if valid; an error message otherwise.
 */
const std::string ProtectStr(const char * c_str)
{
	if (c_str == NULL)
		return "ProtectStr: NULL";

#ifdef UNIX
	struct sigaction handler, old_segv, old_bus;
	if (sigsetjmp(resume, 1))
	{
		sigaction(SIGSEGV, &old_segv, NULL);
		sigaction(SIGBUS, &old_bus, NULL);
		return "ProtectStr: Invalid pointer or unterminated string";
	}

	handler.sa_handler = Handler;
	//handler.sa_mask = 0;
	handler.sa_flags = 0;
	sigaction(SIGSEGV, &handler, &old_segv);
	sigaction(SIGBUS, &handler, &old_bus);

	for (const char * i = c_str; *i != '\0'; ++i);

	sigaction(SIGSEGV, &old_segv, NULL);
	sigaction(SIGBUS, &old_bus, NULL);

#endif //UNIX

	return c_str;

}


