/******************************************************************************
 * ProtectStr - a library to safely access a C-style string from C++ code     *
 * Copyright © 2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>                    *
 *                                                                            *
 *  This program is free software. It comes without any warranty, to the      *
 * extent permitted by applicable law. You can redistribute it and/or modify  *
 * it under the terms of the Do What The Fuck You Want To Public License,     *
 * Version 2, as published by Sam Hocevar. See the shipped COPYING file or    *
 * http://sam.zoy.org/wtfpl/ for more details.                                *
 *                                                                            *
 *  Although is not required, giving a credit to me for using this library    *
 * would be nice.                                                             *
 ******************************************************************************/
/*
DirtY BLoB WaRS
Copyright © 2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/*  This is a dual licensed file. You're free to use any license, or both, under
 * the following restrictions:
 *  You're redistributing DirtY BLoB WaRs, when you keep, or use content from
 * any other file from DirtY BLoB WaRs distribution, than protectstr.cpp and
 * protectstr.hpp .
 *  When you reditribute DirtY BLoB WaRs, you must keep the GPL license, and
 * redistribute it under the GPL terms, but you can choose to drop WTFPL, or
 * not.
 */

/**
 * @file protectstr.hpp
 * Safe convert from char * to std::string
 * @author Kővágó Zoltán <DirtY.iCE.hu@gmail.com>
 */

#ifndef __LIBPROTECTSTR__PROTECTSTR_HPP
#define __LIBPROTECTSTR__PROTECTSTR_HPP

#include <string>

extern const std::string ProtectStr(const char * c_str);

#endif //__LIBPROTECTSTR__PROTECTSTR_HPP
