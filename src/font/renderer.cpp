/*
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "renderer.hpp"
#include "console/console.hpp"
#include "babyxml/baby_xml.hpp"
#include "video/color.hpp"
#include "../exceptions/base.hpp"
#include <map>
#include <vector>
#include <list>
#include <sstream>
#include <iomanip>
#include <stack>

// define to get debug on text
// #define TEXT_DEBUG

namespace FontEngine
{

/**
 * Makes a line, from a Glyph font. Converts the text to UTF-32, then get all
 * the glyphs. If the font supports scaling, try to do it. If not, it doesn't
 * scale (let the caller RenderLine to scale)
 * @return A Video::PointSurface of the final line.
 */
Video::PointSurface Renderer::MakeLine(Font& font, const std::string& text, const char pt_size, const unsigned short max_width)
{
	std::string text2 = text;
	std::string::iterator iter = text2.begin();
	std::vector<unsigned int> unicode_text;
	std::map<unsigned short, Video::PointSurface> glyphs;

	// good luck with the converting
	// if you put an invalid UTF-8 string, the program may access regions after the
	// string, and maybe causing segfault. howewer, it won't make infinitive loop! ;)
	for (; iter < text2.end(); iter++)
	{
		unsigned int glyph = 0;
		// Conversion from UTF-8 to UTF-32
		// UTF8 -> UTF-32
		// 0xxx xxxx -> 0xxx xxxx: normal ASCII characters, no conversion
		if ((*iter & 0x80) == 0)
			glyph = *iter;

		// 110x xxxx  10yy yyyy -> 0000 0xxx  xxyy yyyy
		//       *110* check. 110(0) = 0xc (the mask is 1110 0000 = 0xe0, since we have to check the zero too)
		else if(((*iter & 0xe0) == 0xc0)
				// check if the second byte start with *10* too (10(00) = 0xf)
				&& ((*(iter + 1) & 0xc0) == 0x80))
			// first mask the ---x xx-- characters (the first x block)
			// we have to push 2 right : ---- -xxx, and 8 left ---- -xxx  ---- ----
			// (one byte left). Assuming we're big endian, Font's responsibility to swap if it needs little endian.
			// but take the another two x into account: ---- --xx. this needs to get mapped as
			// ---- ---- xx-- ----. so we can easily combine the two into: ---- -111 22-- ----
			// this mean mask need to be ---x xxxx, and only push 6 left
			glyph = (((*iter & 0x1f) << 6) |
					 // the y pack. a simple mask enough: --yy yyyy
					 (*(++iter) & 0x3f));
		// 1110 xxxx  10yy yyyy  10zz zzzz -> xxxx yyyy  yyzz zzzzz
		// 1110 is 0xe, the mask is 1111 0000 = 0xf0
		else if (((*iter & 0xf0) == 0xe0) &&
				 // the other two character has to start with 10
				 ((*(iter + 1) & 0xc0) == 0x80) &&
				 ((*(iter + 2) & 0xc0) == 0x80))
			// first mask x: ---- xxxx. move them 1 and half bytes left, that's 12 bits: xxxx ----  ---- ----
			glyph = (((*iter & 0x0f) << 12) |
					 // y pack: combine --11 11-- -> ---- 1111  ---- ----
					 //                 ---- --22 -> ---- ----  22-- ----
					 //  thats          --11 1122 -> ---- 1111  22-- ----
					 // so, the mask: --yy yyyy, and shift left 6 -> ---- yyyy  yy-- ----
					 ((*(++iter) & 0x3f) << 6) |
					 // z pack: --zz zzzz
					 (*(++iter) & 0x3f));
		// 1111 0xxx  10yy yyyy  10zz zzzz  10@@ @@@@ -> 000x xxyy  yyyy zzzz  zz@@ @@@@
		// mask is 1111 1000 = 0xf8, 1111 0(000) = 0xf0
		else if (((*iter & 0xf8) == 0xf0) &&
				 ((*(iter + 1) & 0xc0) == 0x80) &&
				 ((*(iter + 2) & 0xc0) == 0x80) &&
				 ((*(iter + 3) & 0xc0) == 0x80))
			// x pack: ---- -xxx. move left 2 bytes and 2 bits (18 bits altogether)
			glyph = (((*iter & 0x07) << 18) |
					 // y pack: --11 2222, and shift it left 12: ---- --yy  yyyy ----  ---- ----
					 ((*(++iter) & 0x3f) << 12) |
					 // z pack: --zz zzzz, shift 6 left: ---- zzzz  zz-- ----
					 ((*(++iter) & 0x3f) << 6) |
					 // @ pack: --@@ @@@@, no shift needed :p
					 (*(++iter) & 0x3f));
		else
			// huhh, something went terribly wrong in the UTF-8 string..
			// replace it with Unicode replacement code: '�' (U+FFFD)
			// and countinue at the next character (so it also possible,
			// that you'll se more replacement characters next to each other.. )
			glyph = 0xfffd;
		
		unicode_text.push_back(glyph);
		//if (glyphs.count(glyph) == 0)
		//	glyphs[glyph] = font.GetGlyph(glyph, pt_size);
	}
	
	std::vector<unsigned int>::iterator unicode_iter;
	float scale = 1;
	Video::PointSurface glyph;
	unsigned short width, height, ascent, descent;

	char run = 0;
	do
	{
		glyphs.clear();
		width = 0;
		descent = ascent = 0;

		for (unicode_iter = unicode_text.begin(); unicode_iter != unicode_text.end(); unicode_iter++)
		{
			if (glyphs.count(*unicode_iter) == 0)
			{
				glyph = font.GetGlyph(*unicode_iter, pt_size, scale);
				//debug << *unicode_iter << " w " << glyph.GetWidth() << " h " << glyph.GetHeight() << std::endl;
				//debug << "x " << glyph["kerning_end"].GetX() << " y " << glyph["kerning_end"].GetY() << std::endl;				
				glyphs.insert(std::pair<unsigned int, Video::PointSurface>(*unicode_iter, glyph));
			}
		}

		for (unicode_iter = unicode_text.begin(); unicode_iter != unicode_text.end(); unicode_iter++)
		{
			glyph = glyphs[*unicode_iter];
			width += glyph["kerning_end"].GetX();

			ascent = std::max(ascent, (unsigned short) glyph["base_point"].GetY());
			descent = std::max(descent, (unsigned short) (glyph.GetHeight() - glyph["base_point"].GetY()));
		}
		width += glyph.GetWidth() - glyph["kerning_end"].GetX();
		height = ascent + descent;

		if (width)
			scale *= max_width / width;

		//prevent infinite loop if the font doesn't scale..
		run++;
	}
	while (font.CanScaleGlyph() && width > (max_width + 2) && run < 5);

#ifdef FONT_DEBUG
	debug << "width " << width << " height " << height << std::endl;
#endif

	Video::PointSurface text_surf(width, height, 0, 0, 0, 0);
	unsigned short x = 0;

	for (unicode_iter = unicode_text.begin(); unicode_iter != unicode_text.end(); unicode_iter++)
	{
		//debug << "draw " << *unicode_iter << " to " << x << ":" << ascent << std::endl;
		//debug << "with base_point " << glyph["base_point"].GetX() << ":" << glyph["base_point"].GetY() << std::endl;
		glyph = glyphs[*unicode_iter];
		text_surf.BlitInto(Video::Point(x, ascent), glyph, glyph["base_point"], false);
		x += glyph["kerning_end"].GetX();
	}

	text_surf["begin_base"] = Video::Point(0, ascent);
	text_surf["center_base"] = Video::Point(width / 2, ascent);
	text_surf["end_base"] = Video::Point(width - 1, ascent);

	return text_surf;
}

/**
 * Renders a line.
 * If font is a Glyph font, make the line from the glyphs via MakeLine(),
 * otherwise use font's algorithm. If the text is too width, scale it down.
 * @param font The font to draw with.
 * @param text The text to draw.
 * @param pt_size The size, to draw with.
 * @param max_width The maximum width of the line.
 * @return A Video::PointSurface with the line.
 */
Video::PointSurface Renderer::RenderLine(Font& font, const std::string& text, const char pt_size, const unsigned short max_width)
{
	Video::PointSurface surface;

#ifdef FONT_DEBUG
	debug << "Drawing text '" << text << "'" << std::endl;
#endif

	if (font.GetType() == Glyph)
		surface = MakeLine(font, text, pt_size, max_width);
	else
		surface = font.GetLine(text, pt_size, max_width);

	if (surface.GetWidth() > max_width + 2)
		surface.ReSize(max_width, surface.GetHeight());

	return surface;
}

/**
 * Renders a text (multiline)
 * If font is Text, use it's routines, else use RenderLine.
 * @param font The font to draw with.
 * @param text The text to draw.
 * @param pt_size The size, to draw with.
 * @param max_width The maximum width of the line.
 * @param max_height The maximum height of the text. Currently <b>UNIMPLEMENTED</b>.
 * @param skip_newline If true, skips all whitespaces (space, tab, newline) and replaces with
 *        a simple space.
 * @return A Video::PointSurface containing the text.
 */
Video::PointSurface Renderer::RenderText(Font& font, const std::string& text, const char pt_size, const unsigned short max_width, const unsigned short max_height, const FontAlign align, const bool skip_newline)
{

	if (font.GetType() == Text)
		return font.GetText(text, pt_size, max_width, max_height, align);

	// Parse the text, then render it
	BabyXML parser(text);
	BabyXML::iterator xml_iter = parser.begin();

	std::stack<Video::Color> colors;
	colors.push(Video::Color(0xff, 0xff, 0xff));

	std::deque<TextPiece> pieces;
	std::string curr_text;

	unsigned short surf_width = 0;
	
	for (; xml_iter != parser.end(); xml_iter++)
	{
		switch (xml_iter->type)
		{
			case BabyXML::Node::START_TAG:
				surf_width = std::max(surf_width, DrawPiece(curr_text, colors.top(), pieces, font, pt_size, max_width));

				if (xml_iter->content == "span")
				{
					for (BabyXML::Node::Attributes::iterator attr_iter = xml_iter->attributes.begin(); attr_iter != xml_iter->attributes.end(); attr_iter++)
					{
						if (attr_iter->name == "foreground")
						{
							std::string color_text = attr_iter->value;
							Video::Color color;
							
							if (color_text[0] == '#')
							{
								char r, g, b;
								// parse hex color
								std::istringstream color_stream(color_text.substr(1, 2));
								std::hex(color_stream);
								color_stream >> r;

								color_stream.str(color_text.substr(3, 2));
								color_stream >> g;

								color_stream.str(color_text.substr(5, 2));
								color_stream >> b;

								color.SetColorA(r, g, b);
							}
							else
								///@todo Text color select via names
								;
							
							colors.push(color);
						} // if (attr_iter->name == "foreground") 
						else
							warning << "Unknown attribute ('" << attr_iter->name << "'), ignored." << std::endl;
					} // for (BabyXML::Node::Attributes::iterator attr_iter = xml_iter->attributes.begin(); attr_iter != xml_iter->attributes.end(); attr_iter++)
				} // if (xml_iter->content == "span")
				else
					warning << "Unknown tag ('" << xml_iter->content << "'), ignored." << std::endl;

				break;

			case BabyXML::Node::END_TAG:
				surf_width = std::max(surf_width, DrawPiece(curr_text, colors.top(), pieces, font, pt_size, max_width));

				if (xml_iter->content == "span")
					colors.pop();
				else
					warning << "Unknown closing tag ('" << xml_iter->content << "'), ignored." << std::endl;

				break;

			case BabyXML::Node::TEXT:
				curr_text += xml_iter->content;
				break;
				
			// Avoid warnings with unhandled enum values (Actually BabyXML never uses them)
			default:
				break;
		} // switch (xml_iter->type)
	} // for (; xml_iter != parser.end(); xml_iter++)

	// Get the end too
	surf_width = std::max(surf_width, DrawPiece(curr_text, colors.top(), pieces, font, pt_size, max_width));

	// Oups, it's empty. To prevent segfault, return now with an empty surface..
	if (pieces.size() == 0)
		return Video::PointSurface();

	// Okay, okay. So we have the pieces list. We also know the width of the surface
	// The height is simple
	unsigned short surf_height = pieces.back().y + pieces.back().piece.GetHeight();

	Video::PointSurface surface(surf_width, surf_height, 0, 0, 0, 0);
#ifdef FONT_DEBUG
	debug << "create surface " << surf_width << " " << surf_height << std::endl;
#endif

	// This is a bit tricky
	// Get a LINE (have the same y), then align, and draw

	std::deque<TextPiece>::iterator piece_begin, piece_end, piece_curr;
	piece_begin = piece_end = piece_curr = pieces.begin();
	unsigned short y, width, offset;

	do
	{
		y = piece_begin->y;
		// get to the end of the line
		for (; (piece_end->y == y) || (piece_end != pieces.end()); piece_end++)
#ifdef FONT_DEBUG
			debug << "piece x " << piece_end->x << " y " << piece_end->y << std::endl;
#endif
		; // if debug is off..

		piece_end--;

		width = piece_end->x + piece_end->piece.GetWidth();
		switch (align)
		{
			case Left: offset = 0; break;
			case Center: offset = (surf_width / 2 ) - (width / 2); break;
			case Right: offset = surf_width - width; break;
		}
#ifdef FONT_DEBUG
		debug << "offset " << offset << std::endl;
#endif

		piece_end++;
		for (; piece_begin != piece_end; piece_begin++)
			surface.BlitInto(Video::Point(piece_begin->x + offset, piece_begin->y), piece_begin->piece, piece_begin->piece["begin_base"], false);

		piece_begin = piece_end;
	}
	while (piece_end != pieces.end());

	unsigned short x;
	switch (align)
	{
		case Left: x = 0; break;
		case Center: x = surf_width / 2; break;
		case Right: x = surf_width - 1; break;
		default: NOT_REACHED();
	}
	surface["base"] = Video::Point(x, pieces.front().piece["begin_base"].GetX());
	return surface;

}

unsigned short Renderer::DrawPiece(std::string& text, const Video::Color& color, std::deque<TextPiece>& pieces, Font& font, const char pt_size, const unsigned short max_width)
{

	Video::PointSurface surface;

	/*if (skip_newline)
	{
		std::string tmp_string, text_string = text;
		std::istringstream input(text_string);

		if (font.GetType() == Text)
		{
			// filter whitespaces
			while (input.good())tmp_string += input.get() + " ";

			surface = font.GetText(tmp_string, pt_size, max_width, max_height, align);
		}
		else
		{
			std::list<Video::PointSurface> lines;
			std::list<Video::PointSurface>::iterator iter;
			std::string readed;
			unsigned short width;
			unsigned short surf_width = 0, surf_height = 0;

			while (input.good())
			{
				input >> readed;

				// get the width of the text
				width = RenderLine(font, tmp_string + " " + readed, pt_size, -1).GetWidth();

				// if it's bigger than the max allowed size,
				if (width > max_width)
				{
					// undo it, draw only the old string
					lines.push_back(RenderLine(font, tmp_string, pt_size, max_width));
					debug << lines.back().GetHeight() << std::endl;
					tmp_string = readed;
					surf_width = std::max(surf_width, lines.back().GetWidth());
					surf_height += lines.back().GetHeight();
				}
				else
					// try the next word
					tmp_string += " " + readed;
			}
			// put into the last line too
			lines.push_back(RenderLine(font, tmp_string, pt_size, -1));
			surf_width = std::max(surf_width, lines.back().GetWidth());
			surf_height += lines.back().GetHeight();

			surface = Video::PointSurface(surf_width, surf_height, 0, 0, 0, 0);
			unsigned short y = lines.front()["begin_base"].GetY(), x;

			std::string point;
			switch (align)
			{
				case Left:   point = "begin_base"; x = 0;break;
				case Center: point = "center_base"; x = surf_width / 2; break;
				case Right:  point = "end_base"; x = surf_width - 1;
			}

			surface["base"] = Video::Point(x, y);
			
			for (iter = lines.begin(); iter != lines.end(); iter++)
			{
				debug << iter->GetWidth() << std::endl;;
				surface.BlitInto(Video::Point(x, y), *iter, (*iter)[point], false);
				y += iter->GetHeight();
			}
		} // if (font.GetType() != Text)
	} // if (skip_newline)
	else 
	{
		if (font.GetType() == Text)
			surface = font.GetText(text, pt_size, max_width, max_height, align);
		else
		{*/


			std::string tmp_string;
			size_t begin_pos = 0, end_pos = 0, c_begin = 0, c_end = 0;
			//std::list<Video::PointSurface> lines;
			unsigned short surf_width = 0; //, surf_height = 0;
			unsigned short x, y;
			if (pieces.size() == 0)
			{
				x = 0; y = 15;
			}
			else
			{
				x = pieces.back().x + pieces.back().piece.GetWidth();
				y = pieces.back().y;
			}

			do
			{
				c_end = text.find_first_of("\n\t ", c_begin);

#ifdef FONT_DEBUG
				debug << "Curr char: '" << text[c_end] << "'" << std::endl;
#endif
				if (text[c_end] == '\n')
					end_pos = c_end;
				if ((text[c_end] == '\n') || (RenderLine(font, text.substr(begin_pos, c_end - begin_pos), pt_size, -1).GetWidth() > (max_width - x)))
				{
#ifdef FONT_DEBUG
					debug << "Pushing back text '" << text.substr(begin_pos, end_pos - begin_pos) << "'" << std::endl;
#endif
					surface = RenderLine(font, text.substr(begin_pos, end_pos - begin_pos), pt_size, (max_width - x));
					surface.ColorizeGrayscale(color);
					pieces.push_back(TextPiece(x, y, surface));
					surf_width = std::max(surf_width, (unsigned short) (pieces.back().piece.GetWidth() + x));
					//surf_height += lines.back().GetHeight();
					begin_pos = c_begin = end_pos + 1;
					x = 0; y += 15; //pieces.back().piece.GetHeight();
				}
				else
				{
					end_pos = c_end;
					c_begin = c_end + 1;
				}

			}
			while (c_end != std::string::npos);

			surface = RenderLine(font, text.substr(begin_pos, end_pos - begin_pos), pt_size, (max_width - x));
			surface.ColorizeGrayscale(color);
			pieces.push_back(TextPiece(x, y, surface));

			surf_width = std::max(surf_width, (unsigned short) (pieces.back().piece.GetWidth() + x));
			text = "";
			
			return surf_width;
}
/*
			surf_height += lines.back().GetHeight();

			surface = Video::PointSurface(surf_width, surf_height, 0, 0, 0, 0);
			debug << "Made surface w=" << surf_width << " h=" << surf_height << std::endl;
			unsigned short y = lines.back()["begin_base"].GetY(), x;
			std::string point;

			switch (align)
			{
				case Left:   point = "begin_base"; x = 0; break;
				case Center: point = "center_base"; x = surf_width / 2; break;
				case Right:  point = "end_base"; x = surf_width - 1;
			}

			surface["base"] = Video::Point(x, y);

			std::list<Video::PointSurface>::iterator iter = lines.begin();
			for (; iter != lines.end(); iter++)
			{
				debug << "Blitting to 0:" << y << std::endl;
				surface.BlitInto(Video::Point(x, y), *iter, (*iter)[point], false);
				y += iter->GetHeight();
			}

		} // if (font.GetType() != Text)
	} // if (!skip_newline)

	return surface;*/


			

									
}
