/*
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "manager.hpp"
#include "console/console.hpp"

namespace FontEngine
{

Manager::~Manager()
{
	font_map.clear();
	size_map.clear();
}

/**
 * Gets a font
 * @param name Name of the font to get.
 * @return A reference to the Font.
 */
Font& Manager::GetFont(const std::string& name)
{
	return font_map[name];
}

/**
 * Loads a font.
 * @param file_name Name of the file to open.
 * @param name Name, under the font will stored.
 */
void Manager::LoadFont(const std::string& file_name, const std::string& name)
{
	debug("font", 2) << "Loading font '" << file_name << "' as '" << name << "'" << std::endl;
	font_map.insert(std::pair<std::string, FontT>(name, FontT(file_name)));
}

/**
 * Set the size_map for old style font sizing,
 * @param map A FontSizeMap containig all sizes.
 */
void Manager::MapOldSizes(const FontSizeMap& map)
{
	debug("font", 2) << "Mapping old sizes:" << std::endl;

	size_map = map;

	for(FontSizeMap::iterator iter = size_map.begin(); iter != size_map.end(); iter++)
		debug("font", 3) << "\tSet " << (int) iter->first << " to pt_size " << (int) iter->second << std::endl;
}

}

