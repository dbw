/*
Copyright (C) 2007 Kővágó Zoltán

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef FONT_MANAGER_HPP
#define FONT_MANAGER_HPP

#include "font.hpp"
#include "sdl_ttf_font.hpp"

#include <map>

namespace FontEngine
{

typedef std::map<char, char> FontSizeMap;
typedef SDLTTFFont FontT;

class Manager
{
	public:
		//Manager();
		~Manager();

		Font& GetFont(const std::string& name);
		/// @param old_size The old_style font style
		/// @return The pt_size of the old size specified in old_size
		char GetOldSize(const char& old_size) { return size_map[old_size]; };

		void LoadFont(const std::string& file_name, const std::string& name);
		void MapOldSizes(const FontSizeMap& map);

	private:
		/// A map containing the fonts.
		std::map<std::string, FontT> font_map;
		/// A map for old sizes.
		FontSizeMap size_map;
};

}

#endif //FONT_MANAGER_HPP
