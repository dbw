/*
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef FONT_RENDERER_HPP
#define FONT_RENDERER_HPP

#include "font.hpp"
#include "video/color.hpp"

#include <deque>

namespace FontEngine
{

class Renderer
{
	public:
		//Renderer();
		//~Renderer();

		static Video::PointSurface RenderLine(Font& font, const std::string& text, const char pt_size, const unsigned short max_width = -1);
		static Video::PointSurface RenderText(Font& font, const std::string& text, const char pt_size, const unsigned short max_width, const unsigned short max_height = -1, const FontAlign align = Left, const bool skip_newline = false);

	private:
		static Video::PointSurface MakeLine(Font& font, const std::string& text, const char pt_size, const unsigned short max_width);

		struct TextPiece
		{
			TextPiece(const unsigned short x, const unsigned short y, const Video::PointSurface& piece) : x(x), y(y), piece(piece) {};
			unsigned short x, y;
			Video::PointSurface piece;
		};
		static unsigned short DrawPiece(std::string& text, const Video::Color& color, std::deque<TextPiece>& pieces, Font& font, const char pt_size, const unsigned short max_width);


};

}

#endif //FONT_RENDERER_HPP
