/*
Copyright (C) 2007 Kővágó Zoltán

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef FONT_SDL_TTF_FONT_HPP
#define FONT_SDL_TTF_FONT_HPP

#include "font.hpp"
#include "../exceptions/base.hpp"

#include <map>
#include <SDL/SDL_ttf.h>

namespace FontEngine
{

/// Exception thrown when an SDL-TTF function fails.
DEF_EXCEPTION(SdlTTFError, "An error occured in SDL-TTF");

class SDLTTFFont : public Font
{
	public:
		/// Needed because of std::map; don't use it!
		SDLTTFFont() {};
		/// Constructs a font. @param file_name The file name of the font.
		SDLTTFFont(const std::string& file_name) { this->file_name = file_name; TryFont();};
		/// Copy constructor. @param font An another font.
		SDLTTFFont(const SDLTTFFont& font) { file_name = font.file_name; };
		/// Destructor + Constructor. @param font An another font.
		void operator= (const SDLTTFFont& font) { FlushFonts(); file_name = font.file_name; };
		/// Sets the font. @param file_name The file name of the font,
		void SetFont(const std::string file_name) { FlushFonts(); this->file_name = file_name; TryFont();};
		/// Destroys the font
		~SDLTTFFont() { FlushFonts(); };

		Video::PointSurface GetGlyph(const unsigned int& glyph, const char& size, const float&);
		Video::PointSurface GetLine(const std::string& text, const char& size, const short& max_width = -1);

		/// The type getter function. @return Line
		FontType GetType() const { return Line; };
		/// We can't scale glyphs. @return false
		bool CanScaleGlyph() const { return false; };

		void FlushFonts();
	protected:
		void CheckSize(const char& size);
		void TryFont();

		/// A map for different sizes of the font.
		std::map<char, TTF_Font *> ttf_fonts;
		/// The file name.
		std::string file_name;

};


}

#endif //FONT_SDL_TTF_FONT_HPP
