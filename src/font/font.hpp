/*
Copyright (C) 2007 Kővágó Zoltán

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef FONT_FONT_HPP
#define FONT_FONT_HPP

#include "video/point_surface.hpp"
#include <string>

namespace FontEngine
{

/// The type of the font engine:
enum FontType
{
	Glyph, ///< Only supports getting glyphs (one character, no text)
	Line,  ///< Supports getting one line of text (no newline support with a specified width).
	Text   ///< Fully supports multiline text, even with inserting newlines after X pixels.
};

/// Alignment of the fonts
enum FontAlign
{
	Left,   ///< Align all text to left (the default)
	Center, ///< Align texts to the center line
	Right,  ///< Align texts to right
};

/**
 * Font is an abstract type to store a font. The real font storing is done by inherited
 * classes (currently only SDLTTFFont). If you would like to add a font, read this as a
 * tutorial. Assume your font engine is named xy. Create two files in fonts folder,
 * xy_font.cpp and xy_font.hpp. Fill in xy_font.hpp, see sdl_ttf_font.hpp for an example.
 * Make constructors and destructors. Then have a look at the Get* functions. If your font's
 * type (FontType) is Glyph, you only need to make GetGlyph(). If the type is Line, you only
 * need to make GetLine(), optionally cou could write GetGlyph(). But for Text, you need
 * GetLine and GetText(). If the lib doesn't have different functions for them, the two
 * function should be the same. GetGlyph() is optionally here too. Make GetType() and
 * CanScaleGlyph() too. If you cache glyphs by your XYFont class, make the FlushCache() and
 * FlushCache(const char& size) too, otherwise leave them alone. See the function's help for
 * info about them.
 */
class Font
{
	public:
		//Font(const std::string& file_name);
		virtual ~Font() {};
		
		/**
		 * Gets a glyph from the font.
		 * @param glyph The glyph to renderer. It's an unicode (UTF-32) character.
		 *        If the character doesn't exists, and you use a lib, let your lib
		 *        to replace it with something, unless it doesn't have this feature.
		 *        In this case, use the Unicode replacement character '�' (U+FFFD).
		 *        If the font doesn't has that character, use
		 *   @code return Video::Surface(5, 5, 0xff, 0xff, 0xff, 0xff); @endcode
		 * @param size The size of the glyph it pt at 72dpi.
		 * @param scale The horizontal scale of the font. 1.0 means no scaling, < 1.0
		 *        means shrinking, in horizontal way. If not supported, simply ignore.
		 * @return A Video::Surface containing the glyph, and 2 points:
		 *         - <tt>"base_point"</tt>: The base point of the glyph. Only y cordinate
		 *          is important, x is ignored. If the base point of X is not null, you
		 *          should add the required space before the begin of glyph, since Point
		 *          is unsigned. See SDLTTFFont::GetGlyph() for an example.
		 *         - <tt>"kerning_end"</tt>: Only X cordinate, also known as advance.
		 */
		virtual Video::PointSurface GetGlyph(const unsigned int& glyph, const char& size, const float& scale = 1) { return Video::Surface(); };
		/**
		 * Gets a line from the font.
		 * @param text The text to render. It's an UTF-8 string. If your lib can't handle
		 *        it, you can use TinyGetText::convert() to do it for you.
		 * @param size The size of the glyph in pt at 72dpi.
		 * @param max_width The maximum width of the returning surface can be. If your lib
		 *        supports horizontal scaling, use it for do it, otherwise simply ignore.
		 *        Renderer::RenderLine() will shrink it..
		 * @return A Video::Surface containing the text. It should have 3 points, Y cordinates
		 *         are on the base line. X cordinates are:
		 *           - <tt>"begin_base"</tt>: 0 (The beginning of the text)
		 *           - <tt>"center_base"</tt>: width/2 (The center of the text)
		 *           - <tt>"end_base"</tt>: width-1 (The end of the text)
		 */
		virtual Video::PointSurface GetLine(const std::string& text, const char& size, const short& max_width = -1) { return Video::Surface(); };
		/**
		 * Gets some text (multiline) from the font.
		 * @param text The text to render. It's an UTF-8 string. If your lib can't handle
		 *        it, you can use TinyGetText::convert() to do it for you.
		 * @param size The size of the glyph in pt at 72dpi.
		 * @param max_width The maximum width of the returning surface can be. If it would
		 *        longer, break the line and start a new.
		 * @param max_height The maximum height the returning surface could be. If it would
		 *        bigger, you could tell the lib to draw width shrinked glyphs. Otherwise
		 *        simply ignore, Renderer::RenderText() also ignores it.. Currently it doesn't
		 *        need any Point..
		 * @param align The alignment of the font. @see FontAlign for possibles.
		 */
		virtual Video::PointSurface GetText(const std::string& text, const char& size, const short& max_width, const short& max_height = -1, const FontAlign& align = Left) { return Video::Surface(); };

		/**
		 * @return The type of the font.
		 * @see Font, FontType
		 */
		virtual FontType GetType() const = 0;
		/**
		 * Wheter the font can scale in horizontal way.
		 * @return True if it can, false otherwise..
		 */
		virtual bool CanScaleGlyph() const = 0;

		/**
		 * Flushes the cache of the font, for only one size.
		 * @param size The pt_size to flush.
		 */
		virtual void FlushCache(const char& size) {};
		/// Flushes the cache of the font for all sizes.
		virtual void FlushCache() {};

};

}

#endif //FONT_FONT_HPP
