/*
Copyright (C) 2007 Kővágó Zoltán

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef FONT_FONTENGINE_HPP
#define FONT_FONTENGINE_HPP

#include "renderer.hpp"
#include "manager.hpp"

extern FontEngine::Manager font_manager;

namespace FontEngine
{

/**
 * A utility function to draw a string to a surface.
 * With this function, you only need to type one line, not two or three to draw a text.
 * @param font The font to draw with. It's taken from font_manager, the global instance
 *        of FontEngine::Manager.
 * @param text The text to draw. Use @code _("your text") @endcode to translate the text.
 * @param pt_size The size to draw with. If you need to use old sizes, use
 *        @code font_manager.GetOldSize(old_size) @endcode
 * @param x The x position to draw to.
 * @param y The y position to draw to.
 * @param base_point The base point of the text, could called as alignment. Legal values:
 *        "begin_base", "center_base", "end_base". @see Font::GetLine() for more info.
 * @param to The Surface to draw to.
 */
static inline void DrawLine(const std::string& font, const std::string& text, const char& pt_size, const unsigned short& x, const unsigned short& y, const std::string base_point, Video::Surface to)
{
	Video::PointSurface text_s(Renderer::RenderLine(font_manager.GetFont(font), text, pt_size));
	to.BlitInto(Video::Point(x, y), text_s, text_s[base_point], false);
}


/**
 * A utility function to draw a string to a surface, with coloring.
 * With this function, you only need to type one line, not two or three to draw a text.
 * @param font The font to draw with. It's taken from font_manager, the global instance
 *        of FontEngine::Manager.
 * @param text The text to draw. Use @code _("your text") @endcode to translate the text.
 * @param pt_size The size to draw with. If you need to use old sizes, use
 *        @code font_manager.GetOldSize(old_size) @endcode
 * @param x The x position to draw to.
 * @param y The y position to draw to.
 * @param base_point The base point of the text, could called as alignment. Legal values:
 *        "begin_base", "center_base", "end_base". @see Font::GetLine() for more info.
 * @param to The Surface to draw to.
 * @param red The intensity of red.
 * @param green The intensity of green.
 * @param blue The intensity of blue
 */
static inline void DrawLine(const std::string& font, const std::string& text, const char& pt_size, const unsigned short& x, const unsigned short& y, const std::string base_point, Video::Surface to, const char& red, const char& green, const char& blue)
{
	Video::PointSurface text_s(Renderer::RenderLine(font_manager.GetFont(font), text, pt_size));
	text_s.ColorizeGrayscale(red, green, blue);
	to.BlitInto(Video::Point(x, y), text_s, text_s[base_point], false);
}


}

#endif // FONT_FONTENGINE_HPP
