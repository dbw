/*
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "sdl_ttf_font.hpp"
#include "physfs/rwops.hpp"
#include <boost/lexical_cast.hpp>

namespace FontEngine
{

/**
 * Closes all opened fonts.
 * Destructor calls this.
 */
void SDLTTFFont::FlushFonts()
{
	std::map<char, TTF_Font *>::iterator iter = ttf_fonts.begin();

	for (; iter != ttf_fonts.end(); iter++)
		TTF_CloseFont(iter->second);
}

/**
 * Gets a glyph.
 * @param glyph An UTF-32 glyph.
 * @param size The size of the font,
 * @return An Surface containing the glyph. Use <tt>"base_point" </tt> point when drawing!
 * <tt>"kerning_end"</tt> contains the end point (only X cordinate)
 */
Video::PointSurface SDLTTFFont::GetGlyph(const unsigned int& glyph, const char& size, const float&)
{
	CheckSize(size);

	SDL_Color color = { 0xff, 0xff, 0xff };

	//try to render
	SDL_Surface * surf = TTF_RenderGlyph_Blended(ttf_fonts[size], glyph, color);
	// fallback to Unicode replacement code: '�' (U+FFFD)
	if (!surf && (glyph != 0xfffd))
		return GetGlyph(0xfffd, size, 1);
	// font doesn't have that.. return nothing..
	if (!surf)
		return Video::Surface(5, 5, 0xff, 0xff, 0xff, 0xff);
	Video::PointSurface surface(surf);
	SDL_FreeSurface(surf);

	int minx, maxy, advance;
	TTF_GlyphMetrics(ttf_fonts[size], glyph, &minx, NULL, NULL, &maxy, &advance);

	// oups... Video::Point is unsigned.. :(
	if (minx != 0)
	{
		Video::Surface old_surf = surface;
		surface = Video::Surface(surface.GetWidth() + minx, surface.GetHeight(), 0, 0, 0, 0);
		surface.BlitInto(Video::Point(minx, 0), old_surf, Video::Point(0, 0), false);
	}
	surface["base_point"] = Video::Point(0, maxy);
	surface["kerning_end"] = Video::Point(advance, 0);

	//debug << "advance " << advance << std::endl;


	return surface;
}

/**
 * Renders a line. Don't use this function directly! Use Renderer::RenderLine instead.
 * @param text The string, to render.
 * @param size The size of the font
 * @param max_width Maximim width of the text. <b>IGNORED!</b>
 * @return A Surface containing the text, with the 3 usual point.
 */
Video::PointSurface SDLTTFFont::GetLine(const std::string& text, const char& size, const short& max_width)
{
	CheckSize(size);

	SDL_Color color = { 0xff, 0xff, 0xff };
	SDL_Surface * sdl_surface = TTF_RenderUTF8_Blended(ttf_fonts[size], text.c_str(), color);
	if (!sdl_surface)
		return Video::Surface();

	Video::PointSurface surface(sdl_surface);
	SDL_FreeSurface(sdl_surface);

	unsigned short ascent = TTF_FontAscent(ttf_fonts[size]);

	surface["begin_base"]  = Video::Point(0, ascent);
	surface["center_base"] = Video::Point(surface.GetWidth() / 2, ascent);
	surface["end_base"]    = Video::Point(surface.GetWidth() - 1, ascent);

	//debug << surface.GetWidth() << std::endl;

	return surface;
}

/**
 * Cheks if the specified font size if loaded.
 * If not, loads it..
 * @param size The pt_size to check
 */
void SDLTTFFont::CheckSize(const char& size)
{
	if (ttf_fonts.count(size) == 0)
	{
		ttf_fonts[size] = TTF_OpenFontRW(PhysFS::RWops(file_name), 1, size);
		if (!ttf_fonts[size])
		{
			SdlTTFError e(AT);
			e.AddInfo("TTF_GetError()", TTF_GetError());
			e.AddInfo("File tried to load", file_name);
			e.AddInfo("Size", boost::lexical_cast<std::string>(size));
			throw e;
		}
	}
}

/**
 * Tries to load the font. Throws an exception, if it fails.
 * When everything is OK, it free the font.
 * Used in constructor and in SetFont to check if the font is ok,
 * and only load something, when we need it..
 */
void SDLTTFFont::TryFont()
{
	TTF_Font * font = TTF_OpenFontRW(PhysFS::RWops(file_name), 1, 20);
	if (!font)
	{
		SdlTTFError e(AT);
		e.AddInfo("TTF_GetError()", TTF_GetError());
		e.AddInfo("File tried to load", file_name);
		e.AddInfo("Size", "20");
		throw e;
	}

	TTF_CloseFont(font);
}

}

