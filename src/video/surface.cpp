/*
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "surface.hpp"
#include "console/console.hpp"

#include <cmath>

// define to enable colors debug
// (you'll get 3 line for every blitted pixel!,
// don't uncomment, unless something is wery wrong with blitting! )
// #define COLOR_DEBUG

// define to get blit debug
// #define BLIT_DEBUG

namespace Video{

/**
 * Initialises a Surface, to the specified width and height
 * @param width The width of the Surface
 * @param height The height of the surface
 * @param r Value of red,
 * @param g green,
 * @param b blue and
 * @param a alpha. The full surface will set to this color.
 */
Surface::Surface(const unsigned short width, const unsigned short height, const char r, const char g, const char b, const char a)
{
	surface = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA, width, height, 32, rmask, gmask, bmask, amask);
	
	Uint32* pixbuf = (Uint32*) surface->pixels;
	
	// we could so without them, but fast up the progress, so the
	// program only have to calculate these values once..
	int max = width * height;
	Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
	
	// set it..
	for (int i = 0; i < max; i++)
		pixbuf[i] = color;

}

/**
 * Blits an another surface into the surface
 * @param dst_point A Point to where should the surface blit to
 * @param src_surface The surface to blit into this surface
 * @param src_point The anchor point in src_surface. It's usally (and by default) is (0, 0)
 *  which means to not do anything interesting. If it differs, the surface will blitted
 *  to dst_point - src_point..
 * @param fast_blit Use SDL's Blit function. Much faster, but have problems with blitting to alpha surfaces.
 */
void Surface::BlitInto(const Point& dst_point, const Surface& src_surface, const Point& src_point, const bool fast_blit)
{
	int x = dst_point.GetX();
	int y = dst_point.GetY();

	x -= src_point.GetX();
	y -= src_point.GetY();

#ifdef BLIT_DEBUG
	debug("blit", 5) << "Blit to " << x << ":" << y << "  size " << src_surface.GetWidth() << "x" << src_surface.GetHeight() << std::endl;
#endif

	// if it's outside the image, don't blame
	if (((x + src_surface.GetWidth()) < 0) ||
		((y + src_surface.GetHeight()) < 0) ||
		(x >= GetWidth()) || (y >= GetHeight()))
		return;

	// SDL_BlitSurface needs SDL_Rect...
	SDL_Rect rect;
	rect.x = x;
	rect.y = y;

	if (fast_blit)
		SDL_BlitSurface(src_surface.surface, NULL, surface, &rect);
	else
	{
		int * dst_pixbuf, * src_pixbuf;
		dst_pixbuf = (int *) surface->pixels;
		src_pixbuf = (int *) src_surface.surface->pixels;
		
		int cx, cy, mx, my;
		Uint8 r[3], g[3], b[3], a[3];

		mx = std::min(x + src_surface.GetWidth(), (int) GetWidth());
		my = std::min(y + src_surface.GetHeight(), (int) GetHeight());
		
		for (cx = std::max(x, 0); cx < mx; cx++)
			for (cy = std::max(y, 0); cy < my; cy++)
			{
				SDL_GetRGBA(src_pixbuf[((cy - y) * src_surface.GetWidth()) + (cx - x)], src_surface.surface->format, r, g, b, a);
				SDL_GetRGBA(dst_pixbuf[(cy * GetWidth()) + cx], surface->format, &r[1], &g[1], &b[1], &a[1]);

				float mpy = (float) (a[0]) / 255;

#ifdef COLOR_DEBUG
				debug("color", 10) << " r0 " << (int) r[0] << " g0 " << (int) g[0] << " b0 " << (int) b[0] << " a0 " << (int) a[0] << std::endl;
				debug("color", 10) << " r1 " << (int) r[1] << " g1 " << (int) g[1] << " b1 " << (int) b[1] << " a1 " << (int) a[1] << std::endl;
#endif

				r[2] = static_cast<Uint8>((r[0] - r[1]) * mpy + r[1]);
				g[2] = static_cast<Uint8>((g[0] - g[1]) * mpy + g[1]);
				b[2] = static_cast<Uint8>((b[0] - b[1]) * mpy + b[1]);

				a[2] = static_cast<Uint8>((255 - a[1]) * mpy + a[1]);

#ifdef COLOR_DEBUG
				debug("color", 10) << " r2 " << (int) r[2] << " g2 " << (int) g[2] << " b2 " << (int) b[2] << " a2 " << (int) a[2] << std::endl;
#endif

				dst_pixbuf[(cy * GetWidth()) + cx] = SDL_MapRGBA(surface->format, r[2], g[2], b[2], a[2]);
			}
	} // if (!fast_blit)
}

/**
 * Resizes the surface via bilinear interpolation,
 * @see http://en.wikipedia.org/wiki/Bilinear_interpolation for more info
 * @param width The width to resize to
 * @param height The height to resize to
 * @todo Move resizing code to a protected function, so we don't have to duplicate code in PointSurface..
 * @bug Only enlarging works..
 */
void Surface::ReSize(const unsigned short width, const unsigned short height)
{
	// If the requested size is the same as the current, don't blame
	if ((GetWidth() == width) && (GetHeight() == height))
		return;

	SDL_Surface * resized;
	resized = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA, width, height, 32, rmask, gmask, bmask, amask);

	// how much we have to scale
	float xscale = (float) GetWidth()  / width;
	float yscale = (float) GetHeight() / height;

	// x and y pos in the original image
	int fromx, fromy;

	// pixel buffers.. maybe we should use a function to access the image
	int * scaled_pixbuf, * pixbuf;
	scaled_pixbuf = (int *) resized->pixels;
	pixbuf = (int *) surface->pixels;

	//store the colors [0..3] : original colors, [4]: interpolated color
	Uint8 r[5], g[5], b[5], a[5];
	//plus how may pixels (less than 1, for bilinear interpolation)
	float plusx, plusy;

	for (int x = 0; x < width; x++)
		for (int y = 0; y < height; y++)
		{
			fromx = static_cast<int>(floor(x * xscale));
			fromy = static_cast<int>(floor(y * yscale));

			plusx = (x * xscale) - fromx;
			plusy = (y * yscale) - fromy;
				
			// set the source colors [0]: topleft, [1]: topright, [2]: bottomleft, [3]: bottomright
			SDL_GetRGBA(pixbuf[(fromy * GetWidth()) + fromx], surface->format, r, g, b, a);
			SDL_GetRGBA(pixbuf[(fromy * GetWidth()) + std::min(fromx + 1, GetWidth() - 1)], surface->format, &r[1], &g[1], &b[1], &a[1]);
			SDL_GetRGBA(pixbuf[(std::min(fromy + 1, GetWidth() - 1) * GetWidth()) + fromx], surface->format, &r[2], &g[2], &b[2], &a[2]);
			SDL_GetRGBA(pixbuf[(std::min(fromy + 1, GetWidth() - 1) * GetWidth()) + std::min(fromx + 1, GetWidth() -1)], surface->format, &r[3], &g[3], &b[3], &a[3]);

			// calculate bilinear interpolation
			// see http://en.wikipedia.org/wiki/Bilinear_interpolation for more info
			r[4] = static_cast<Uint8>(r[0] * (1 - plusx) * (1 - plusy) + r[1] * plusx * (1 - plusy) + r[2] * (1 - plusx) * plusy + r[3] * plusx * plusy);
			g[4] = static_cast<Uint8>(g[0] * (1 - plusx) * (1 - plusy) + g[1] * plusx * (1 - plusy) + g[2] * (1 - plusx) * plusy + g[3] * plusx * plusy);
			b[4] = static_cast<Uint8>(b[0] * (1 - plusx) * (1 - plusy) + b[1] * plusx * (1 - plusy) + b[2] * (1 - plusx) * plusy + b[3] * plusx * plusy);
			a[4] = static_cast<Uint8>(a[0] * (1 - plusx) * (1 - plusy) + a[1] * plusx * (1 - plusy) + a[2] * (1 - plusx) * plusy + a[3] * plusy * plusy);
				
			// we've done, set the color
			scaled_pixbuf[(y * GetWidth()) + x] = SDL_MapRGBA(resized->format, r[4], g[4], b[4], a[4]);
		}

	// ok, free the old surface and replace with the new
	SDL_FreeSurface(surface);
	surface = resized;

}

/**
 * Colorizes a GRAYSCALE Surface.
 * @warning The surface really need to be grayscale. We don't have really grayscale surfaces,
 * every surface is 32bpp, so we can't check if the surface is grayscale, however, this
 * algorythm only works with them..
 * @param color The Color to colorize
 */
void Surface::ColorizeGrayscale(const Color& color)
{

	Uint8 new_red, new_green, new_blue, value, alpha;
	Uint32 * pixbuf = (Uint32 *) surface->pixels;

	for (int x = 0; x < surface->w; x++)
		for (int y = 0; y < surface->h; y++)
		{
			// it segfaults with NULL pointer, so set the three color pointer to &value
			// if the surface is really grayscale, the three colors have the same value
			SDL_GetRGBA(pixbuf[(y * surface->w) + x], surface->format, &value, &value, &value, &alpha);

			new_red   = (color.GetR() * value ) / 255;
			new_green = (color.GetG() * value) / 255;
			new_blue  = (color.GetB() * value) / 255;

			pixbuf[(y * surface->w) + x] = SDL_MapRGBA(surface->format, new_red, new_green, new_blue, alpha);
		}
}

}


