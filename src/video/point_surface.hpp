/*
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef VIDEO_POINT_SURFACE_HPP
#define VIDEO_POINT_SURFACE_HPP

#include "surface.hpp"

namespace Video
{

class PointSurface : public Surface
{
	public:
		PointSurface() { surface = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA, 0, 0, 32, rmask, gmask, bmask, amask); };
		PointSurface(const unsigned short width, const unsigned short height) { surface = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA, width, height, 32, rmask, gmask, bmask, amask); };
		PointSurface(const unsigned short width, const unsigned short height, const char r, const char g, const char b, const char a);
		PointSurface(SDL_Surface * surface) { this->surface = surface; surface->refcount++; };
		PointSurface(const PointSurface& surface) { this->surface = surface.surface; this->surface->refcount++; this->point_map = surface.point_map; };
		PointSurface(const Surface& surface) { this->surface = surface.GetSDLSurface(); this->surface->refcount++; };
		void operator= (const PointSurface& surface) { SDL_FreeSurface(this->surface); this->surface = surface.surface; this->surface->refcount++; this->point_map = surface.point_map; };
		void operator= (const Surface& surface) { SDL_FreeSurface(this->surface); this->surface = surface.GetSDLSurface(); this->surface->refcount++; point_map.clear(); };
		//~PointSurface() { SDL_FreeSurface(surface); };

		//unsigned short GetWidth() const { return surface->w; };
		//unsigned short GetHeight() const { return surface->h; };

		//SDL_Surface * GetSDLSurface() { return surface; };

		//void BlitInto(const Point& dst_point, const PointSurface& src_surface, const Point& src_point = Point(0, 0));
		void ReSize(const unsigned short width, const unsigned short height);

		Point& operator[] (const std::string& name) { return point_map[name]; };
	protected:
		//SDL_Surface * surface;
		std::map<std::string, Point> point_map;
};

}

#endif //VIDEO_POINT_SURFACE_HPP
