/*
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef VIDEO_SURFACE_HPP
#define VIDEO_SURFACE_HPP

#include "point.hpp"
#include "color.hpp"
#include "sdl_masks.hpp"

#include <map>
#include <string>
#include <SDL/SDL.h>

namespace Video
{

class Surface
{
	public:
		Surface() { surface = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA, 0, 0, 32, rmask, gmask, bmask, amask); };
		Surface(const unsigned short width, const unsigned short height) { surface = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA, width, height, 32, rmask, gmask, bmask, amask); };
		Surface(const unsigned short width, const unsigned short height, const char r, const char g, const char b, const char a);
		Surface(SDL_Surface * surface) { this->surface = surface; surface->refcount++; };
		Surface(const Surface& surface) { this->surface = surface.surface; this->surface->refcount++; };
		void operator= (const Surface& surface) { SDL_FreeSurface(this->surface); this->surface = surface.surface; this->surface->refcount++; };
		~Surface() { SDL_FreeSurface(surface); };

		unsigned short GetWidth() const { return surface->w; };
		unsigned short GetHeight() const { return surface->h; };

		SDL_Surface * GetSDLSurface()const { return surface; };

		void BlitInto(const Point& dst_point, const Surface& src_surface, const Point& src_point = Point(0, 0), const bool fast_blit = true);
		void ReSize(const unsigned short width, const unsigned short height);
		void ColorizeGrayscale(const Color& color);
		void ColorizeGrayscale(const unsigned char red, const unsigned char green, const unsigned char blue) { ColorizeGrayscale(Color(red, green, blue)); };

		//Point& operator[] (const std::string& name) { return point_map[name]; };
	protected:
		SDL_Surface * surface;
};

}

#endif //VIDEO_SURFACE_HPP
