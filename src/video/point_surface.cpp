/*
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "point_surface.hpp"

#include <cmath>

namespace Video{

/**
 * Initialises a PointSurface, to the specified width and height
 * @param width The width of the Surface
 * @param height The height of the surface
 * @param r Value of red,
 * @param g green,
 * @param b blue and
 * @param a alpha. The full surface will set to this color.
 */
PointSurface::PointSurface(const unsigned short width, const unsigned short height, const char r, const char g, const char b, const char a)
{
	surface = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA, width, height, 32, rmask, gmask, bmask, amask);
	
	Uint32* pixbuf = (Uint32*) surface->pixels;
	
	// we could so without them, but fast up the progress, so the
	// program only have to calculate these values once..
	int max = width * height;
	Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
	
	// set it..
	for (int i = 0; i < max; i++)
		pixbuf[i] = color;

}

/**
 * Blits an another surface into the surface
 * @param dst_point A Point to where should the surface blit to
 * @param src_surface The surface to blit into this surface
 * @param src_point The anchor point in src_surface. It's usally (and by default) is (0, 0)
 *  which means to not do anything interesting. If it differs, the surface will blitted
 *  to dst_point - src_point..
 */
/*void Surface::BlitInto(const Point& dst_point, const Surface& src_surface, const Point& src_point)
{
	int x = dst_point.GetX();
	int y = dst_point.GetY();

	x -= src_point.GetX();
	y -= src_point.GetY();

	// if it's outside the image, don't blame
	if (((x + src_surface.GetWidth()) < 0) ||
		((y + src_surface.GetHeight()) < 0) ||
		(x >= GetWidth()) || (y >= GetHeight()))
		return;

	// SDL_BlitSurface needs SDL_Rect...
	SDL_Rect rect;
	rect.x = x;
	rect.y = y;

	SDL_BlitSurface(src_surface.surface, NULL, surface, &rect);
	}*/

/**
 * Resizes the surface via bilinear interpolation,
 * @see http://en.wikipedia.org/wiki/Bilinear_interpolation for more info
 * @param width The width to resize to
 * @param height The height to resize to
 */
void PointSurface::ReSize(const unsigned short width, const unsigned short height)
{
	// If the requested size is the same as the current, don't blame
	if ((GetWidth() == width) && (GetHeight() == height))
		return;

	SDL_Surface * resized;
	resized = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA, width, height, 32, rmask, gmask, bmask, amask);

	// how much we have to scale
	float xscale = (float) GetWidth()  / width;
	float yscale = (float) GetHeight() / height;

	// x and y pos in the original image
	int fromx, fromy;

	// pixel buffers.. maybe we should use a function to access the image
	int * scaled_pixbuf, * pixbuf;
	scaled_pixbuf = (int *) resized->pixels;
	pixbuf = (int *) surface->pixels;

	//store the colors [0..3] : original colors, [4]: interpolated color
	Uint8 r[5], g[5], b[5];
	//plus how may pixels (less than 1, for bilinear interpolation)
	float plusx, plusy;

	for (int x = 0; x < width; x++)
		for (int y = 0; y < height; y++)
		{
			fromx = static_cast<int>(floor(x * xscale));
			fromy = static_cast<int>(floor(y * yscale));

			plusx = (x * xscale) - fromx;
			plusy = (y * yscale) - fromy;
				
			// set the source colors [0]: topleft, [1]: topright, [2]: bottomleft, [3]: bottomright
			SDL_GetRGB(pixbuf[(fromy * GetWidth()) + fromx], surface->format, r, g, b);
			SDL_GetRGB(pixbuf[(fromy * GetWidth()) + std::min(fromx + 1, GetWidth() - 1)], surface->format, &r[1], &g[1], &b[1]);
			SDL_GetRGB(pixbuf[(std::min(fromy + 1, GetWidth() - 1) * GetWidth()) + fromx], surface->format, &r[2], &g[2], &b[2]);
			SDL_GetRGB(pixbuf[(std::min(fromy + 1, GetWidth() - 1) * GetWidth()) + std::min(fromx + 1, GetWidth() -1)], surface->format, &r[3], &g[3], &b[3]);

			// calculate bilinear interpolation
			// see http://en.wikipedia.org/wiki/Bilinear_interpolation for more info
			r[4] = static_cast<Uint8>(r[0] * (1 - plusx) * (1 - plusy) + r[1] * plusx * (1 - plusy) + r[2] * (1 - plusx) * plusy + r[3] * plusx * plusy);
			g[4] = static_cast<Uint8>(g[0] * (1 - plusx) * (1 - plusy) + g[1] * plusx * (1 - plusy) + g[2] * (1 - plusx) * plusy + g[3] * plusx * plusy);
			b[4] = static_cast<Uint8>(b[0] * (1 - plusx) * (1 - plusy) + b[1] * plusx * (1 - plusy) + b[2] * (1 - plusx) * plusy + b[3] * plusx * plusy);
				
			// we've done, set the color
			scaled_pixbuf[(y * GetWidth()) + x] = SDL_MapRGB(resized->format, r[4], g[4], b[4]);
		}

	// ok, free the old surface and replace with the new
	SDL_FreeSurface(surface);
	surface = resized;

	std::map<std::string, Point>::iterator iter = point_map.begin();

	for (; iter != point_map.end(); iter++)
		point_map[iter->first] = Video::Point(static_cast<int>((iter->second.GetX()) * xscale), static_cast<int>((iter->second.GetY()) * yscale));

}

}


