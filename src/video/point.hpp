/*
Copyright (C) 2007 Kővágó Zoltán

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef VIDEO_POINT_HPP
#define VIDEO_POINT_HPP

namespace Video
{

class Point
{
	public:
		Point() { x = 0; y = 0; };
		Point(const int& x, const int& y) { this->x = x; this->y = y; };
		
		void SetPoint(const int& x, const int& y) { this->x = x; this->y = y; };
		int GetX() const { return x; };
		int GetY() const { return y; };
		
	private:
		int x, y;
};

}

#endif //VIDEO_POINT_HPP
