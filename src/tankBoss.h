/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef TANKBOSS_H
#define TANKBOSS_H

#include "CBoss.h"
#include "CEntity.h"

extern void checkObjectives(const char *name, bool alwaysInform);
extern void addColorParticles(float x, float y, int amount, int color);
extern void addBullet(Entity *owner, float dx, float dy);
extern void addExplosion(float x, float y, int radius, Entity *owner);
extern void addBlood(Entity *ent, float dx, float dy, int amount);
extern void addSmokeAndFire(Entity *ent, float dx, float dy, int amount);
extern void throwAndDamageEntity(Entity *ent, int damage, int minDX, int maxDX, int DY);
extern Weapon *getWeaponByName(const char *name);
extern Entity *getDefinedEnemy(const char *name);

// this is used exclusively by the bosses
extern Boss *self;

#endif //TANKBOSS_H
