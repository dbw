/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <SDL_image.h>
#include <stdexcept>

#include "CGraphics.h"
#include "CMath.h"
#include "font/fontengine.hpp"
#include "gettext.hpp"
#include "console/console.hpp"
#include "exceptions/base.hpp"
#include "physfs/manager.hpp"
#include "physfs/rwops.hpp"

Graphics::Graphics()
{
	for (int i = 0 ; i < MAX_TILES ; i++)
	{
		tile[i] = NULL;
	}

	background = NULL;
	infoMessage = NULL;

	fontSize = 0;

	currentLoading = 0;

	screenShotNumber = 0;
	takeRandomScreenShots = false;

	waterAnim = 201;
	slimeAnim = 208;
	lavaAnim = 215;

	width = 640; height = 480;
}

void Graphics::free()
{
	debug("graphics", 1) << "Freeing data" << std::endl;
	debug("graphics", 2) << "graphics.free: Background" << std::endl;
	if (background != NULL)
	{
		SDL_FreeSurface(background);
	}
	debug("graphics", 2) << "graphics.free: Background - Done" << std::endl;

	background = NULL;

	debug("graphics", 2) << "graphics.free: Tiles" << std::endl;
	for (int i = 0 ; i < MAX_TILES ; i++)
	{
		if (tile[i] != NULL)
		{
			SDL_FreeSurface(tile[i]);
			tile[i] = NULL;
		}
	}
	debug("graphics", 2) << "graphics.free: Tiles - Done" << std::endl;

	debug("graphics", 2) << "graphics.free: Sprites" << std::endl;
	Sprite *sprite = (Sprite*)spriteList.getHead();
	while (sprite->next != NULL)
	{
		sprite = (Sprite*)sprite->next;
		debug("graphics", 3) << "graphics.free: Sprites Sprite::Free - '" << sprite->name << "'" << std::endl;
		sprite->free();
	}
	debug("graphics", 2) << "graphics.free: Sprites Clear()" << std::endl;
	spriteList.clear();
	debug("graphics", 2) << "graphics.free: Sprites - Done" << std::endl;
}

void Graphics::destroy()
{
	free();

	for (int i = 0 ; i < 5 ; i++)
	{
		if (font[i])
		{
			TTF_CloseFont(font[i]);
		}
	}


	if (fadeBlack)
	{
		SDL_FreeSurface(fadeBlack);
	}

	if (infoBar)
	{
		SDL_FreeSurface(infoBar);
	}
}

void Graphics::registerEngine(Engine *engine)
{
	this->engine = engine;
}

void Graphics::mapColors()
{
	red = SDL_MapRGB(screen->format, 0xff, 0x00, 0x00);
	yellow = SDL_MapRGB(screen->format, 0xff, 0xff, 0x00);
	green = SDL_MapRGB(screen->format, 0x00, 0xff, 0x00);
	darkGreen = SDL_MapRGB(screen->format, 0x00, 0x77, 0x00);
	skyBlue = SDL_MapRGB(screen->format, 0x66, 0x66, 0xff);
	blue = SDL_MapRGB(screen->format, 0x00, 0x00, 0xff);
	cyan = SDL_MapRGB(screen->format, 0x00, 0x99, 0xff);
	white = SDL_MapRGB(screen->format, 0xff, 0xff, 0xff);
	lightGrey = SDL_MapRGB(screen->format, 0xcc, 0xcc, 0xcc);
	grey = SDL_MapRGB(screen->format, 0x88, 0x88, 0x88);
	darkGrey = SDL_MapRGB(screen->format, 0x33, 0x33, 0x33);
	black = SDL_MapRGB(screen->format, 0x00, 0x00, 0x00);

	fontForeground.r = fontForeground.g = fontForeground.b = 0xff;
	fontBackground.r = fontBackground.g = fontBackground.b = 0x00;

	fontForeground.unused = fontBackground.unused = 0;

	fadeBlack = alphaRect(640, 480, 0x00, 0x00, 0x00);

	infoBar = alphaRect(640, 25, 0x00, 0x00, 0x00);
}

Sprite *Graphics::getSpriteHead()
{
	return (Sprite*)spriteList.getHead();
}

void Graphics::setTransparent(SDL_Surface *sprite)
{
	SDL_SetColorKey(sprite, (SDL_SRCCOLORKEY|SDL_RLEACCEL), SDL_MapRGB(sprite->format, 0, 0, 0));
}

void Graphics::updateScreen()
{
	SDL_Flip(screen);

	if (takeRandomScreenShots)
	{
		if ((rand() % 500) == 0)
		{
			sprintf(screenshot, "screenshots/screenshot%.3d.bmp", screenShotNumber);
			SDL_SaveBMP(screen, screenshot);
			screenShotNumber++;
		}

	}

	if (engine->keyState[SDLK_F12])
	{
		sprintf(screenshot, "screenshots/screenshot%.3d.bmp", screenShotNumber);
		SDL_SaveBMP(screen, screenshot);
		screenShotNumber++;

		engine->keyState[SDLK_F12] = 0;
	}

	if ((engine->keyState[SDLK_F10]) || ((engine->keyState[SDLK_RETURN]) && (engine->keyState[SDLK_LALT])))
	{
		SDL_WM_ToggleFullScreen(screen);
		engine->fullScreen = !engine->fullScreen;

		engine->keyState[SDLK_F10] = engine->keyState[SDLK_LALT] = engine->keyState[SDLK_RETURN] = 0;
	}
}

void Graphics::delay(int time)
{
	unsigned long then = SDL_GetTicks();

	engine->keyState[SDLK_ESCAPE] = 0;

	while (true)
	{
		updateScreen();
		
		if (SDL_GetTicks() >= then + time)
		{
			break;
		}

		engine->getInput();
		
		if (engine->keyState[SDLK_ESCAPE])
		{
			break;
		}
	}
}

void Graphics::RGBtoHSV(float r, float g, float b, float *h, float *s, float *v)
{
	float mn, mx, delta;
	mn = std::min(std::min(r, g), b);
	mx = std::max(std::max(r, g), b);
	*v = mx;
	delta = mx - mn;

	if (mx != 0)
	{
		*s = delta / mx;
	}
	else
	{
		*s = 0;
		*h = -1;
		return;
	}

	if (r == mx)
	{
		*h = (g - b) / delta;
	}
	else if (g == mx)
	{
		*h = 2 + (b - r) / delta;
	}
	else
	{
		*h = 4 + (r - g) / delta;
	}

	*h *= 60;

	if (*h < 0)
	{
		*h += 360;
	}
}

void Graphics::HSVtoRGB(float *r, float *g, float *b, float h, float s, float v)
{
	int i;
	float f, p, q, t;
	if (s == 0)
	{
		*r = *g = *b = v;
		return;
	}

	h /= 60;
	i = (int)(h);
	f = h - i;
	p = v * (1 - s);
	q = v * (1 - s * f);
	t = v * (1 - s * (1 - f));

	switch (i)
	{
		case 0:
			*r = v;
			*g = t;
			*b = p;
			break;

		case 1:
			*r = q;
			*g = v;
			*b = p;
			break;

		case 2:
			*r = p;
			*g = v;
			*b = t;
			break;

		case 3:
			*r = p;
			*g = q;
			*b = v;
			break;

		case 4:
			*r = t;
			*g = p;
			*b = v;
			break;

		default:
			*r = v;
			*g = p;
			*b = q;
			break;
	}
}

SDL_Surface *Graphics::loadImage(const char *filename, bool colorkey)
{
	SDL_Surface *image, *newImage;

	SDL_RWops * buffer = PhysFS::RWops(filename);
	image = IMG_Load_RW(buffer, 1);

	if (!image)
		showErrorAndExit(ERR_FILE, filename);

	if (colorkey)
		newImage = SDL_DisplayFormat(image);
	else
		newImage = SDL_DisplayFormatAlpha(image);

	if (newImage)
	{
		SDL_FreeSurface(image);
	}
	else
	{
		// This happens when we are loading the window icon image
		newImage = image;
	}

	if (colorkey)
		setTransparent(newImage);

	return newImage;
}

SDL_Surface *Graphics::loadImage(const char *filename, int hue, int sat, int value)
{
	SDL_Surface *image, *newImage;

	SDL_RWops * buffer = PhysFS::RWops(filename);
	image = IMG_Load_RW(buffer, 1);

	if (!image)
		showErrorAndExit(ERR_FILE, filename);

	if ((hue != 0) || (sat != 0) || (value != 0))
	{
		if (image->format->BitsPerPixel != 8)
		{
			warning << "Could not set Hue for '" << filename << "'! Not an 8 bit image!" << std::endl;
		}
		else
		{
			SDL_Color *color;
			float r, g, b, h, s, v;

			if (image->format->palette->colors != NULL)
			{
				for (int i = 1 ; i < image->format->palette->ncolors ; i++)
				{
					color = &image->format->palette->colors[i];

					r = (int)color->r;
					g = (int)color->g;
					b = (int)color->b;

					RGBtoHSV(r, g, b, &h, &s, &v);

					h += hue;
					s += sat;
					v += value;

					HSVtoRGB(&r, &g, &b, h, s, v);

					color->r = (int)r;
					color->g = (int)g;
					color->b = (int)b;

				}
			}
		}
	}

	newImage = SDL_DisplayFormat(image);

	if (newImage)
	{
		SDL_FreeSurface(image);
	}
	else
	{
		// This happens when we are loading the window icon image
		newImage = image;
	}

	setTransparent(newImage);

	return newImage;
}

SDL_Surface *Graphics::quickSprite(const char *name, SDL_Surface *image)
{
	Sprite *sprite = addSprite(name);
	sprite->setFrame(0, image, 60);

	return sprite->getCurrentFrame();
}

void Graphics::fade(int amount)
{
	SDL_SetAlpha(fadeBlack, SDL_SRCALPHA|SDL_RLEACCEL, amount);
	blit(fadeBlack, 0, 0, screen, false);
}

void Graphics::fadeToBlack()
{
	int start = 0;

	while (start < 50)
	{
		SDL_SetAlpha(fadeBlack, SDL_SRCALPHA|SDL_RLEACCEL, start);
		blit(fadeBlack, 0, 0, screen, false);
		delay(60);
		start++;
	}
}

void Graphics::loadMapTiles(const char *baseDir)
{
	bool autoAlpha;
	char filename[255];
	strcpy(filename, "");

	autoAlpha = false;
	
	if (strcmp(baseDir, "gfx/common") == 0)
	{
		autoAlpha = true;
	}

	for (int i = 1 ; i < MAX_TILES ; i++)
	{
		sprintf(filename, "%s/%d.png", baseDir, i);

		if (PhysFS::Manager::FileExists(filename))
		{
			tile[i] = loadImage(filename);
			
			if (i < MAP_DECORATION)
			{
				SDL_SetColorKey(tile[i], 0, 0);
			}

			if (autoAlpha)
			{
				if ((i < MAP_EXITSIGN) || (i >= MAP_WATERANIM))
				{
					SDL_SetAlpha(tile[i], SDL_SRCALPHA|SDL_RLEACCEL, 128);
				}
			}
		}
	}
}

/**
 * Load a font file with the specified size.
 * @param i: the font id
 * @param filename: the name of the font file, loaded through PhysFS
 * @param pt_size: the size of the font in <i>pt</i> at <b>72 dpi</b>.
 */
void Graphics::loadFont(int i, const char *filename, int pt_size)
{
	debug("graphics", 2) << "Attempting to load a font with pt size of " << pt_size << std::endl;
	
	if (font[i])
	{
		debug("graphics", 3) << "Freeing Font " << i << " first..." << std::endl;
		TTF_CloseFont(font[i]);
	}
	
	SDL_RWops *buffer = PhysFS::RWops(filename);
	font[i] = TTF_OpenFontRW(buffer, 1, pt_size);

	if (!font[i])
	{
		engine->reportFontFailure();
	}
		
	TTF_SetFontStyle(font[i], TTF_STYLE_NORMAL);
	
	debug("graphics", 3) << "Loaded font '" << filename << "' with ptsize " << pt_size << std::endl;
}

Sprite *Graphics::addSprite(const char *name)
{
	Sprite *sprite = new Sprite;
	strcpy(sprite->name, name);

	spriteList.add(sprite);

	return sprite;
}

Sprite *Graphics::getSprite(const char *name, const bool& required)
{
	Sprite *sprite = (Sprite*)spriteList.getHead();

	while (sprite->next != NULL)
	{
		sprite = (Sprite*)sprite->next;
		
		if (strcmp(sprite->name, name) == 0)
		{
			return sprite;
		}
	}

	if (required)
		showErrorAndExit(_("The requested sprite '%s' does not exist"), name);

	return NULL;
}

void Graphics::animateSprites()
{
	Sprite *sprite = (Sprite*)spriteList.getHead();

	while (sprite->next != NULL)
	{
		sprite = (Sprite*)sprite->next;

		sprite->animate();
	}

	if ((engine->getFrameLoop() % 8) == 0)
	{
		Math::wrapInt(&(++waterAnim), 201, 204);
		Math::wrapInt(&(++slimeAnim), 207, 212);
		Math::wrapInt(&(++lavaAnim), 214, 220);
	}
}

int Graphics::getWaterAnim()
{
	return waterAnim;
}

int Graphics::getSlimeAnim()
{
	return slimeAnim;
}

int Graphics::getLavaAnim()
{
	return lavaAnim;
}

int Graphics::getLavaAnim(int current)
{
	if ((engine->getFrameLoop() % 8) == 0)
		return Math::rrand(214, 220);

	return current;
}

void Graphics::loadBackground(const char *filename, bool tile /* = true */)
{
	if (background != NULL)
		SDL_FreeSurface(background);

	if (strcmp(filename, "@none@") == 0)
		return;

	background = loadImage(filename);

	SDL_SetColorKey(background, 0, SDL_MapRGB(background->format, 0, 0, 0));

	background_tile = tile;
}

void Graphics::putPixel(int x, int y, Uint32 pixel, SDL_Surface *dest)
{
	if ((x < 0) || (x > 639) || (y < 0) || (y > 479))
		return;

	int bpp = dest->format->BytesPerPixel;
	/* Here p is the address to the pixel we want to set */
	Uint8 *p = (Uint8 *)dest->pixels + y * dest->pitch + x * bpp;

	switch(bpp)
	{
		case 1:
			*p = pixel;
			break;

		case 2:
			*(Uint16 *)p = pixel;
			break;

		case 3:
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
				p[0] = (pixel >> 16) & 0xff;
				p[1] = (pixel >> 8) & 0xff;
				p[2] = pixel & 0xff;
#else
				p[0] = pixel & 0xff;
				p[1] = (pixel >> 8) & 0xff;
				p[2] = (pixel >> 16) & 0xff;
#endif
			break;

		case 4:
			*(Uint32 *)p = pixel;
			break;
	}
}

Uint32 Graphics::getPixel(SDL_Surface *surface, int x, int y)
{
	if ((x < 0) || (x > (surface->w - 1)) || (y < 0) || (y > (surface->h - 1)))
		return 0;

	int bpp = surface->format->BytesPerPixel;
	Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

	switch(bpp) {
	case 1:
		return *p;

	case 2:
		return *(Uint16 *)p;

	case 3:
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
				return p[0] << 16 | p[1] << 8 | p[2];
#else
				return p[0] | p[1] << 8 | p[2] << 16;
#endif

	case 4:
		return *(Uint32 *)p;

	default:
		return 0;       /* shouldn't happen, but avoids warnings */
	}
}

void Graphics::drawLine(float startX, float startY, float endX, float endY, int color, SDL_Surface *dest)
{
	lock(screen);
	
	float dx, dy;
	
	Math::calculateSlope(startX, startY, endX, endY, &dx, &dy);

	while (true)
	{
		putPixel((int)startX, (int)startY, color, dest);

		if ((int)startX == (int)endX)
			break;

		startX -= dx;
		startY -= dy;
	}

	unlock(screen);
}

void Graphics::blit(SDL_Surface *image, int x, int y, SDL_Surface *dest, bool centered)
{
	if (!image)
	{
		showErrorAndExit(_("graphics::blit() - NULL pointer"), SDL_GetError());
	}

	if ((x < -image->w) || (x > width + image->w))
		return;

	if ((y < -image->h) || (y > height + image->h))
		return;

	// Set up a rectangle to draw to
	gRect.x = x;
	gRect.y = y;
	if (centered)
	{
		gRect.x -= (image->w / 2);
		gRect.y -= (image->h / 2);
	}

	gRect.w = image->w;
	gRect.h = image->h;

	/* Blit onto the screen surface */
	if (SDL_BlitSurface(image, NULL, dest, &gRect) < 0)
		showErrorAndExit("graphics::blit() - %s", SDL_GetError());
}

void Graphics::drawBackground()
{
	SDL_FillRect(screen, NULL, black);
	if (background != NULL)
	{
		int xb, yb;
		xb = (width/2)-(640/2);
		yb = (height/2)-(480/2);

		if (background_tile)
		{
			while (xb > 0) xb -= 640;
			while (yb > 0) yb -= 480;

			for (int x = xb; x < width; x += 640)
				for (int y = yb; y < height; y += 480)
					blit(background, x, y, screen, false);
		}
		else
		{
			int x = xb - (background->w - 640)/2;
			int y = yb - (background->h - 480)/2;
			blit(background, x, y, screen, false);
		}
	}
}

void Graphics::drawBackground(SDL_Rect *r)
{
	if (r->x < 0) r->x = 0;
	if (r->y < 0) r->y = 0;
	if (r->x + r->w > 639) r->w = 640 - r->x;
	if (r->y + r->h > 639) r->h = 480 - r->y;

	if (SDL_BlitSurface(background, r, screen, r) < 0)
		showErrorAndExit("graphics::blit() - %s", SDL_GetError());
}

void Graphics::drawRect(int x, int y, int w, int h, int color, SDL_Surface *dest)
{
	gRect.x = x;
	gRect.y = y;
	gRect.w = w;
	gRect.h = h;

	SDL_FillRect(dest, &gRect, color);
}

void Graphics::drawRect(int x, int y, int w, int h, int color, int borderColor, SDL_Surface *dest)
{
	drawRect(x - 1, y - 1, w + 2, h + 2, borderColor, dest);
	drawRect(x, y, w, h, color, dest);
}

void Graphics::setFontColor(int red, int green, int blue, int red2, int green2, int blue2)
{
	fontForeground.r = red;
	fontForeground.g = green;
	fontForeground.b = blue;

	fontBackground.r = red2;
	fontBackground.g = green2;
	fontBackground.b = blue2;

	fontForeground.unused = fontBackground.unused = 0;
}

void Graphics::setFontSize(int size)
{
	fontSize = size;
	Math::limitInt(&fontSize, 0, 4);
}

SDL_Surface *Graphics::getString(const char *in, bool transparent)
{
	SDL_Surface *text = TTF_RenderUTF8_Blended(font[fontSize], in, fontForeground); //, fontBackground);

	if (!text)
	{
		text = TTF_RenderUTF8_Blended(font[fontSize], "FONT_ERROR", fontForeground); //, fontBackground);
	}

	/*if (transparent)
	  setTransparent(text);*/

	return text;
}

int Graphics::GetStringWidth(const std::string& text)
{
	int width, height;

	if (TTF_SizeUTF8(font[fontSize], text.c_str(), &width, &height))
		throw Exception::RuntimeError("Failed to get the width of the string: '" + text + "'", AT);

	return width;
}

void Graphics::drawString(const char *in, int x, int y, int alignment, SDL_Surface *dest)
{
	bool center = false;

	SDL_Surface *text = TTF_RenderUTF8_Shaded(font[fontSize], in, fontForeground, fontBackground);

	if (!text)
	{
		text = TTF_RenderUTF8_Shaded(font[fontSize], "FONT_ERROR", fontForeground, fontBackground);
	}

	if (alignment == TXT_RIGHT) x -= text->w;
	if (alignment == TXT_CENTERED) center = true;

	setTransparent(text);
	blit(text, x, y, dest, center);
	SDL_FreeSurface(text);
}

void Graphics::clearChatString()
{
	strcpy(chatString, "");
}

void Graphics::createChatString(char *in)
{
	sprintf(chatString, "%s %s", chatString, in);
}

void Graphics::drawChatString(SDL_Surface *surface, int y)
{
	char *word = strtok(chatString, " ");
	char wordWithSpace[100];
	
	int r, g, b;

	int x = 10;
	int surfaceWidth = surface->w - 10;

	SDL_Surface *wordSurface;

	while (word)
	{
		if (strcmp(word, "<RGB>") == 0)
		{
			r = atoi(strtok(NULL, " "));
			g = atoi(strtok(NULL, " "));
			b = atoi(strtok(NULL, " "));

			if ((!r) && (!g) && (!b))
			{
				error << "Parse Error in Text Color (" << r << ":" << g << ":" << b << ")!!" << std::endl;
			}

			setFontColor(r, g, b, 0, 0, 0);

			word = strtok(NULL, " ");

			continue;
		}

		sprintf(wordWithSpace, "%s ", word);

		wordSurface = getString(wordWithSpace, false);

		if (x + wordSurface->w > surfaceWidth)
		{
			y += wordSurface->h ;
			x = 10;
		}

		blit(wordSurface, x, y, surface, false);

		SDL_FreeSurface(wordSurface);

		x += wordSurface->w;

		word = strtok(NULL, " ");
	}
}

void Graphics::drawWidgetRect(int x, int y, int w, int h)
{
	drawRect(x - 5, y - 4, w + 10, h + 8, white, screen);
	drawRect(x - 4, y - 3, w + 8, h + 6, black, screen);
	drawRect(x - 3, y - 2, w + 6, h + 4, green, screen);
}

SDL_Surface *Graphics::createSurface(int width, int height)
{
	SDL_Surface *surface, *newImage;
	Uint32 rmask, gmask, bmask, amask;

	/* SDL interprets each pixel as a 32-bit number, so our masks must depend
	on the endianness (byte order) of the machine */
	#if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
		rmask = 0xff000000;
		gmask = 0x00ff0000;
		bmask = 0x0000ff00;
		amask = 0x000000ff;
	#else
		rmask = 0x000000ff;
		gmask = 0x0000ff00;
		bmask = 0x00ff0000;
		amask = 0xff000000;
	#endif

	surface = SDL_CreateRGBSurface(SDL_SWSURFACE, width, height, 32, rmask, gmask, bmask, amask);

	if (surface == NULL)
		showErrorAndExit("CreateRGBSurface failed: %s\n", SDL_GetError());

	newImage = SDL_DisplayFormat(surface);

	SDL_FreeSurface(surface);

	return newImage;
}

SDL_Surface *Graphics::alphaRect(int width, int height, Uint8 red, Uint8 green, Uint8 blue)
{
	SDL_Surface *surface = createSurface(width, height);

	SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, red, green, blue));

	SDL_SetAlpha(surface, SDL_SRCALPHA|SDL_RLEACCEL, 128);

	return surface;
}

void Graphics::colorize(SDL_Surface *image, int red, int green, int blue)
{
	SDL_Surface *alpha = alphaRect(image->w, image->h, red, green, blue);

	blit(alpha, 0, 0, image, false);

	SDL_SetColorKey(image, (SDL_SRCCOLORKEY|SDL_RLEACCEL), SDL_MapRGB(image->format, red / 2, green / 2, blue / 2));
}

void Graphics::lock(SDL_Surface *surface)
{
	/* Lock the screen for direct access to the pixels */
	if (SDL_MUSTLOCK(surface))
	{
		if (SDL_LockSurface(surface) < 0 )
		{
			showErrorAndExit("Could not lock surface", "");
		}
	}
}

void Graphics::unlock(SDL_Surface *surface)
{
	if (SDL_MUSTLOCK(surface))
	{
		SDL_UnlockSurface(surface);
	}
}

void Graphics::resetLoading()
{
	currentLoading = 0;
}

void Graphics::showLoading(int amount, int max)
{
	max *= 4;

	if (max > 398)
		max = 398;

	Math::limitInt(&(currentLoading += amount), 0, max);

	float greenload = (((float) currentLoading/398) * 2 * 255);
	float redload = (((float) currentLoading/398) * -2 +2) *255;

	drawRect(width/2 - 200, height - 60, 400, 10, black, white, screen);
	drawRect(width/2 - 199, height - 59, currentLoading, 8, SDL_MapRGB(screen->format, std::min((int)redload, 255), std::min((int)greenload, 255), 0), screen);

	SDL_UpdateRect(screen, width/2 - 200, height - 60, 400, 10);
}

void Graphics::showLicenseErrorAndExit()
{
	//setFontSize(3); setFontColor(0xff, 0x00, 0x00, 0x00, 0x00, 0x00);
	FontEngine::DrawLine("default", _("License Agreement Missing"), font_manager.GetOldSize(3), width / 2, height / 2 - 190, "center_base", screen);
	
	Video::PointSurface text = FontEngine::Renderer::RenderText(font_manager.GetFont("default"),
		_("The GNU General Public License was not found.\n"
		  "It could either not be properly loaded or has been removed.\n"
		  "DirtY BLoB WaRs will not run with the license missing."), font_manager.GetOldSize(1), 640, -1, FontEngine::Center);
	Video::Surface(screen).BlitInto(Video::Point(width / 2, height / 2 - 60), text, text["base"]);
	
	text = FontEngine::Renderer::RenderText(font_manager.GetFont("default"),
		_("DirtY BLoB WaRs now exit\n"
		  "Press escape to quit."), font_manager.GetOldSize(1), 640, -1, FontEngine::Center);
	Video::Surface(screen).BlitInto(Video::Point(width / 2, height / 2 + 190), text, text["base"]);
	//blit(screen, 100, 200, text.GetSDLSurface(), false);

	engine->flushInput();

	while (true)
	{
		updateScreen();
		engine->getInput();
		if (engine->keyState[SDLK_ESCAPE])
			exit(1);
		SDL_Delay(16);
	}
}

void Graphics::showErrorAndExit(const char *error_str, const char *param)
{
	SDL_FillRect(screen, NULL, black);

	char message[256];
	sprintf(message, error_str, param);

	Video::PointSurface text = FontEngine::Renderer::RenderLine(font_manager.GetFont("default"), _("An unforseen error has occurred"), font_manager.GetOldSize(3), 640);
	text.ColorizeGrayscale(255, 0, 0);
	Video::Surface(screen).BlitInto(Video::Point(width / 2, height / 2 - 190), text, text["center_base"]);

	FontEngine::DrawLine("default", message, font_manager.GetOldSize(1), width / 2, height / 2 - 150, "center_base", screen);

	FontEngine::DrawLine("default", _("You may wish to try the following,"), font_manager.GetOldSize(1), width / 2 - 245, height / 2 - 90, "begin_base", screen);

	text = FontEngine::Renderer::RenderText(font_manager.GetFont("default"),
		_("1) Try reinstalling the game.\n"
		  "2) Ensure you have SDL 1.2.5 or greater installed.\n"
		  "3) Ensure you have the latest versions of additional required SDL libraries."), font_manager.GetOldSize(0), 565);
	Video::Surface(screen).BlitInto(Video::Point(width / 2 - 245, height / 2 - 50), text, text["base"]);

	//drawString("4) Install using an RPM if you originally built the game from source", width/2 - 245, height/2 + 10, false, screen);
	//drawString("or try building from source if you installed using an RPM.", width/2 - 245, height/2 + 30, false, screen);
	//drawString("5) Visit http://www.parallelrealities.co.uk/blobWars.php and check for updates.", width/2 - 245, height/2 + 50, false, screen);

	FontEngine::DrawLine("default", _("If problems persist, recompile the game with debugging support and make a backtrace."),
						 font_manager.GetOldSize(1), width / 2, height / 2 + 120, "center_base", screen);
	//drawString("If problems persist contact ??. Please be aware however that we will not", width/2, width/2 + 120, true, screen);
	//drawString("be able to assist in cases where the code or data has been modified.", width/2, width/2 + 140, true, screen);

	text = FontEngine::Renderer::RenderText(font_manager.GetFont("default"),
		_("DirtY BLoB WaRs now exit\n"
		  "Press escape to quit."), font_manager.GetOldSize(1), 640, -1, FontEngine::Center);
	Video::Surface(screen).BlitInto(Video::Point(width / 2, height / 2 + 190), text, text["base"]);

	engine->flushInput();

	while (true)
	{
		updateScreen();
		engine->getInput();
		if (engine->keyState[SDLK_ESCAPE])
		{
			exit(1);
		}

	SDL_Delay(16);
	}
}

void Graphics::resize()
{
	fadeBlack = alphaRect(width, height, 0x00, 0x00, 0x00);
	infoBar   = alphaRect(width, 25,     0x00, 0x00, 0x00);
	resized   = true;
}

bool Graphics::IfResized()
{
	if (resized)
	{
		resized = false;
		return true;
	}
	return false;
}
