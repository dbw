/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "CAudio.h"
#include "CData.h"
#include "CGame.h"
#include "CGameData.h"
#include "CGraphics.h"
#include "CMap.h"
#include "CPersistant.h"
#include "CPersistData.h"
#include "gettext.hpp"
#include "loadSave.h"
#include "physfs/manager.hpp"
#include "physfs/stream.hpp"

void initSaveSlots()
{
	int mod_time, new_time;
	mod_time = new_time = 0;

	Game tempGame;

	engine.continueSaveSlot = 0;

	//READ SAVE GAME DATA
	for (int i = 0 ; i < 5 ; i++)
	{
		std::stringbuf sb;
		std::ostream os(&sb);
		os << "save" << i << ".dat";

		if (!PhysFS::Manager::FileExists(sb.str()))
		{
			sprintf(engine.saveSlot[i], "%.2d - %s", i + 1, _("Empty").c_str());
		}
		else
		{
			if (PhysFS::Manager::ReadFileIntoPointer(sb.str(), (char *) &tempGame, sizeof(Game)) != sizeof(Game))
			{
				sprintf(engine.saveSlot[i], "%.2d - %s", i + 1, _("Corrupt Save Data").c_str());
			}
			else
			{
				sprintf(engine.saveSlot[i], "%.2d - %s (%.2d:%.2d:%.2d)", (i + 1), _(tempGame.stageName).c_str(), tempGame.totalHours, tempGame.totalMinutes, tempGame.totalSeconds);
			}

			if ((new_time = PHYSFS_getLastModTime(sb.str().c_str())) > mod_time)
			{
				mod_time = new_time;
				engine.continueSaveSlot = (i + 1);
			}
		}
	}
	
	debug("pers", 3) << "Continue Save Slot = " << engine.continueSaveSlot << std::endl;
}

bool loadGame(int slot)
{
	debug("pers", 1) << "Loading Game #" << slot << ".." << std::endl;;
	game.clear();

	char string[2][1024];
	int param[2];
	
	Data *data = NULL;

	std::stringbuf * sb = new std::stringbuf;
	std::ostream os(sb);
	os << "save" << slot << ".dat";

	
	if (!PhysFS::Manager::FileExists(sb->str()))
	{
		delete sb;
		return false;
	}

	int num_read;
	if (sizeof(Game) != (num_read = PhysFS::Manager::ReadFileIntoPointer(sb->str(), (char *) &game, sizeof(Game))))
	{
		delete sb;
		error << "Load error: size mismatch.. read: " << num_read << " excepted: " << sizeof(Game) << std::endl;
		graphics.showErrorAndExit(_("The save data loaded was not in the format expected"), "");
	}
	
	sb->str("");
	os << "persistant" << slot << ".dat";

	if (!PhysFS::Manager::FileExists(sb->str()))
	{
		return false;
	}

	PhysFS::IStream is(sb->str());

	while (is.good())
	{
		is.ignore(256, '"');
		is.get(string[0], 256, '"');
		is.ignore(1); is.ignore(256, '"');
		is.get(string[1], 256, '"');
		is.ignore(1);
		is >> param[0];
		is >> param[1];
		
		data = new Data();
		
		data->set(string[0], string[1], param[0], param[1]);
		
		debug("pers", 5) << "Read '" << data->key << "' = '" << data->value << "', " << data->current << " -> " << data->target << std::endl;
		
		if ((data->current == -1) && (data->target == -1))
		{
			break;
		}

		gameData.addCompletedObjective(data);
		
	}

	if (!data || ((data->current != -1) && (data->target != -1)))
	{
		delete data;
		delete sb;
		error << "Persistant file end not found" << std::endl;
		graphics.showErrorAndExit(_("Invalid persistant file"), "");
	}

	char stageName[50];
	int numberOfLines = 0;
	
	Persistant *persistant;
	PersistData *persistData;

	is.ignore(512, '\n');

	while (is.good())
	{
		is.get(stageName, 512);
		
		if (strcmp(stageName, "@EOF@") == 0)
		{
			break;
		}
		
		is >> numberOfLines;
		is.ignore(512, '\n');
		
		debug("pers", 5) << "Read '" << stageName << "' with " << numberOfLines << " lines." << std::endl;
		
		persistant = map.createPersistant(stageName);
		
		for (int i = 0 ; i < numberOfLines ; i++)
		{
			persistData = new PersistData();
			
			is.getline(persistData->data, 150);
			
			debug("pers", 5) << "Read line "<< i << ": '" << persistData->data << "'" << std::endl;
			
			persistant->addLine(persistData->data);

			//is.ignore(512, '\n');
			
		}
	}

	debug("pers", 1) << "Loaded Successfully." << std::endl;

	return true;
}

int confirmSave()
{
	if (game.autoSave)
	{
		return game.autoSaveSlot;
	}
	
	if (!engine.loadWidgets("data/saveWidgets"))
		graphics.showErrorAndExit(ERR_FILE, "data/saveWidgets");
	
	int slot[6], quitYes, quitNo;
	slot[0] = slot[1] = slot[2] = slot[3] = slot[4] = slot[5] = 0;
	quitYes = quitNo = 0;
	
	engine.setWidgetVariable("slot1", &slot[0]);
	engine.setWidgetVariable("slot2", &slot[1]);
	engine.setWidgetVariable("slot3", &slot[2]);
	engine.setWidgetVariable("slot4", &slot[3]);
	engine.setWidgetVariable("slot5", &slot[4]);
	engine.setWidgetVariable("slot6", &slot[5]);
	
	engine.setWidgetVariable("contyes", &quitYes);
	engine.setWidgetVariable("contno", &quitNo);
	
	char widgetName[10];
	strcpy(widgetName, "");
	
	for (int i = 0 ; i < 5 ; i++)
	{
		sprintf(widgetName, "slot%d", i + 1);
		strcpy(engine.getWidgetByName(widgetName)->label, engine.saveSlot[i]);
	}
	
	engine.highlightWidget("slot1");
	
	int menuSound = 0;
	
	int rtn = -1;
	
	engine.showWidgetGroup("gameSlots", true);
	engine.showWidgetGroup("continueconf", false);
	
	graphics.setFontSize(4);
	SDL_Surface *title = graphics.quickSprite("savetitle", graphics.getString(_("Save Game"), true));
	
	while (true)
	{
		graphics.updateScreen();
		SDL_FillRect(graphics.screen, NULL, graphics.black);
		engine.getInput();

		menuSound = engine.processWidgets();

		if (menuSound)
			audio.playMenuSound(menuSound);
		
		graphics.blit(title, bx + 320, by + 100, graphics.screen, true);
		
		drawWidgets();
		
		if (slot[5])
		{
			engine.showWidgetGroup("gameSlots", false);
			engine.showWidgetGroup("continueconf", true);
			engine.highlightWidget("contno");
			drawWidgets();
			slot[5] = 0;
		}
		
		if (quitYes)
		{
			break;
		}
		
		if (quitNo)
		{
			engine.showWidgetGroup("gameSlots", true);
			engine.showWidgetGroup("continueconf", false);
			engine.highlightWidget("slot1");
			drawWidgets();
			quitNo = 0;
		}
		
		for (int i = 0 ; i < 5 ; i++)
		{
			if (slot[i])
			{
				rtn = i;
			}
		}
		
		if ((slot[0]) || (slot[1]) || (slot[2]) || (slot[3]) || (slot[4]) || (slot[5]))
		{
			break;
		}

		SDL_Delay(16);
	}
	
	SDL_FillRect(graphics.screen, NULL, graphics.black);
	graphics.updateScreen();
	
	return rtn;
}

void saveGame()
{
	char message[100];

	SDL_FillRect(graphics.screen, NULL, graphics.black);
	graphics.updateScreen();
	
	int slot = confirmSave();
	
	if (slot == -1)
		return;

	graphics.setFontSize(1);
	sprintf(message, _("Saving Game to Save Slot #%d. Please Wait...").c_str(), slot + 1);
	graphics.drawString(message, bx+320, by+220, true, graphics.screen);
	graphics.updateScreen();

	std::stringbuf  sb;
	std::ostream os(&sb);
	os << "save" << slot << ".dat";
	
	PhysFS::Manager::WriteFile(sb.str(), (char *) &game, sizeof(Game));

	sb.str("");
	os << "persistant" << slot << ".dat";

	PhysFS::OStream os2(sb.str());
	
	createPersistantMapData();
	
	Data *data = (Data*)gameData.dataList.getHead();

	while (data->next != NULL)
	{
		data = (Data*)data->next;
		
		os2 << '"' << data->key << "\" \"" << data->value << "\" " << data->current << " " << data->target << std::endl;
	}
	
	os2 << "\"@EOF@\" \"@EOF\" -1 -1" << std::endl;
	
	Persistant *persistant = (Persistant*)map.persistantList.getHead();
	PersistData *persistData;
	
	while (persistant->next != NULL)
	{
		persistant = (Persistant*)persistant->next;
		
		if (strcmp(persistant->stageName, "@none@") == 0)
		{
			continue;
		}
		
		os2 << persistant->stageName << std::endl;
		os2 << persistant->numberOfLines << std::endl;
	
		persistData = (PersistData*)persistant->dataList.getHead();
		
		while (persistData->next != NULL)
		{
			persistData = (PersistData*)persistData->next;
			
			os2 << persistData->data;
			if (persistData->data[strlen(persistData->data) - 1] != '\n')
			{
				debug("pers", 3) << "No newline at the end of persistant data, fixing.." << std::endl;
				os2 << std::endl;
			}
		}
	}
	
	os2 << "@EOF@" << std::endl;

	map.clear();
	
	graphics.drawString(_("Save Complete"), bx+320, by+260, true, graphics.screen);
	graphics.updateScreen();
}
