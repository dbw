/*
Copyright (C) 2007 K�v�g� Zolt�n

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef GETTEXT_HPP
#define GETTEXT_HPP

#include <string>
#include "tinygettext/tinygettext.hpp"
#include <iostream>
extern TinyGetText::DictionaryManager dictionary_manager;

static inline std::string _(const std::string& string)
{
	//std::cout << "translate: '" << string << "' -> '" << dictionary_manager.get_dictionary().translate(string) << "'" << std::endl;
	return dictionary_manager.get_dictionary().translate(string);
}

static inline std::string _(const std::string& singular, const std::string& plural, const int& num)
{
	return dictionary_manager.get_dictionary().translate(singular, plural, num);
}

#endif
