/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "CAudio.h"
#include "CEngine.h"
#include "CGame.h"
#include "CGameData.h"
#include "CGraphics.h"
#include "CMap.h"
#include "config.h"
#include "defs.h"
#include "main.h"

// for sucking SDL implementation on windows
// -- no, we don't need SDL_main!!!
#undef main

// Global variables
Audio audio;
Engine engine;
Game game;
GameData gameData;
Graphics graphics;
Map map;

Entity defEnemy[MAX_ENEMIES];
Entity defItem[MAX_ITEMS];
Entity player;
Weapon weapon[MAX_WEAPONS];


void __attribute__((__noreturn__)) showHelp()
{
	printf("\n");
	printf("DirtY BLoB WaRs (Version %s)\n", VERSION);
	printf("Copyright (C) 2004 Parallel Realities\n");
	printf("Copyright (C) 2007-2008 Kovago Zoltan and contributors\n");
	printf("Licensed under the GNU General Public License (GPL)\n\n");

//	printf("The DirtY BLoB WaRs gameplay manual can be found in,\n");
//	printf("\t%s\n\n", GAMEPLAYMANUAL);

	printf("Additional Commands\n");
	printf("\t--fullscreen        Start the game in Full Screen mode\n");
	printf("\t--mono              Use mono sound output instead of stereo\n");
	printf("\t--noaudio           Disables audio\n");
	printf("\t--version           Display version number\n");
	printf("\t--help              This help\n");
#ifdef DEBUG
	printf("\t-d|--debug [global_lvl,][name=lvl][,...] Set debug level\n");
	printf("\t--show-func         Show functions in debug messages\n");
#endif //DEBUG
	printf("\t--map path          Load map at path (PhysFS path!)\n");
	printf("\t--skill [0-3]       Set skill\n");
	printf("\t--showsprites       Show loaded sprites\n");
	printf("\t--hub               Load hub immediately\n");
	printf("\t--randomscreens     Take screenshots randomly\n");
	printf("\t--nomonsters        Remove enemies from levels\n");
	printf("\t--credits           Show credits\n");

	exit(0);
}

void __attribute__((__noreturn__)) showVersion()
{
	printf("DirtY BLoB WaRs version: %s, branch: %s\n", VERSION, BRANCH);

	// showVersion is the command name, and not showVersionAndCopyright !!!
/*	printf("Copyright (C) 2004 Parallel Realities\nCopyright (C) 2007 Kovago Zoltan\n");
	printf("Licensed under the GNU General Public License (GPL)\n\n");*/
	exit(0);
}

int main(int argc, char *argv[])
{
	// Don't show the stupid intro in debug mode..
#ifdef DEBUG
	int requiredSection = SECTION_TITLE;
#else
	int requiredSection = SECTION_INTRO;
#endif
	bool showSprites = false;
	bool hub = false;

	for (int i = 1 ; i < argc ; i++)
	{
		if (strcmp(argv[i], "--fullscreen") == 0) engine.fullScreen = true;
		else if (strcmp(argv[i], "--noaudio") == 0) engine.useAudio = 0;
		else if (strcmp(argv[i], "--mono") == 0) engine.useAudio = 1;
		else if (strcmp(argv[i], "--version") == 0) showVersion();
		else if (strcmp(argv[i], "--help") == 0) showHelp();

		// DEBUG
#ifdef DEBUG
		else if (strcmp(argv[i], "--show-func") == 0) DebugStream::show_func = true;
		else if ((strcmp(argv[i], "-d") == 0) || (strcmp(argv[i], "--debug") == 0)) DebugLevelManager::ParseOpts(argv[++i]);
#endif
		else if (strcmp(argv[i], "--map") == 0) {game.setMapName(argv[++i]); requiredSection = SECTION_GAME;}
		else if (strcmp(argv[i], "--skill") == 0) game.skill = atoi(argv[++i]);
		else if (strcmp(argv[i], "--showsprites") == 0) showSprites = true;
		else if (strcmp(argv[i], "--hub") == 0) hub = true;
		else if (strcmp(argv[i], "--randomscreens") == 0) graphics.takeRandomScreenShots = true;
		/// @todo Move devNoMonsters to cheats..
		else if (strcmp(argv[i], "--nomonsters") == 0) engine.devNoMonsters = true;
		/// @todo Add a credits options to menu
		else if (strcmp(argv[i], "--credits") == 0) requiredSection = SECTION_CREDITS;
		else warning << "Unknown parameter '" << argv[i] << "', ignored.." << std::endl;
	}

	#if DEMO
		game.setMapName("data/floodedTunnel1");
		requiredSection = SECTION_GAME;
		game.skill = 1;
	#endif

	atexit(cleanup);

	initSystem(argv[0]);

	player.setName("Player");

	engine.flushInput();
	
	engine.allowQuit = true;

	if (hub)
	{
		requiredSection = SECTION_HUB;
		loadGame(0);
		loadResources();
	}
	
	if (game.skill == 3)
	{
		game.hasAquaLung = game.hasJetPack = true;
	}

	if (showSprites)
	{
		showAllSprites();
	}

	while (true)
	{
		switch (requiredSection)
		{
			case SECTION_INTRO:
				requiredSection = doIntro();
				break;

			case SECTION_TITLE:
				requiredSection = title();
				break;

			case SECTION_HUB:
				requiredSection = doHub();
				break;

			case SECTION_GAME:
				if (!game.continueFromCheckPoint)
				{
					#if DEMO
						showDemoStart();
					#endif
					checkStartCutscene();
					loadResources();
				}
				requiredSection = doGame();
				break;

			case SECTION_GAMEOVER:
				requiredSection = gameover();
				break;
				
			case SECTION_EASYOVER:
				map.clear();
				game.clear();
				easyGameFinished();
				requiredSection = SECTION_TITLE;
				break;
				
			case SECTION_CREDITS:
				doCredits();
				requiredSection = SECTION_TITLE;
				break;
		}
	}

	return 0;
}
