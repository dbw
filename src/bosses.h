/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef BOSSES_H
#define BOSSES_H

#include "CBoss.h"
#include "CEntity.h"
#include "CWeapon.h"

extern void addItem(int itemType, const char *name, int x, int y, const char *spriteName, int health, int value, int flags, bool randomMovement);
extern void addTeleportParticles(float x, float y, int amount, int soundToPlay);
extern void addColorParticles(float x, float y, int amount, int color);
extern bool okayToSpawnEnemy(char *name, int x, int y);
extern void addEnemy(const char *name, int x, int y, int flags);
extern void moveEntity(Entity *entity);
extern void addFireTrailParticle(float x, float y);
extern void checkObjectives(const char *name, bool alwaysInform);

extern Entity *getEnemy(char *name);
extern Weapon *getRandomAimedWeapon();
extern Weapon *getRandomStraightWeapon();
extern Weapon *getRandomGaldovWeapon();

extern void tankBossMGInit();
extern void tankBossGLInit();
extern void aquaBossInit();
extern void droidBossInit();
extern void galdovInit();
extern void galdovFinalInit();

// this is used exclusively by the bosses
Boss *self;

#endif //BOSSES_H
