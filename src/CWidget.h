/*
Copyright (C) 2004 Parallel Realities
Copyright (C) 2007 Kővágó Zoltán

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef CWIDGET_H
#define CWIDGET_H

#include "CGameObject.h"
#include <SDL/SDL.h>

class Widget : public GameObject {

	public:

		char name[50], groupName[50], label[80], options[100];

		int type;

		int x, y;

		int *value; // NB - THIS IS A POINTER!!

		int min, max;

		bool enabled, visible, changed;

		SDL_Surface *image;

		Widget *previous;

	Widget();
	void setProperties(char *name, char *groupName, char *label, char *options, int x, int y, int min, int max);
	void setValue(int *value);
	void redraw();

};

#endif //CWIDGET_H
