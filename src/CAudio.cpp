/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <stdexcept>

#include "CAudio.h"
#include "console/console.hpp"
#include "physfs/rwops.hpp"

Audio::Audio()
{
	output = 2;
	useSound = true;
	useMusic = true;

	for (int i = 0 ; i < MAX_SOUNDS ; i++)
	{
		sound[i] = NULL;
	}

	music = NULL;
	quickSound = NULL;
	
	strcpy(levelMusicName, "");
}

void Audio::setSoundVolume(int soundVolume)
{
	this->soundVolume = soundVolume;
	if (engine->useAudio)
		Mix_Volume(-1, soundVolume);
}

void Audio::setMusicVolume(int musicVolume)
{
	this->musicVolume = musicVolume;
	if (engine->useAudio)
	{
		Mix_VolumeMusic(musicVolume);
	}
}

void Audio::registerEngine(Engine *engine)
{
	this->engine = engine;
}

/**
 * Loads a sound file
 * @param i: the id to load the sound to
 * @param filename: the file to load (relative to datadir)
 */

bool Audio::loadSound(int i, const char *filename)
{
	if (!engine->useAudio)
	{
		return true;
	}
		
	if (i >= MAX_SOUNDS)
	{
		error << "Sound index is higher than maximum allowed (requested " << i << ", maximum is " << MAX_SOUNDS << ")" << std::endl
			  << "Ignoring, but this is an internal error, so report it!" << std::endl;
		return false;
	}

	if (sound[i] != NULL)
	{
		Mix_FreeChunk(sound[i]);
		sound[i] = NULL;
	}

	try
	{
		SDL_RWops * buffer = PhysFS::RWops(filename);
		sound[i] = Mix_LoadWAV_RW(buffer, 1);

		if (!sound[i])
		{
			Exception::SdlMixerError e(AT);
			e.AddInfo("Mix_GetError()", Mix_GetError());
			e.AddInfo("File tried to load", filename);
			throw e;
		}
	}
	catch (const Exception::Base& e)
	{
		error << e.GetDesc() << std::endl;
		return false;
	}
	catch (const std::exception& e)
	{
		error << "Failed to load '" << filename << "'" << std::endl << "The error was: " << e.what() << std::endl;
		return false;
	}
	catch (...)
	{
		error << "Failed to load '" << filename << "'" << std::endl << "Unknown error while loading." << std::endl;
		return false;
	}
	
	return true;
}

bool Audio::loadMusic(const char *filename, const bool save)
{
	if (!engine->useAudio)
	{
		return true;
	}

	if (music != NULL)
	{
		Mix_HaltMusic();
		Mix_FreeMusic(music);
		music = NULL;
		SDL_FreeRW(music_rwops);
	}


	try
	{
		music_rwops = PhysFS::RWops(filename);
		music = Mix_LoadMUS_RW(music_rwops);

		if (!music)
		{
			Exception::SdlMixerError e(AT);
			e.AddInfo("Mix_GetError()", Mix_GetError());
			e.AddInfo("File tried to load", filename);
			throw e;
		}
	}
	catch (const Exception::Base& e)
	{
		error << e.GetDesc() << std::endl;
		return false;
	}
	catch (const std::exception& e)
	{
		error << "Failed to load '" << filename << "'" << std::endl << "The error was: " << e.what() << std::endl;
		return false;
	}
	catch (...)
	{
		error << "Failed to load '" << filename << "'" << std::endl << "Unknown error while loading." << std::endl;
		return false;
	}
		
	if (save)
		strcpy(levelMusicName, filename);

	return true;
}

void Audio::playSound(int snd, int channel)
{
	if ((!engine->useAudio) || (soundVolume == 0))
		return;
	
	if (!output)
	{
		return;
	}

	Mix_Volume(channel, soundVolume);

	Mix_PlayChannel(channel, sound[snd], 0);
}

void Audio::playMusic()
{
	if (!engine->useAudio)
		return;
	
	if (!output)
	{
		return;
	}

	Mix_PlayMusic(music, -1);

	Mix_VolumeMusic(musicVolume);
}

void Audio::playMusicOnce()
{
	if (!engine->useAudio)
		return;
	
	if (!output)
	{
		return;
	}

	Mix_PlayMusic(music, 0);

	Mix_VolumeMusic(musicVolume);
}

bool Audio::loadGameOverMusic()
{
	#if DEMO
		return true;
	#endif

		return loadMusic("music/friendDied.mod", false);
}

bool Audio::reloadLevelMusic()
{
	// remove the Game Over music first...

	if (music != NULL)
	{
		Mix_HaltMusic();
		Mix_FreeMusic(music);
		music = NULL;
	}

	return loadMusic(levelMusicName);
}

void Audio::playAmbiance()
{
	if ((!engine->useAudio) || (soundVolume == 0))
		return;
	
	if (!output)
	{
		return;
	}

	Mix_PlayChannel(CH_AMBIANCE, sound[SND_AMBIANCE], -1);
}

void Audio::stopAmbiance()
{
	if ((!engine->useAudio) || (soundVolume == 0))
		return;

	Mix_HaltChannel(CH_AMBIANCE);
}

int Audio::playMenuSound(int sound)
{
	if ((!engine->useAudio) || (soundVolume == 0))
		return sound;

	if ((sound == 0) || (sound == 3))
		return sound;

	if (sound == 1)
		playSound(SND_HIGHLIGHT, CH_ANY);

	if (sound == 2)
		playSound(SND_SELECT, CH_ANY);
		
	return sound;
}

void Audio::pause()
{
	if (!engine->useAudio)
		return;

	for (int i = 0 ; i < 8 ; i++)
		Mix_Pause(i);

	Mix_PauseMusic();
}

void Audio::resume()
{
	if (!engine->useAudio)
		return;
	
	if (!output)
	{
		return;
	}

	for (int i = 0 ; i < 8 ; i++)
		Mix_Resume(i);

	Mix_ResumeMusic();
}

void Audio::stopMusic()
{
	if (!engine->useAudio)
		return;

	Mix_HaltMusic();
}

void Audio::fadeMusic()
{
	if (!engine->useAudio)
		return;

	Mix_FadeOutMusic(3500);
}

void Audio::free()
{
	for (int i = 0 ; i < MAX_SOUNDS - 3 ; i++)
	{
		if (sound[i] != NULL)
		{
			Mix_FreeChunk(sound[i]);
			sound[i] = NULL;
		}
	}

	if (music != NULL)
	{
		Mix_HaltMusic();
		Mix_FreeMusic(music);
	}

	music = NULL;

	if (quickSound != NULL)
		Mix_FreeChunk(quickSound);

	quickSound = NULL;
}

void Audio::destroy()
{
	free();

	for (int i = MAX_SOUNDS - 3 ; i < MAX_SOUNDS ; i++)
	{
		if (sound[i] != NULL)
		{
			Mix_FreeChunk(sound[i]);
			sound[i] = NULL;
		}
	}
}
