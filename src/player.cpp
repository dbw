/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "CAudio.h"
#include "CCollision.h"
#include "CGame.h"
#include "CGraphics.h"
#include "CMath.h"
#include "CMap.h"
#include "console/console.hpp"
#include "gettext.hpp"
#include "player.h"

void resetPlayer()
{
	game.getCheckPoint(&player.x, &player.y);
	
	player.dx = 0;
	player.dy = 0;
	player.immune = 120;
	player.environment = ENV_AIR;
	player.oxygen = 7;
	player.fuel = 7;
	addTeleportParticles(player.x + 10, player.y + 10, 50, SND_TELEPORT2);
	
	Math::removeBit(&player.flags, ENT_FLIES);
	Math::removeBit(&player.flags, ENT_TELEPORTING);
	player.setSprites(graphics.getSprite("BobRight", true), graphics.getSprite("BobLeft", true), graphics.getSprite("BobSpin", true));
	
	map.windPower = 0;
	map.windChangeTime = 90;
}

void gibPlayer()
{
	float x, y, dx, dy;
	int amount = (game.gore) ? 25 : 150;

	for (int i = 0 ; i < amount ; i++)
	{
		x = player.x + Math::rrand(-3, 3);
		y = player.y + Math::rrand(-3, 3);
		
		if (game.gore)
		{
			dx = Math::rrand(-5, 5);
			dy = Math::rrand(-15, -5);
			addEffect(x, y, dx, dy, EFF_BLEEDS);
		}
		else
		{
			dx = Math::rrand(-5, 5);
			dy = Math::rrand(-5, 5);
			addColoredEffect(x, y, dx, dy, graphics.yellow, EFF_COLORED + EFF_WEIGHTLESS);
		}
	}

	(game.gore) ? audio.playSound(SND_SPLAT, CH_ANY) : audio.playSound(SND_POP, CH_ANY);
}

void checkPlayerBulletCollisions(Entity *bullet)
{
	if ((bullet->health < 1) || (player.health <= -60))
	{
		return;
	}

	if ((player.flags & ENT_TELEPORTING) || (game.missionOver > 0))
	{
		return;
	}

	if (bullet->owner != &player)
	{
		if (Collision::collision(&player, bullet))
		{
			if ((!player.immune) && (!(bullet->flags & ENT_EXPLODES)))
			{
				addBlood(&player, bullet->dx / 4, Math::rrand(-6, -3), 1);
				audio.playSound(SND_HIT, CH_ANY);
				if (game.missionOverReason == MIS_INPROGRESS)
				{
					player.health -= bullet->damage;
					player.immune = 120;
				}
			}

			Math::removeBit(&bullet->flags, ENT_SPARKS);
			Math::removeBit(&bullet->flags, ENT_PUFFS);

			if (player.health <= 0)
			{
				player.dx = bullet->dx;
				player.dy = -5;
				audio.playSound(SND_DEATH1 + rand() % 3, CH_DEATH);
				player.health = 0;
			}

			if (bullet->id == WP_LASER)
			{
				debug("misc", 4) << "Laser Hit Player" << std::endl;
				throwAndDamageEntity(&player, 0, -3, 3, -8);
			}
			else
			{
				bullet->health = 0;
			}
		}
	}
}

void doPlayer()
{
	int x = (int) player.x - map.offsetX;
	int y = (int) player.y - map.offsetY;

	if (engine.cheatHealth)
	{
		player.health = MAX_HEALTH;
	}

	if (game.missionOverReason == MIS_PLAYERESCAPE)
	{
		return;
	}

	if (player.y > (map.limitDown + 500))
	{
		if (game.missionOver == 0)
		{
			player.health--;
		}

		if (player.health > 0)
		{
			game.setMissionOver(MIS_PLAYEROUT);
		}
		else
		{
			game.setMissionOver(MIS_PLAYERDEAD);
		}
	}
	
	if (player.flags & ENT_TELEPORTING)
	{
		moveEntity(&player);
		return;
	}

	if (game.missionOverReason == MIS_PLAYEROUT)
	{
		graphics.blit(player.getFaceImage(), x, y, graphics.screen, false);
		moveEntity(&player);
		return;
	}

	if ((player.health < 1) || (player.immune > 120))
	{
		if (player.health <= -60)
		{
			return;
		}

		player.think();
		
		if (game.hasAquaLung)
		{
			player.oxygen = 7;
		}

		moveEntity(&player);

		graphics.blit(player.getFaceImage(), x, y, graphics.screen, false);

		if (player.health < 1)
		{
			player.health--;
		}

		if (player.health <= -60)
		{
			gibPlayer();
		}

		//return;
	}

	if ((!(player.flags & ENT_FLIES)) && (!map.isIceLevel))
	{
		player.dx = 0;
	}
	
	// toggles the Jetpack if available
	if (engine.isKey(KEY_JETPACK))
	{
		if ((game.hasJetPack) || (engine.cheatExtras))
		{
			if (player.flags & ENT_FLIES)
			{
				Math::removeBit(&player.flags, ENT_FLIES);
				player.setSprites(graphics.getSprite("BobRight", true), graphics.getSprite("BobLeft", true), graphics.getSprite("BobSpin", true));
			}
			else
			{
				if ((player.fuel > 3) && (player.environment == ENV_AIR))
				{
					Math::addBit(&player.flags, ENT_FLIES);
					player.setSprites(graphics.getSprite("JPBobRight", true), graphics.getSprite("JPBobLeft", true), graphics.getSprite("BobSpin", true));
				}
				else if ((player.environment == ENV_WATER))
				{
					engine.setInfoMessage(_("Jetpack cannot be used underwater"), 0, INFO_NORMAL);
				}
				else
				{
					engine.setInfoMessage(_("Jetpack is recharging..."), 0, INFO_NORMAL);
				}
			}
		}
		else
		{
			engine.setInfoMessage(_("Don't have jetpack!"), 0, INFO_NORMAL);
		}
		
		engine.clearKey(KEY_JETPACK);
	}
	
	if (map.isBlizzardLevel)
	{
		if ((!engine.isKey(KEY_LEFT)) && (!engine.isKey(KEY_RIGHT)))
		{
			if ((player.dx > -2) && (player.dx < 2))
			{
				player.dx += map.windPower * 0.1;
			}
		}
	}

	signed char dir = 0;
	if (engine.isKey(KEY_LEFT) || engine.getJoyLeft())
		dir = -1;
	else if (engine.isKey(KEY_RIGHT) || engine.getJoyRight())
		dir = 1;
	if (dir != 0)
	{
		player.face = dir < 0 ? 1 : 0;

		if (player.flags & ENT_FLIES || map.isIceLevel)
			player.dx += (engine.cheatSpeed ? 0.3f : 0.1f) * dir;
		else
			player.dx = (engine.cheatSpeed ? 3 * dir : dir) * PLAYER_WALK_SPEED;

		if (player.flags & ENT_FLIES)
			Math::limitFloat(&player.dx, (engine.cheatSpeed ? -3 : -1) * PLAYER_FLY_SPEED, (engine.cheatSpeed ? 3 : 1) * PLAYER_FLY_SPEED);
		else if (map.isIceLevel)
			Math::limitFloat(&player.dx, (engine.cheatSpeed ? -3.75f : -1.25f) *  PLAYER_WALK_SPEED, (engine.cheatSpeed ? 3.75f : 1.25f) * PLAYER_WALK_SPEED);

		if (!(player.flags & ENT_FLIES))
			player.animate();

	}
	else if (!(player.flags & ENT_FLIES))
	{
		// usual idle frame
		player.currentFrame = 0;
	}

	Math::limitFloat(&player.x, 10, map.limitRight + 608);
	Math::limitFloat(&player.y, map.limitUp + 5, (MAPHEIGHT * BRICKSIZE) + 64);

	dir = 0;
	if ((engine.isKey(KEY_UP)) || engine.getJoyUp())
		dir = -1;
	if (engine.isKey(KEY_DOWN) || engine.getJoyDown())
		dir = 1;

	if (player.flags & ENT_FLIES)
	{
		player.dy += (engine.cheatSpeed ? 0.3f : 0.1f) * dir;
		Math::limitFloat(&player.dy, (engine.cheatSpeed ? -3 : -1) * PLAYER_FLY_SPEED, (engine.cheatSpeed ? 3 : 1) * PLAYER_FLY_SPEED);
	}
	else if (dir == -1 && player.environment == ENV_AIR && player.falling == false)
	{
		player.falling = true;
		player.dy = (engine.cheatSpeed ? 3 : 1) * PLAYER_JUMP_SPEED;
		engine.clearKey(KEY_UP);
	}
	else if (player.environment != ENV_AIR)
	{
		player.dy = (engine.cheatSpeed ? 3 : 1) * (dir < 0 ? -PLAYER_WALK_SPEED : 2);
		player.animate();
	}

	
	/// DEBUG / CHEAT
	if (engine.keyState[SDLK_1])
		player.currentWeapon = &weapon[WP_PISTOL];
	else if (engine.keyState[SDLK_2])
		player.currentWeapon = &weapon[WP_MACHINEGUN];
	else if (engine.keyState[SDLK_3])
		player.currentWeapon = &weapon[WP_GRENADES];
	else if (engine.keyState[SDLK_4])
		player.currentWeapon = &weapon[WP_LASER];
	else if (engine.keyState[SDLK_5])
		player.currentWeapon = &weapon[WP_SPREAD];

	moveEntity(&player);

	// Math::limitFloat(&player.x, 10, map.limitRight + 608);
	// moveEntity() limits x < 10
	if (player.x > map.limitRight + 608)
	{
		player.x = map.limitRight + 608;
		if (player.dx > 0)
		{
			player.dx = 0;
		}
	}
	// Math::limitFloat(&player.y, map.limitUp + 5, (MAPHEIGHT * BRICKSIZE) + 64);
	if (player.y < map.limitUp + 5)
	{
		player.y = map.limitUp + 5;
		if (player.dy < 0)
		{
			player.dy = 0;
		}
	}
	else if (player.y > (MAPHEIGHT * BRICKSIZE) + 64)
	{
		player.y = (MAPHEIGHT * BRICKSIZE) + 64;
	}

	if (engine.isKey(KEY_FIRE) || engine.mouseLeft)
	{
		if (player.reload == 0)
		{
			float dx, dy, speed;
			Math::calculateSlope(engine.getMouseX(), engine.getMouseY(), x, y, &dx, &dy);
			speed = player.currentWeapon->getSpeed(false);
			dx *= speed; dy *= speed;
			addBullet(&player, dx, dy);
			if (player.currentWeapon == &weapon[WP_SPREAD])
			{
				addBullet(&player, dx, dy + 2);
				addBullet(&player, dx, dy - 2);
			}
		}
	}
	
	if (player.environment == ENV_WATER)
	{
		Math::removeBit(&player.flags, ENT_FLIES);

		addBubble(player.x, player.y);
		
		if ((player.thinktime == 30) && (player.oxygen == 0))
		{
			audio.playSound(SND_DROWNING, CH_ANY);
		}
	}

	player.think();
	
	if (player.fuel == 0)
	{
		player.setSprites(graphics.getSprite("BobRight", true), graphics.getSprite("BobLeft", true), graphics.getSprite("BobSpin", true));
	}
	
	if ((game.hasAquaLung) || (engine.cheatExtras))
	{
		player.oxygen = 7;
	}
		
	if (engine.cheatFuel)
	{
		player.fuel = 7;
	}

	if (((player.immune % 5) == 0) && (!(player.flags & ENT_TELEPORTING)))
	{
		if ((game.missionOverReason == MIS_COMPLETE) || (game.missionOverReason == MIS_INPROGRESS) || (game.missionOverReason == MIS_GAMECOMPLETE))
		{
			if (player.flags & ENT_FLIES)
			{
				player.animate();
				addFireTrailParticle(player.x + (player.face * 16) + Math::rrand(-1, 1), player.y + 10 + Math::rrand(-1, 1));
			}
			graphics.blit(player.getFaceImage(), x, y, graphics.screen, false);
		}
	}
}
