/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef MAPEDITOR_H
#define MAPEDITOR_H

#include <cstring>

extern void initSystem(const char * argv0);
extern void cleanup();
extern void loadResources();

extern void doItems();
extern void doPlayer();
extern void doTrains();
extern void doSwitches();
extern void doBullets();
extern void doEffects();
extern void doParticles();
extern void doMIAs();
extern void doObstacles();
extern void doTeleporters();
extern void doLineDefs();
extern void doTraps();

extern void addEnemy(const char *name, int x, int y, int flags);
extern void addItem(int itemType, const char *name, int x, int y, const char *spriteName, int health, int value, int flags, bool randomMovement);

class String {

	public:

		char string[1024];
		String *next;

		String()
		{
			strcpy(string, "");
			next = NULL;
		}

};

String *stringHead = new String;
String *stringTail = stringHead;

#endif //MAPEDITOR_H
