/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef CGRAPHICS_H
#define CGRAPHICS_H

#include "CEngine.h"
#include "defs.h"
#include <SDL/SDL_ttf.h>

#include <string>

class Graphics {

	private:

		Engine *engine;
		SDL_Rect gRect;
		TTF_Font *font[5];
		SDL_Color fontForeground, fontBackground;

		List spriteList;

		int fontSize;
		
		int waterAnim;
		int slimeAnim;
		int lavaAnim;

		int currentLoading;

		int screenShotNumber;
		char screenshot[100];
		char chatString[1024];

		SDL_Surface *fadeBlack;
		SDL_Surface *infoMessage;

		bool background_tile; ///< Wheter to tile or not the background image

		bool resized; ///< True, if the window was resized
	public:

		bool takeRandomScreenShots;

		SDL_Surface *screen, *background;
		SDL_Surface *tile[MAX_TILES];
		
		SDL_Surface *infoBar;

		int red, yellow, green, darkGreen, skyBlue, blue, cyan, white, lightGrey, grey, darkGrey, black;
		int width, height;

	Graphics();
	void free();
	void destroy();
	void registerEngine(Engine *engine);
	void mapColors();
	Sprite *getSpriteHead();
	void setTransparent(SDL_Surface *sprite);
	void updateScreen();
	void delay(int time);
	void RGBtoHSV(float r, float g, float b, float *h, float *s, float *v);
	void HSVtoRGB(float *r, float *g, float *b, float h, float s, float v);
	SDL_Surface *loadImage(const char *filename, bool colorkey = true);
	SDL_Surface * loadImage(const std::string& name, const bool colorkey = true) { return loadImage(name.c_str(), colorkey); };
	SDL_Surface *loadImage(const char *filename, int hue, int sat, int value);
	SDL_Surface *quickSprite(const char *name, SDL_Surface *image);
	void fade(int amount);
	void fadeToBlack();
	void loadMapTiles(const char *baseDir);
	void loadFont(int i, const char *filename, int pixelSize);
	Sprite *addSprite(const char *name);
	Sprite *getSprite(const char *name, const bool& required);
	void animateSprites();
	int getWaterAnim();
	int getSlimeAnim();
	int getLavaAnim();
	int getLavaAnim(int current);
	void loadBackground(const char *filename, bool tile = true);
	void putPixel(int x, int y, Uint32 pixel, SDL_Surface *dest);
	Uint32 getPixel(SDL_Surface *surface, int x, int y);
	void drawLine(float startX, float startY, float endX, float endY, int color, SDL_Surface *dest);
	void blit(SDL_Surface *image, int x, int y, SDL_Surface *dest, bool centered);
	void drawBackground();
	void drawBackground(SDL_Rect *r);
	void drawRect(int x, int y, int w, int h, int color, SDL_Surface *dest);
	void drawRect(int x, int y, int w, int h, int color, int borderColor, SDL_Surface *dest);
	void setFontColor(int red, int green, int blue, int red2, int green2, int blue2);
	void setFontSize(int size);
	SDL_Surface *getString(const char *in, bool transparent);
	SDL_Surface *getString(const std::string& in, bool transparent) { return getString(in.c_str(), transparent); };
	int GetStringWidth(const std::string& text);
	void drawString(const char *in, int x, int y, int alignment, SDL_Surface *dest);
	void drawString(std::string str, int x, int y, int align, SDL_Surface * dest) { drawString(str.c_str(), x, y, align, dest); };
	void clearChatString();
	void createChatString(char *in);
	void drawChatString(SDL_Surface *surface, int y);
	void drawWidgetRect(int x, int y, int w, int h);
	SDL_Surface *createSurface(int width, int height);
	SDL_Surface *alphaRect(int width, int height, Uint8 red, Uint8 green, Uint8 blue);
	void colorize(SDL_Surface *image, int red, int green, int blue);
	void lock(SDL_Surface *surface);
	void unlock(SDL_Surface *surface);
	void resetLoading();
	void showLoading(int amount, int max);
	void showErrorAndExit(const char *error_str, const char *param);
	void showErrorAndExit(std::string error_str, std::string param) { showErrorAndExit(error_str.c_str(), param.c_str()); };
	void showLicenseErrorAndExit();

	void resize();
	bool IfResized();

};

#define bx ((graphics.width/2)-(640/2))
#define by ((graphics.height/2)-(480/2))

extern Graphics graphics;

#endif //CGRAPHICS_H
