/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef MAIN_H
#define MAIN_H

#include "console/console.hpp"

extern void initSystem(const char * argv0);
extern void cleanup();

extern void newGame();
extern bool loadGame(int slot);

extern int doIntro();
extern int title();
extern int doGame();
extern int doHub();
extern void doCredits();

extern void showDemoStart();
extern void showDemoEnd();
extern int gameover();

extern void showMissionInformation(int mission);

extern void loadResources();

extern void showAllSprites();
extern void showMissionClear();

extern void checkStartCutscene();
extern void easyGameFinished();

#endif //MAIN_H
