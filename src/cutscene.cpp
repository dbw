/*
Copyright © 2004 Parallel Realities
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "CEngine.h"
#include "CGame.h"
#include "CGraphics.h"
#include "console/console.hpp"
#include "screens/cutscene.hpp"

/**
 * @file src/cutscene.cpp
 * Currently an ugly hack, see screens/cutscene.cpp for the actual cutscene code.
 */


/**
 * @todo Implement the new map system, then replace this ugly hack!
 */
void checkStartCutscene()
{
	#if DEMO
	return;
	#endif
	
	// Easy mode doesn't have cutscenes!
	//  * hmm.. in easy mode you couldn't reach these levels at all!
	/*if (game.skill == 0)
	{
		return;
		}*/
	
	std::string cutscene_file;

	debug("misc", 2) << "Check begin cutscene '" << game.stageName << "'" << std::endl; 

	if (strcmp(game.stageName, "BioMech Assimilator") == 0)
		cutscene_file = "levels/original/biomech_assimilator.start.dbwcs";
	else if (strcmp(game.stageName, "BioMech Communications") == 0)
		cutscene_file = "levels/original/biomech_communications.start.dbwcs";
	else if (strcmp(game.stageName, "BioMech HQ") == 0)
		cutscene_file = "levels/original/biomech_hq.start.dbwcs";
	else if (strcmp(game.stageName, "Final Battle") == 0)
		cutscene_file = "levels/original/final_battle.start.dbwcs";
	else if (strcmp(game.stageName, "BioMech Supply Depot") == 0)
		cutscene_file = "levels/original/biomech_supplydepot.start.dbwcs";
	else
		return;

	Screens::Cutscene cutscene(cutscene_file);

	bool ok = true;

	while (ok = cutscene.EventProcess())
	{
		cutscene.Draw();
		graphics.updateScreen();
		engine.getInput();
	}

}

void checkEndCutscene()
{
	/*	// Easy mode doesn't have cutscenes!
	if (game.skill == 0)
	{
		return;
		}*/

	debug("misc", 2) << "Check end cutscene '" << game.stageName << "'" << std::endl;

	std::string cutscene_file;

	if (strcmp(game.stageName, "Ancient Tomb #4") == 0)
		cutscene_file = "levels/original/ancient_tomb_4.end.dbwcs";
	else if (strcmp(game.stageName, "BioMech Assimilator") == 0)
		cutscene_file = "levels/original/biomech_assimilator.end.dbwcs";
	else if (strcmp(game.stageName, "BioMech Communications") == 0)
		cutscene_file = "levels/original/biomech_communications.end.dbwcs";
	else if (strcmp(game.stageName, "BioMech HQ") == 0)
		cutscene_file = "levels/original/biomech_hq.end.dbwcs";
	else if (strcmp(game.stageName, "BioMech Supply Depot") == 0)
		cutscene_file = "levels/original/biomech_supplydepot.end.dbwcs";
	else if (strcmp(game.stageName, "Final Battle") == 0)
		cutscene_file = "levels/original/final_battle.end.dbwcs";
	else
		return;
	
	Screens::Cutscene cutscene(cutscene_file);

	bool ok = true;

	while (ok = cutscene.EventProcess())
	{
		cutscene.Draw();
		graphics.updateScreen();
		engine.getInput();
	}
}

void easyGameFinished()
{

	Screens::Cutscene cutscene("levels/original/easy_mode.end.dbwcs");

	bool ok = true;

	while (ok = cutscene.EventProcess())
	{
		cutscene.Draw();
		graphics.updateScreen();
		engine.getInput();
	}
}



