/*
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/** @file console/console.hpp
 * Headers for Console class
 * @warning This file contains a lot of preprocessor directives.
 *  If you don't like them don't look in this file!
 */

#ifndef CONSOLE_HPP
#define CONSOLE_HPP

#include <iostream>

/// Really this does DebugOut's job
class DebugStream : public std::streambuf
{
	public:
		DebugStream(const std::string& message);
		/**
		 * Set's the actual file, line, and function
		 * @warning Don't use it directly, leave this job for debug, warning and error macros!
		 * @param file The current file (__FILE__)
		 * @param line The current line (__LINE__)
		 * @param func The current function (__PRETTY_FUNCTION__)
		 */

#ifdef DEBUG
		void SetPos(const std::string& file, unsigned short line, const std::string& func)
			{ sync(); this->file = file; this->line = line; this->func = func; };

		/// If true, it will display the current function true (only in DEBUG mode)
		static bool show_func;
#endif
	protected:
		virtual int overflow(int c);
		virtual int sync();
	private:
		void PutLineBeg();
		/// Buffer
		char buf[1024];

		std::string message;

#ifdef DEBUG		
		std::string file, func;
		unsigned short line;
		static unsigned short file_maxw, line_maxw, func_maxw;
#endif
		bool putline;
};

/**
 * DebugOut is a class that displays debug messages on the screen, but
 * with printing a message; the file and line if compiled in debug mode,
 * and also the function if it enabled by <tt>--show-func</tt> argument.
 */
class DebugOut : public std::ostream
{
	public:
		/**
		 * Creates a new DebugOut.
		 * @param message The message displayed between []
		 */
		DebugOut(const std::string& message) : std::ostream(stream = new DebugStream(message)) {};

		/// Poniter to the stream object
		DebugStream * stream;
	private:
		/// Don't copy it
		DebugOut(const DebugOut&) : stream(NULL) {};
		/// Really! Don't copy it. It's just stupid
		DebugOut& operator=(const DebugOut&) { return *this; }
};

#ifdef DEBUG

#include <map>

/// Class to manage debug levels
class DebugLevelManager
{
	public:
		static void ParseOpts(const std::string& opts);
		static bool IsToPrint(const std::string& module, unsigned char level);

	protected:
		static unsigned char global_level;
		static std::map<std::string, unsigned char> level_map;
};

extern DebugOut debug_out, warning_out, error_out;
#define debug(module, level) if (DebugLevelManager::IsToPrint((module), (level))) debug_out.stream->SetPos(__FILE__ + 7, __LINE__, __PRETTY_FUNCTION__), debug_out
#define warning warning_out.stream->SetPos(__FILE__ + 7, __LINE__, __PRETTY_FUNCTION__), warning_out
#define error error_out.stream->SetPos(__FILE__ + 7, __LINE__, __PRETTY_FUNCTION__), error_out

#else //ifdef DEBUG

extern DebugOut warning_out, error_out;
#define debug(a, b) if (false) std::cout
#define warning warning_out
#define error error_out

#endif //ifndef DEBUG

#endif
