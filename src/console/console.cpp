/*
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/** @file console/console.cpp
 * Contains methods to write to console
 * @warning This file contains a lot of preprocessor directives.
 *  If you don't like them don't look in this file!
 */

#include <cstdio>
#include <iomanip>
#include <sstream>

#include "console.hpp"

/**
 * Initializes DebugStream and set the message to
 * @param message Set message to
 */
DebugStream::DebugStream(const std::string& message) : message(message),
#ifdef DEBUG
													   file(""), func(""), line(0),
#endif
													   putline(true)
{
	setp(buf, buf + sizeof(buf));
}

/// If the buffer is out of space
int DebugStream::overflow(int c)
{
	size_t size = pptr() - pbase();
	if (!size) return 0;

	for (char * pos = pbase(); pos < pptr(); pos++)
	{
		if (putline)
			PutLineBeg();

		putline = (*pos == '\n');

		putchar(*pos);
	}

	if (c != traits_type::eof())
	{
		if (putline)
			PutLineBeg();

		putline = (char(c) == '\n');		

		putchar(char(c));
	}

	setp(buf, buf + sizeof(buf));
	return 0;
}

/// Flush the cache
int DebugStream::sync()
{
	return overflow(traits_type::eof());
}

/// Writes out the beginning of the line (message, func, file, line)
void DebugStream::PutLineBeg()
{
	std::stringstream str;

#ifdef DEBUG
	if (file.size() > file_maxw) file_maxw = file.size();
	str << line;
	if (str.str().size() > line_maxw) line_maxw = str.str().size();
	str.str("");
	if (func.size() > func_maxw) func_maxw = func.size();

	str << "[" << message << "] { ";

	if (show_func)
		str << std::setw(func_maxw) << func << " (";

	str << std::setw(file_maxw) << file << ":" << std::setw(line_maxw) << line;

	if (show_func)
		str << ")";
	str << " } : ";

#else
	str << "[" << message << "] ";
#endif

	fputs(str.str().c_str(), stdout);
}

#ifdef DEBUG

unsigned short DebugStream::file_maxw = 16;
unsigned short DebugStream::line_maxw = 3;
unsigned short DebugStream::func_maxw = 74;
bool DebugStream::show_func = false;

#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

/* static */ void DebugLevelManager::ParseOpts(const std::string& opts)
{
	boost::char_separator<char> sep(",");
	boost::tokenizer<boost::char_separator<char> > tokens(opts, sep);
	
	for (boost::tokenizer<boost::char_separator<char> >::iterator iter = tokens.begin(); iter != tokens.end(); iter++)
	{
		size_t pos;
		if ((pos = iter->find_first_of("=")) == std::string::npos)
		{
			try
			{
				global_level = boost::lexical_cast<unsigned short>(*iter);
			}
			catch (const boost::bad_lexical_cast& e)
			{
				error << "Failed to set global level to '" << *iter << "'!" << std::endl
					  << "The error was: " << e.what() << " Parameter ignored." << std::endl;
			}
		}
		else
		{
			try
			{
				level_map[iter->substr(0, pos)] = boost::lexical_cast<unsigned short>(iter->substr(pos + 1, iter->size() - pos - 1));
			}
			catch (const boost::bad_lexical_cast& e)
			{
				error << "Failed to set '" << iter->substr(0, pos) << "' group to '" << iter->substr(pos + 1, iter->size() - pos - 1) << "'!" << std::endl
					  << "The error was: " << e.what() << " Ignored." << std::endl;
			}
		}
	}
}

/* static */ bool DebugLevelManager::IsToPrint(const std::string& module, const unsigned char level)
{
	if (level_map.count(module) == 0)
		return global_level >= level;
	else
		return level_map[module] >= level;
}

unsigned char DebugLevelManager::global_level = 0;
std::map<std::string, unsigned char> DebugLevelManager::level_map;

DebugOut debug_out  (" Debug ");
#endif //ifdef DEBUG
DebugOut warning_out("WARNING");
DebugOut error_out  ("!ERROR!");
