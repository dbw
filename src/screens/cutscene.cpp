/*
Copyright © 2007-2008 Kővágó Zoltán <DirtY.iCE.hu@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/**
 * @file screens/cutscene.cpp
 * A cutscene screen..
 */

#include <sstream>
#include "cutscene.hpp"
#include "lisp/list_iterator.hpp"
#include <stdexcept>
#include <cmath>

#include "CAudio.h"
#include "CEngine.h"
#include "CGraphics.h"
extern Audio audio;
extern Engine engine;
extern Graphics graphics;

#include "console/console.hpp"
#include "font/manager.hpp"

namespace Screens
{

/**
 * Copy constructor
 * @param page An another Page, constructor will initialized about that.
 */
Cutscene::Page::Page(const Page& page)
{
	background = SDL_DisplayFormat(page.background);
	text = page.text;
	wait_time = page.wait_time;
	time_begin = 0;
	scaled_background = NULL;
	backdrop = NULL;

	alpha = 0;
}

/**
 * Destroy the Page.
 */
Cutscene::Page::~Page()
{
	std::vector<SDL_Surface *>::iterator iter = lines.begin();
	for (; iter != lines.end(); iter++)
		SDL_FreeSurface(*iter);
	SDL_FreeSurface(background);
}

/**
 * Assign operator. Nearly same as running the destructor, and the copy constructor.
 * @param page An another Page instance
 */
void Cutscene::Page::operator= (const Page& page)
{
	std::vector<SDL_Surface *>::iterator iter = lines.begin();
	for (; iter != lines.end(); iter++)
		SDL_FreeSurface(*iter);
	SDL_FreeSurface(background);

	background = page.background;
	background->refcount++;
	text = page.text;
	wait_time = page.wait_time;
	time_begin = page.time_begin;
	scaled_background = NULL;
	backdrop = NULL;
	alpha = 0;
}

/**
 * Loads a background image. Image loaded through PhysFS and can be in any format,
 * that SDL_image supports.
 * @param name Name of the image file
 */
void Cutscene::Page::LoadBackground(const std::string& name)
{
	SDL_FreeSurface(background);
	SDL_FreeSurface(scaled_background);
	
	// we bypass the sprite manager, and we manage resources directly :)
	background = graphics.loadImage(name, false);
}

/**
 * Rescales the background to the size of the screen. It uses bilinear interpolation.
 * @see http://en.wikipedia.org/wiki/Bilinear_interpolation for more info
 */
void Cutscene::Page::ScaleBackground()
{
	SDL_FreeSurface(scaled_background);

	// if the background has the same size of the screen, don't blame
	if ((graphics.width == background->w) && (graphics.height == background->h))
	{
		scaled_background = background;
		background->refcount++;
	}
	else
	{
		scaled_background = SDL_CreateRGBSurface(SDL_SWSURFACE, graphics.width, graphics.height, 32, 0,0,0,0);

		// how much we have to scale
		float xscale = (float) background->w / graphics.width;
		float yscale = (float) background->h / graphics.height;

		// x and y pos in the original image
		int fromx, fromy;
		// pixel buffers.. maybe we should use a function to access the image
		int * scaled_pixbuf, * pixbuf;
		scaled_pixbuf = (int *) scaled_background->pixels;
		pixbuf = (int *) background->pixels;

		//store the colors [0..3] : original colors, [4]: interpolated color
		Uint8 r[5], g[5], b[5];
		//plus how may pixels (less than 1, for bilinear interpolation)
		float plusx, plusy;

		for (int x = 0; x < graphics.width; x++)
			for (int y = 0; y < graphics.height; y++)
			{
				fromx = static_cast<int>(floor(x * xscale));
				fromy = static_cast<int>(floor(y * yscale));

				plusx = (x * xscale) - fromx;
				plusy = (y * yscale) - fromy;
				
				// set the source colors [0]: topleft, [1]: topright, [2]: bottomleft, [3]: bottomright
				SDL_GetRGB(pixbuf[(fromy * background->w) + fromx], background->format, r, g, b);
				SDL_GetRGB(pixbuf[(fromy * background->w) + std::min(fromx + 1, background->w - 1)], background->format, &r[1], &g[1], &b[1]);
				SDL_GetRGB(pixbuf[(std::min(fromy + 1, background->h - 1) * background->w) + fromx], background->format, &r[2], &g[2], &b[2]);
				SDL_GetRGB(pixbuf[(std::min(fromy + 1, background->h - 1) * background->w) + std::min(fromx + 1, background->w -1)], background->format, &r[3], &g[3], &b[3]);

				// calculate bilinear interpolation
				// see http://en.wikipedia.org/wiki/Bilinear_interpolation for more info
				r[4] = static_cast<Uint8>(r[0] * (1 - plusx) * (1 - plusy) + r[1] * plusx * (1 - plusy) + r[2] * (1 - plusx) * plusy + r[3] * plusx * plusy);
				g[4] = static_cast<Uint8>(g[0] * (1 - plusx) * (1 - plusy) + g[1] * plusx * (1 - plusy) + g[2] * (1 - plusx) * plusy + g[3] * plusx * plusy);
				b[4] = static_cast<Uint8>(b[0] * (1 - plusx) * (1 - plusy) + b[1] * plusx * (1 - plusy) + b[2] * (1 - plusx) * plusy + b[3] * plusx * plusy);
				
				// we've done, set the color
				scaled_pixbuf[(y * scaled_background->w) + x] = SDL_MapRGB(scaled_background->format, r[4], g[4], b[4]);
			}

	}
} //Cutscene::Page::ScaleBackground()

/**
 * Sets the text of the page
 * @param text The text to the text should set to
 */
void Cutscene::Page::SetText(const std::string& text)
{
	this->text = text;
	lines.clear();
}

/**
 * Makes a list of surfaces, from the text
 */
void Cutscene::Page::MakeText()
{
	lines.clear();

	/* we work with an input stream, so we don't have to care about newlines
	 * also, this means, multiple whitespaces will lost
	 * non-breakable space might help (remember - cutscenes are in UTF-8!) */
	std::istringstream input(text);

	std::string tmp_string, word;

	int width;

	while (input.good())
	{
		input >> word;

		// get the width of the text
		width = graphics.GetStringWidth(tmp_string + " " + word);

		// if it's bigger than screen width
		if (width > graphics.width)
		{
			// discard it, push the older string to the list
			lines.push_back(graphics.getString(tmp_string, false));
			// and set tmp_string to the word
			tmp_string = word;
		}
		else
			// otherwise try the next word
			tmp_string += " " + word;
	}

	// the last words will stay in tmp_string, so we have to push it manually
	lines.push_back(graphics.getString(tmp_string, false));

	// the backdrop.. backdrop is the black surface behind the text
	SDL_FreeSurface(backdrop);
	backdrop = SDL_CreateRGBSurface(SDL_HWSURFACE, graphics.width, lines.size()*20, 8, 0, 0, 0, 0);
}

/**
 * Process events. (button press, resize)
 * @return false if the page is end, otherwise true
 */
bool Cutscene::Page::EventProcess()
{
	// we can't set it on initialize, because we initialize all pages on the same time
	if (!time_begin)
		time_begin = SDL_GetTicks();

	// if the user pressed a key, or the time is over, return false
	if (((engine.userAccepts()) /*&& (allowSkip)*/) ||
		(SDL_GetTicks() > (time_begin + wait_time*1000)))
	{
		engine.clearInput();
		return false;
	}

	// if the screen was resized, rescale background and made the text again
	if (graphics.IfResized())
	{
		ScaleBackground();
		MakeText();
	}

	return true;
}

/**
 * Draws the page
 */
void Cutscene::Page::Draw()
{
	/* check for a scaled background - we don't made it in the constructor
	 * if we would do it, then the user resizes the window on a page, the other
	 * pages won't resize, that would cause funny images */
	if (!scaled_background)
		ScaleBackground();
	graphics.blit(scaled_background, 0,0, graphics.screen, false);

	// same to text
	if (lines.empty())
		MakeText();

	// if we dont have full alpha, increase it
	// we have to do it after surfaces was made
	if (alpha != 255)
	{
		// max to 255 (alpha is char!)
		alpha = std::min(255, (int) (alpha + engine.getTimeDifference()));
		// set backdrop's alpha
		SDL_SetAlpha(backdrop, SDL_SRCALPHA | SDL_RLEACCEL, alpha);

		// set alpha for the lines
		std::vector<SDL_Surface *>::iterator iter = lines.begin();
		for (; iter != lines.end(); iter++)
			SDL_SetAlpha(*iter, SDL_SRCALPHA | SDL_RLEACCEL, alpha);
	}

	int y = graphics.height;
	std::vector<SDL_Surface *>::reverse_iterator iter = lines.rbegin();

	// blit the black bacdrop, so the background of the text will blacj
	graphics.blit(backdrop, 0, y - backdrop->h, graphics.screen, false);

	// blit the texts, from down to up
	for (; iter != lines.rend(); iter++)
	{
		y -= 20;

		graphics.blit(*iter, 0, y, graphics.screen, false);
	}
}

/**
 * The constructor for a cutscene. This initializes everything, needed by the cutscene.
 * @param name The name of the cutscene's file. Read by PhysFS.
 */
Cutscene::Cutscene(const std::string& name)
{
	debug("misc", 2) << "Loading cutscene '" << name << "'" << std::endl;

	engine.clearInput();
	engine.flushInput();

	graphics.setFontSize(0);

	const lisp::Lisp * cutscene_root = parser.parse(name);
	const lisp::Lisp * cutscene = cutscene_root->get_lisp("dirtyblobwars-cutscene");

	lisp::ListIterator iter = cutscene;

	bool version_ok = false;
	std::string music = "music/daisyChain2.mod"; 

	std::vector<Page>::iterator page_iter = pages.begin();

	while(iter.next())
	{
		if (iter.item() == "version")
		{
			if (iter.value()->get_int() != 1)
				throw std::runtime_error("Bad version number.");
			else
				version_ok = true;
		}
		else if (iter.item() == "music")
			music = iter.value()->get_string();
		else if (iter.item() == "page")
		{
			Page page;
			if (!pages.empty())
				page = pages.back();
			
			lisp::ListIterator sub_iter = iter.lisp();
			while (sub_iter.next())
			{
				if (sub_iter.item() == "image")
					page.LoadBackground(sub_iter.value()->get_string());
				else if (sub_iter.item() == "time")
					page.SetWaitTime(sub_iter.value()->get_int());
				else if (sub_iter.item() == "text")
					page.SetText(sub_iter.value()->get_string());
				else
					throw std::runtime_error("Unknown token inside a page: " + sub_iter.item());
			}
			pages.push_back(page);

		}
		else
			throw std::runtime_error("Unknown token: " + iter.item());
	}

	if (!version_ok)
		throw std::runtime_error("Version not specified!");

	audio.loadMusic(music);
	audio.playMusic();

	current_page = pages.begin();

	engine.resetTimeDifference();
} // Cutscene::Cutscene(const std::string& name);

/**
 * Process event on the current page
 * @return false, if the cutscene ended, otherwise true
 */
bool Cutscene::EventProcess()
{
	if (!current_page->EventProcess())
		if (current_page != pages.end() - 1)
			current_page++;
		else
			return false;

	engine.doTimeDifference();
	return true;
}

/**
 * Draws the current page
 */
void Cutscene::Draw()
{
	current_page->Draw();
}

} //namespace Screens

