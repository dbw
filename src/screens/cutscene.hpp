/*
Copyright (C) 2007 K�v�g� Zolt�n

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/** @file screens/cutscene.hpp
 * Headers for cutscene screen
 */

#include "lisp/parser.hpp"

#include <SDL/SDL.h>
#include <vector>

namespace Screens {

/**
 * A Screen, that shows a cutscene. Cutscenes are made of pages, every page has an image, some text,
 * and a maximum waiting time. The whole cutscene has a background music, and it shown before or after
 * a level.
 * @see Screen
 * @see @ref cutscenes for info about the file format
 */
class Cutscene //: public Screen
{
	public:
		Cutscene(const std::string& name);
		//~Cutscene() {};

		bool EventProcess();
		void Draw();

	private:
		/// The lisp parser, used by cutscene
		lisp::Parser parser;

		/**
		 * The Page class. The page is a part of cutscene, which has an image som text, and a wait time.
		 */
		class Page
		{
			public:
				/// Default constroctor. Initializes everithing to null..
				Page() : background(NULL), scaled_background(NULL), text(""), lines(), wait_time(0), time_begin(0), alpha(0) {};
				Page(const Page& page);
				void operator= (const Page& page);
				~Page();
				
				void LoadBackground(const std::string& name);
				void SetWaitTime(const int& time) { wait_time = time; };
				void SetText(const std::string& text);

				bool EventProcess();
				void Draw();

			private:
				void ScaleBackground();
				void MakeText();

				SDL_Surface * background; ///< The unscaled background.
				SDL_Surface * scaled_background; ///< The scaled background. NULL if it hasn't scaled.
				SDL_Surface * backdrop; ///< The backdrop image (a black box behind the text)
				std::string text; ///< The text of the page
				std::vector<SDL_Surface *> lines; ///< The text, rendered into SDL_Surfaces, and stored per line
				unsigned int wait_time; ///< Time to wait. In sec.

				unsigned int time_begin; ///< What time did the page begin? NULL, if we didn't start yet.

				unsigned char alpha; ///< Alpha of the backdrop
		};

		std::vector<Page> pages; ///< A vector of pages. When there's no more pages, cutscene ends.
		std::vector<Page>::iterator current_page; ///< An iterator to the current page.
};

}
