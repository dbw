<?xml version='1.0' encoding='ISO-8859-1' standalone='yes' ?>
<tagfile>
  <compound kind="page">
    <name>index</name>
    <title>PhysicsFS</title>
    <filename>index</filename>
  </compound>
  <compound kind="file">
    <name>physfs.h</name>
    <path>/home/dirty_ice/dev/physfs-1.1.1/</path>
    <filename>physfs_8h</filename>
    <class kind="struct">PHYSFS_File</class>
    <class kind="struct">PHYSFS_ArchiveInfo</class>
    <class kind="struct">PHYSFS_Version</class>
    <class kind="struct">PHYSFS_Allocator</class>
    <member kind="define">
      <type>#define</type>
      <name>PHYSFS_file</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>d32c738a1a111f0d3f29e4637633bfd1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PHYSFS_VERSION</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>cb77c1f8f70028add43b9f81eef1ed78</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="typedef">
      <type>unsigned char</type>
      <name>PHYSFS_uint8</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>bb348466106a912e03cf63f494a30356</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>signed char</type>
      <name>PHYSFS_sint8</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>76505754bb5c7f8f5e964e1a4d1a2571</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned short</type>
      <name>PHYSFS_uint16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>c4d1c74e276b04ef993530a405553574</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>signed short</type>
      <name>PHYSFS_sint16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>eed97311e1294c0b42648f99d331e5a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned int</type>
      <name>PHYSFS_uint32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>ae011b45ebd619edb193fe196e1f5f74</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>signed int</type>
      <name>PHYSFS_sint32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>e0264996863470a315e29d5a740a0f36</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned long long</type>
      <name>PHYSFS_uint64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>632ba02500231e5f6f6df2e4cb71c818</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>signed long long</type>
      <name>PHYSFS_sint64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>88c5f1d7b088888c729b22644651c9f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>PHYSFS_StringCallback</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>6331ac65f8e0d32288f8597e48188a4f</anchor>
      <arglist>)(void *data, const char *str)</arglist>
    </member>
    <member kind="typedef">
      <type>void(*</type>
      <name>PHYSFS_EnumFilesCallback</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>51c1e0145502aeaf080973d308d33c6a</anchor>
      <arglist>)(void *data, const char *origdir, const char *fname)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PHYSFS_getLinkedVersion</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>8977d0ba0a2301256e823532479f1a05</anchor>
      <arglist>(PHYSFS_Version *ver)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_init</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>61b94cac8e65267afb8a5f2344e60dd1</anchor>
      <arglist>(const char *argv0)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_deinit</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>a465c9b58e5020f503085d433a413b5b</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>const PHYSFS_ArchiveInfo **</type>
      <name>PHYSFS_supportedArchiveTypes</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>4ca65fbaa485d68113912bbbd70bd19e</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PHYSFS_freeList</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>f6aeb2ffdcee44f02cf380da62462b05</anchor>
      <arglist>(void *listVar)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>PHYSFS_getLastError</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>9f78e4ab560885fd10202988d2fff1b6</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>PHYSFS_getDirSeparator</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>d1dc17d387c45e9155d1f0c348a7cbcc</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PHYSFS_permitSymbolicLinks</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>ad451d9b3f46f627a1be8caee2eef9b7</anchor>
      <arglist>(int allow)</arglist>
    </member>
    <member kind="function">
      <type>char **</type>
      <name>PHYSFS_getCdRomDirs</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>f9a9674a36f7c96ed7813f9db06eefed</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>PHYSFS_getBaseDir</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>0034c327dfb381a1c95deb80878bcede</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>PHYSFS_getUserDir</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>9d86fca7a5cbbc53f5425ff7539b1426</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>PHYSFS_getWriteDir</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>6533ff91180a4c8abfe24d458f6b9915</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_setWriteDir</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>36c408d40b3a93c8f9fc02a16c02e430</anchor>
      <arglist>(const char *newDir)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_addToSearchPath</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>0956d926822291a23ee540c2ffefa1af</anchor>
      <arglist>(const char *newDir, int appendToPath)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_removeFromSearchPath</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>1e4ed59d6c80b8fb28904723cfd5b510</anchor>
      <arglist>(const char *oldDir)</arglist>
    </member>
    <member kind="function">
      <type>char **</type>
      <name>PHYSFS_getSearchPath</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>c738531e9c7e5ca3797b64985c103cf4</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_setSaneConfig</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>fef8700f714b6800ff688372c540bfe2</anchor>
      <arglist>(const char *organization, const char *appName, const char *archiveExt, int includeCdRoms, int archivesFirst)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_mkdir</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>e11fb98bf8c08a2e028f52ac9a728aa9</anchor>
      <arglist>(const char *dirName)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_delete</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>27a939bce4c1132bacdfcb3d3cc29e37</anchor>
      <arglist>(const char *filename)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>PHYSFS_getRealDir</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>539bcf0ebde6271e3e4a90dd287ca975</anchor>
      <arglist>(const char *filename)</arglist>
    </member>
    <member kind="function">
      <type>char **</type>
      <name>PHYSFS_enumerateFiles</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>0f4ae950c2dae0735a91263ddd20fbf4</anchor>
      <arglist>(const char *dir)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_exists</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>ccd31941b636ff153d328aebf72048c7</anchor>
      <arglist>(const char *fname)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_isDirectory</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>5877fcafb72dca892c1c2465292eef47</anchor>
      <arglist>(const char *fname)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_isSymbolicLink</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>e44880fa7528d0b4edb00034324a0884</anchor>
      <arglist>(const char *fname)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_sint64</type>
      <name>PHYSFS_getLastModTime</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>58e182a3deb67dd780f1de23440fa3e5</anchor>
      <arglist>(const char *filename)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_File *</type>
      <name>PHYSFS_openWrite</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>bbf92d66c6cb82d0ede969aa2c964fba</anchor>
      <arglist>(const char *filename)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_File *</type>
      <name>PHYSFS_openAppend</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>beeca12e2fe7c5f68cbd67e2e5cafa3d</anchor>
      <arglist>(const char *filename)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_File *</type>
      <name>PHYSFS_openRead</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>5e38a425bb0b44c09ec25b105f770c63</anchor>
      <arglist>(const char *filename)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_close</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>6822f8ff10073e855a1c3a6485b882f2</anchor>
      <arglist>(PHYSFS_File *handle)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_sint64</type>
      <name>PHYSFS_read</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>91234f576477f7de67d635761d01aa7b</anchor>
      <arglist>(PHYSFS_File *handle, void *buffer, PHYSFS_uint32 objSize, PHYSFS_uint32 objCount)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_sint64</type>
      <name>PHYSFS_write</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>f5dc83d5e4ef0b0c9bce1834ea1bb498</anchor>
      <arglist>(PHYSFS_File *handle, const void *buffer, PHYSFS_uint32 objSize, PHYSFS_uint32 objCount)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_eof</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>6d7ac838797951323072060392645dbe</anchor>
      <arglist>(PHYSFS_File *handle)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_sint64</type>
      <name>PHYSFS_tell</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>0d481bdd4cf32ed1a41540ee59840c24</anchor>
      <arglist>(PHYSFS_File *handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_seek</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>e562b45b1e1b02505ce1d82ad785f04a</anchor>
      <arglist>(PHYSFS_File *handle, PHYSFS_uint64 pos)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_sint64</type>
      <name>PHYSFS_fileLength</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>3f1d8b415d37d8040ca423ed29536fde</anchor>
      <arglist>(PHYSFS_File *handle)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_setBuffer</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>ede5de47197e1a613d2091802befc886</anchor>
      <arglist>(PHYSFS_File *handle, PHYSFS_uint64 bufsize)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_flush</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>3fd8f9cfdfd09f4fb778a386d8e97e8a</anchor>
      <arglist>(PHYSFS_File *handle)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_sint16</type>
      <name>PHYSFS_swapSLE16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>73f900bb3ed3a2df81cf7c12b83b0fd4</anchor>
      <arglist>(PHYSFS_sint16 val)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_uint16</type>
      <name>PHYSFS_swapULE16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>cb9b90e29f097f4feaa4568ec405db01</anchor>
      <arglist>(PHYSFS_uint16 val)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_sint32</type>
      <name>PHYSFS_swapSLE32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>9b59892fb2846084a272c1bfc4aade4e</anchor>
      <arglist>(PHYSFS_sint32 val)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_uint32</type>
      <name>PHYSFS_swapULE32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>6e86a4c1461dc47784e3f68034ad97c9</anchor>
      <arglist>(PHYSFS_uint32 val)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_sint64</type>
      <name>PHYSFS_swapSLE64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>f032bd8e6116b2b3b6de279cb05b98a8</anchor>
      <arglist>(PHYSFS_sint64 val)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_uint64</type>
      <name>PHYSFS_swapULE64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>4b7249bf3cae48f1e04d99505011f280</anchor>
      <arglist>(PHYSFS_uint64 val)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_sint16</type>
      <name>PHYSFS_swapSBE16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>5fc8e4b9696316e25e3f6422910726c6</anchor>
      <arglist>(PHYSFS_sint16 val)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_uint16</type>
      <name>PHYSFS_swapUBE16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>056321eea71215938431ad4501c2cf9c</anchor>
      <arglist>(PHYSFS_uint16 val)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_sint32</type>
      <name>PHYSFS_swapSBE32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>8908fb655aa60f0feb2f1662c93c8a0e</anchor>
      <arglist>(PHYSFS_sint32 val)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_uint32</type>
      <name>PHYSFS_swapUBE32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>464ba8bc7f121e7f31423ad5d2ec6ed2</anchor>
      <arglist>(PHYSFS_uint32 val)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_sint64</type>
      <name>PHYSFS_swapSBE64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>974af7dedc1125735efd9b14e23f20e6</anchor>
      <arglist>(PHYSFS_sint64 val)</arglist>
    </member>
    <member kind="function">
      <type>PHYSFS_uint64</type>
      <name>PHYSFS_swapUBE64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>6c9b62d93cd121039c00554cb627a425</anchor>
      <arglist>(PHYSFS_uint64 val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_readSLE16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>0959e10405416c139aad78dbc21617a0</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_sint16 *val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_readULE16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>f13248f4af186eda3983831dd8632da1</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_uint16 *val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_readSBE16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>d524c869540cc428264c318330a89144</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_sint16 *val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_readUBE16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>083c97d848b89e08f6529c31cd7dba67</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_uint16 *val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_readSLE32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>2e2bfb4e8204c99f75642c4833e18566</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_sint32 *val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_readULE32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>def7cf2fb6311b0bc9d39634b4088545</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_uint32 *val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_readSBE32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>ab2bc016ea5dbe2a91479045576e644d</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_sint32 *val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_readUBE32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>357b5757a9483ef893977761d7d3fc1e</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_uint32 *val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_readSLE64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>e916644039fad7ae4cb7d81440150d35</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_sint64 *val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_readULE64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>e72ad9e14404add23d432838f4e9f35a</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_uint64 *val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_readSBE64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>04eabd9e8da4265ede4018b24d4a39b1</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_sint64 *val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_readUBE64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>380430eda70acd8728930a2f1256c071</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_uint64 *val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_writeSLE16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>eaee4b15c4d683a1996ffc72d1d2a6aa</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_sint16 val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_writeULE16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>dd6bdd5da0e7466b2bace0c914bcf3ac</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_uint16 val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_writeSBE16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>621ed6d4cb61f9f31efb58acbe3090e3</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_sint16 val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_writeUBE16</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>66a392f7637bb2ad4c3e395e1a71c54e</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_uint16 val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_writeSLE32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>3a702d04d16b0fa732905b0e7daab407</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_sint32 val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_writeULE32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>9566e5d04e793b851dc7b9c26f3b23cb</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_uint32 val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_writeSBE32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>065910571b8d1bc82eb6ef8e93936028</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_sint32 val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_writeUBE32</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>98a497bc47cb177fe1bf835abe28baad</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_uint32 val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_writeSLE64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>b48f7be67e4d68315709e7b3fa48f34b</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_sint64 val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_writeULE64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>aeb795dbdde0fb87fbc8b92471fb85ab</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_uint64 val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_writeSBE64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>b5f40bbc37733f52fde58e7a5bc55341</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_sint64 val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_writeUBE64</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>03fc124e5dfd2801361eeefcee7d1ff7</anchor>
      <arglist>(PHYSFS_File *file, PHYSFS_uint64 val)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_isInit</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>6154be7bfa5d924476356a9fbd0d8223</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_symbolicLinksPermitted</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>5223ccbe06246dd5379d6a57d6cd0d87</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_setAllocator</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>e6297d1ae7755b9236b01950bfbb0ef1</anchor>
      <arglist>(const PHYSFS_Allocator *allocator)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>PHYSFS_mount</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>8eb320e9af03dcdb4c05bbff3ea604d4</anchor>
      <arglist>(const char *newDir, const char *mountPoint, int appendToPath)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>PHYSFS_getMountPoint</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>365aee0c326b0e6bfbba5c039f7c4b5b</anchor>
      <arglist>(const char *dir)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PHYSFS_getCdRomDirsCallback</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>ce04dfd0654ce55a81a85f57f56c7410</anchor>
      <arglist>(PHYSFS_StringCallback c, void *d)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PHYSFS_getSearchPathCallback</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>bd5f7188ab27722b0651e7503c6a737a</anchor>
      <arglist>(PHYSFS_StringCallback c, void *d)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PHYSFS_enumerateFilesCallback</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>9d3541369a4eb0a95f946bc3f4103dfb</anchor>
      <arglist>(const char *dir, PHYSFS_EnumFilesCallback c, void *d)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PHYSFS_utf8FromUcs4</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>c18c9c45db1be6fd26be19d0f4d25a60</anchor>
      <arglist>(const PHYSFS_uint32 *src, char *dst, PHYSFS_uint64 len)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PHYSFS_utf8ToUcs4</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>3ff1045e71cd4c14607973da7bfc41e5</anchor>
      <arglist>(const char *src, PHYSFS_uint32 *dst, PHYSFS_uint64 len)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PHYSFS_utf8FromUcs2</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>c4fdac7e09fb03ccdccdeed997f09c1a</anchor>
      <arglist>(const PHYSFS_uint16 *src, char *dst, PHYSFS_uint64 len)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PHYSFS_utf8ToUcs2</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>0ab51c265f7ca4e8218d6c74b4f43401</anchor>
      <arglist>(const char *src, PHYSFS_uint16 *dst, PHYSFS_uint64 len)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PHYSFS_utf8FromLatin1</name>
      <anchorfile>physfs_8h.html</anchorfile>
      <anchor>2838ad1240439afd3a21109ea7138014</anchor>
      <arglist>(const char *src, char *dst, PHYSFS_uint64 len)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>PHYSFS_Allocator</name>
    <filename>structPHYSFS__Allocator.html</filename>
    <member kind="variable">
      <type>int(*</type>
      <name>Init</name>
      <anchorfile>structPHYSFS__Allocator.html</anchorfile>
      <anchor>ea7ba8bea9a99b9719ac29bd8352ff51</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>Deinit</name>
      <anchorfile>structPHYSFS__Allocator.html</anchorfile>
      <anchor>9ee00828d38fcf741814356c214a54fe</anchor>
      <arglist>)(void)</arglist>
    </member>
    <member kind="variable">
      <type>void *(*</type>
      <name>Malloc</name>
      <anchorfile>structPHYSFS__Allocator.html</anchorfile>
      <anchor>0ad5c0a2612ceaadc1ed6d95e8ce66f4</anchor>
      <arglist>)(PHYSFS_uint64)</arglist>
    </member>
    <member kind="variable">
      <type>void *(*</type>
      <name>Realloc</name>
      <anchorfile>structPHYSFS__Allocator.html</anchorfile>
      <anchor>7bd602e0ac4c50ad58fbf7c1cf263ed9</anchor>
      <arglist>)(void *, PHYSFS_uint64)</arglist>
    </member>
    <member kind="variable">
      <type>void(*</type>
      <name>Free</name>
      <anchorfile>structPHYSFS__Allocator.html</anchorfile>
      <anchor>d63aeb97ba59185434cd24f6662ea13f</anchor>
      <arglist>)(void *)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>PHYSFS_ArchiveInfo</name>
    <filename>structPHYSFS__ArchiveInfo.html</filename>
    <member kind="variable">
      <type>const char *</type>
      <name>extension</name>
      <anchorfile>structPHYSFS__ArchiveInfo.html</anchorfile>
      <anchor>fedfd22d607fe194bbc6ae9bda021342</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const char *</type>
      <name>description</name>
      <anchorfile>structPHYSFS__ArchiveInfo.html</anchorfile>
      <anchor>a544edc050c4b8bea6cf6a0385aba736</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const char *</type>
      <name>author</name>
      <anchorfile>structPHYSFS__ArchiveInfo.html</anchorfile>
      <anchor>359cd08b6f06b3845234e4cd06501ea2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const char *</type>
      <name>url</name>
      <anchorfile>structPHYSFS__ArchiveInfo.html</anchorfile>
      <anchor>2b54ca7e3dd183f133c83eeab8399f9f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>PHYSFS_File</name>
    <filename>structPHYSFS__File.html</filename>
    <member kind="variable">
      <type>void *</type>
      <name>opaque</name>
      <anchorfile>structPHYSFS__File.html</anchorfile>
      <anchor>594c10e8f0537c4f2cd8f58f5bc8a748</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>PHYSFS_Version</name>
    <filename>structPHYSFS__Version.html</filename>
    <member kind="variable">
      <type>PHYSFS_uint8</type>
      <name>major</name>
      <anchorfile>structPHYSFS__Version.html</anchorfile>
      <anchor>c60a805b6b7c342cb1718356f5be4dc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>PHYSFS_uint8</type>
      <name>minor</name>
      <anchorfile>structPHYSFS__Version.html</anchorfile>
      <anchor>753a884de5c4ef33bff3dfb2e0bb1a0c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>PHYSFS_uint8</type>
      <name>patch</name>
      <anchorfile>structPHYSFS__Version.html</anchorfile>
      <anchor>565624d6f0785e5cb4c706583c682e20</anchor>
      <arglist></arglist>
    </member>
  </compound>
</tagfile>
