#!/bin/sh

echo Findig cpp/hpp/h files..

find src/ -name '*.[ch]pp' -or -name '*.h' > tmp.cppfiles;
echo Extracting from them..
xgettext --c++ --boost --directory "." --from-code=UTF-8 --output tmp.pot --keyword=_:1,1t --keyword=_:1,2,3t --files-from tmp.cppfiles;
rm tmp.cppfiles;

echo Extracting lisp..
xgettext -L Lisp --keyword=_ --from-code=UTF-8 -j --output tmp.pot data/credits.dbwc data/levels/original/*.dbwcs

echo Extracting widgets..

grep -Hn '"*"' \
	data/data/*Widgets \
	| sed -e 's/^\([0-9A-Za-z./\\_]*:[0-9]*\):[0-9A-Za-z \t_-]*"\(.*\)"[ \t]*"\*".*/#: \1\nmsgid "\2"\nmsgstr ""\n/' \
	-e 's/^\([0-9A-Za-z./\\_]*:[0-9]*\):[0-9A-Za-z \t_-]*"\(.*\)"[ \t]*"\(.*\)".*/#: \1\nmsgid "\2"\nmsgstr ""\n\n#: \1\nmsgid "\3"\nmsgstr ""\n/' >> tmp.pot

echo Extracting items..

grep -Hn '"*"' data/data/defItems | grep -v Points | sed 's/^\([0-9A-Za-z./]*:[0-9]*\):[0-9 ]*"\(.*\)".*$/#: \1\nmsgid "\2"\nmsgstr ""\n/' >> tmp.pot

echo Extracting levels..

grep -Hn '"*"' \
	data/data/arcticWastes \
	data/data/assimilator \
	data/data/caves? \
	data/data/comm \
	data/data/finalBattle \
	data/data/floodedTunnel? \
	data/data/grasslands? \
	data/data/hq \
	data/data/icecave? \
	data/data/practice \
	data/data/spaceStation \
	data/data/supply \
	data/data/tomb? \
	| grep -e 'STAGENAME' -e 'OBJECTIVE' -e 'ITEM' -e 'Message' \
	| grep -v 'L.R.T.S' | grep -v 'LRTS' \
	| sed -e 's/^\([0-9A-Za-z.\\/]*:[0-9]*\):[EMHX]* [SOI][A-Z0-9 ]*"\([A-Za-z0-9 .:,#]*\)".*/#: \1\nmsgid "\2"\nmsgstr ""\n/' \
	-e 's/^\([0-9A-Za-z.\\/]*:[0-9]*\):[EMHX]* [A-Z0-9 ]*"Message" [@a-zA-Z0-9 _#]* "\(.*\)".*/#: \1\nmsgid "\2"\nmsgstr ""\n/' >> tmp.pot

echo Removing doubles..
msguniq tmp.pot > data/locale/messages.pot
echo OK
rm tmp.pot


